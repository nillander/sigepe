<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	A classe InputConstruct e responsavel por tratar os dados recebidos e retornar em um array de dados com as configuracoes input. 

	Enquanto a classe inputconstructor monta os array para serem enviadas para a view
*/

class Inputmounting {


    public function mounting($arrData){
      //return $arrData;
      foreach ($arrData as $key => $value) {

        // Identificar cada tipo de input e encaminhar para a funcao que ira 
        // imprimir na tela o respecto conteudo html.

      }
    }


    public function html($input){
      
     switch ($input['input-type']) {
       case 'text':
          $this->text($input);
         # code...
         break;
       
       default:
         # code...
         break;
     }

    }


  /***********************************************  
  TEXT
  ***********************************************/
  public function text($input){

    $inputType         = $input['input-type'];
    $inputLabel        = $input['input-label'];
    $inputHelper       = $input['input-helper'];
    $inputValue        = $input['input-value'];
    $inputPlaceholder  = $input['input-placeholder'];
    $inputId           = $input['input-id'];
    $inputClass        = $input['input-class'];
    $inputRules        = $input['input-rules'];
    $inputDisabled     = $input['input-disabled'];
    $inputEdit         = $input['input-edit'];
    $inputIcon         = $input['input-icon'];
    $inputMaxLength    = $input['input-maxLength'];
    $bootstrapGrid     = $input['bootstrap-grid'];
    $databaseColumn    = $input['database-column'];

    // position icons
    $iconPositionLeft = '';
    $iconPositionRight = '';

    // disabled
    switch ($inputDisabled) {
      case NULL:
        $inputDisabled = '';
        break;
      
      case TRUE:
        $inputDisabled = 'disabled';
        break;
    }


    /* Quando tem icone entra o input-group*/

    if(empty($inputIcon)):
    echo '
    <div class="'.$bootstrapGrid.'">
      <div class="form-group">
        <label class="control-label">'.$inputLabel.'</label>

        <div class="input-group">
        <input type="text" class="form-control" placeholder="'.$inputPlaceholder.'" />
        </div>

        <span class="help-block">'.$inputHelper.' </span>
      </div>
    </div>
    ';
    endif;
      
    if($inputIcon[0]=='inside'):

      if($inputIcon[1]=='left'):
        $iconPositionLeft = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
      endif;    

      if($inputIcon[1]=='right'):
        $iconPositionRight = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
      endif;    

      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label class="control-label" for="'.$inputId.'">'.$inputLabel.'</label>
          <div class="input-group">
            '.$iconPositionLeft.'
            <input type="text" class="form-control '.$inputClass.'" id="'.$inputId.'" placeholder="'.$inputPlaceholder.'">
            '.$iconPositionRight.'
          </div>
          <span class="help-block">'.$inputHelper.' </span>
        </div>
      </div>
      ';

    endif;

    if($inputIcon[0]=='outside'):

      switch ($inputIcon[1]) {
        case 'left':
          $iconPositionLeft = '<i class="'.$inputIcon[2].'"></i> ';
          break;   

        case 'right':
          $iconPositionRight = '<span class="input-group-addon"><i class="'.$inputIcon[2].'"></i></span>';
          break;
      }

      echo '
      <div class="'.$bootstrapGrid.'">
        <div class="form-group">
          <label for="'.$inputId.'">'.$inputLabel.'</label>
          <div class="input-icon">
            '.$iconPositionLeft.'
            <input type="text" class="form-control '.$inputClass.'" id="'.$inputId.'"  placeholder="'.$inputPlaceholder.'" '.$inputDisabled.'>
            '.$iconPositionRight.'
          </div>
        </div>
      </div>
      ';

    endif;


/*
    echo '

      <div class="form-group">

        <label for="exampleInputPassword1">Password</label>

        <div class="input-group">
          <span class="input-group-addon">
            <i class="fa fa-user"></i>
          </span>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>

      </div>


      <div class="form-group">

        <label class="control-label">Email Address</label>

        <div class="input-group">
          <input type="email" class="form-control" placeholder="Email Address">
          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
        </div>

      </div>


      <div class="form-group">
    
        <label class=" control-label">Left Icon</label>
    
          <div class="input-icon">
            <i class="fa fa-bell-o"></i>
            <input type="text" class="form-control" placeholder="Left icon">
          </div>

      </div>


    ';
   */

    return TRUE;
  }


  /***********************************************  
  BTN
  ------------------------------------------------
  array (size=10)
  'input-type' => string 'btn' (length=3)
  'input-label' => string 
  'input-link' => string 
  'input-size' => string
  'input-block' => boolean 
  'input-color' => string 
  'input-icon' => string 
  'input-disabled' => boolean 
  'input-id' => string 
  'input-class
  ***********************************************/
  public function btn($data){

    # LINK
    switch ($data['input-link'][0]) {
      case 'linkInternal':
        $link = base_url() . $data['input-link'][1];
        break;
      case 'linkExternal':
        $link = $data['input-link'][1];
        break;
    }


    echo ' <a href="'.$link.'" id="'.$data['input-id'].'" class="btn ' . $data['input-color'] . ' ' . $data['input-block'] . ' ' . $data['input-size'] . ' ' . $data['input-class'] .  ' ' . $data['input-disabled'] . '" target="'.$data['input-link'][2].'"><i class="'.$data['input-icon'].'"></i> '.$data['input-label'].' </a>';
    return TRUE;
  }


    /***********************************************	
      SELECT
    ***********************************************/
	public function select(array $data){
    return NULL;
	}


    /***********************************************	
      RADIOBUTTON
    ***********************************************/
	public function radiobutton(array $data){
    return NULL;
	}


    /***********************************************	
      CHECKBOX
    ***********************************************/
	public function checkbox(array $data){
    return NULL;
	}

    /***********************************************	
      TEXTAREA
    ***********************************************/
	public function textarea(array $data){
    return NULL;
	}


    /***********************************************	
      SLUG
    ***********************************************/
	public function slug(array $data){
    return NULL;
	}


    /***********************************************	
      PASSWORD
    ***********************************************/
	public function password(array $data){
    return NULL;
	}



}
/* End class*/