<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ResetarSenha extends MX_Controller {

    public $data;

    function __construct() {
        parent::__construct();

        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }


    public function index() {
		$this->Login();    	
    }


    /***********************************************    
      Login
    ***********************************************/
    public function Processar() {

        $this->UsuarioAutenticado();
        $this->LoadAuthentication('Template/GuestOffice/Autenticacao/Formulario', $this->data);            

    }




}
