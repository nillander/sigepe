<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Telefone extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->data['Class'] = $this->router->fetch_class();
        $this->MetronicAsset();
        $this->SigepeAsset();
        $this->data['base_dir'] = 'Template/BackOffice/sistema/Pessoa/';
        $this->data['pessoaId'] = $this->uri->segment(5);
        $this->data['Method']   = '';
        $this->data['StylesFile']['Styles']['profile']  =  TRUE;
        $this->data['ShowProfilePessoa'] = TRUE;

        /* Controladores */
        $this->load->module('pessoa/BackOffice/Pessoa');
    }

    public function index() {
        $this->telefone_browse();
    }

    // fim do método @index



    /**
     * telefone_dashboard.
     *
     *
     * @author Gustavo
     *
     * @return view
     */
    public function telefone_dashboard($pessoaId) {
        $pessoa = $this->obterPessoa($pessoaId);
        $this->data['menuAtivo']  = 'dashboard';
        $this->data['PageHeadTitle'] = $pessoa->pes_nome_razao_social;

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }


    /**
     * telefone_browse.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function telefone_browse_bread($pessoaId) {
        $pessoa = $this->pessoa->obterPessoa($pessoaId);
        $this->data['menuAtivo']  = 'telefone';
        $this->data['pessoaId']   = $pessoa->pes_id;
        $this->data['PageHeadTitle'] = $pessoa->pes_nome_razao_social;
        
        /************************************************/
        $pessoa = $this->pessoa->obterPessoa($pessoaId);
        
        $SqlTelefone = "
        SELECT * FROM tb_telefone as tel
        WHERE fk_peo_id = $pessoaId AND flag_deletado IS NULL
        ORDER BY tel_principal DESC, tel_id DESC";

        $this->data['DatasetTelefone']      =   $this->db->query($SqlTelefone)->result();

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);

    }

    // fim do método @telefone_browse

    /**
     * telefone_read.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function telefone_read() {
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

    // fim do método @telefone_read
    /**
     * telefone_edit.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function telefone_edit() {
        
    }

    // fim do método @telefone_edit
    /**
     * telefone_add.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function telefone_add() {
        
    }

    // fim do método @telefone_add
    /**
     * telefone_disable.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function telefone_disable() {
        
    }

    // fim do método @telefone_disable



    /* # Fim Bread
      # Inicio Middleware
      ==================================================================================================================== */



    /* # Fim Middleware
      # Inicio Acessório
      ==================================================================================================================== */



    /* # Fim Acessório
      # Inicio Controladores
      ==================================================================================================================== */



      /**
       * obterPessoa.
       *
       *
       * @author Nillander
       *
       * @return objeto Pessoa
       */
    public function obterTelefone($telefoneId) {
        return $this->model_crud->get_rowSpecificObject('tb_telefone', 'tel_id', $telefoneId);
    } // fim do metodo @obterTelefone

    public function obterTelefonesDaPessoa($pessoaId) {
        return $this->db->query("SELECT * FROM tb_telefone where fk_peo_id = $pessoaId order by tel_numero asc")->result();
    } // fim do metodo @obterTelefonesDaPessoa



    /* # Fim Controladores
      # Inicio Asset
      ==================================================================================================================== */



    public function MetronicAsset() {
        $this->data['StylesFile']['Plugins']['datatables'] = TRUE;
        /* Styles */
        $this->data['StylesFile']['Plugins']['datatables-bootstrap'] = TRUE;


        /* Scripts */
        $this->data['ScriptsFile']['Plugins']['datatable'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables-bootstrap'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-buttons'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader'] = TRUE;
    }

// fim do método @MetronicAsset

    /**
     * SigepeAsset
     *
     * Carregar aqui dinamicamente JS e CSS
     *
     * @author Gustavo Botega
     * @return array
     */
    public function SigepeAsset() {
        /* Carregando Estilos */
        /* Carregando Scripts */
        $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/Telefone';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/Telefone';
    }

// fim do método @SigepeAsset



}

// end of Class
// end of file