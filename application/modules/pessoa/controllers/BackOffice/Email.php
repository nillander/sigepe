<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Email extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->data['Class'] = $this->router->fetch_class();
        $this->MetronicAsset();
        $this->SigepeAsset();
        $this->data['base_dir'] = 'Template/BackOffice/sistema/Pessoa/';
        $this->data['pessoaId'] = $this->uri->segment(5);
        $this->data['Method']   = '';
        $this->data['StylesFile']['Styles']['profile']  =  TRUE;
        $this->data['ShowProfilePessoa'] = TRUE;

        /* Controladores */
        $this->load->module('pessoa/BackOffice/Pessoa');
    }

    public function index() {
        $this->email_browse();
    }

    // fim do método @index



    /**
     * email_dashboard.
     *
     *
     * @author Gustavo
     *
     * @return view
     */
    public function email_dashboard($pessoaId) {
        $pessoa = $this->obterPessoa($pessoaId);
        $this->data['menuAtivo']  = 'dashboard';
        $this->data['PageHeadTitle'] = $pessoa->pes_nome_razao_social;

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }


    /**
     * email_browse.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function email_browse_bread($pessoaId) {
        $pessoa = $this->pessoa->obterPessoa($pessoaId);
        $this->data['menuAtivo']  = 'email';
        $this->data['pessoaId']   = $pessoa->pes_id;
        $this->data['PageHeadTitle'] = $pessoa->pes_nome_razao_social;
        
        /************************************************/
        $pessoa = $this->pessoa->obterPessoa($pessoaId);
        
        $Sqlemail = "
        SELECT * FROM tb_email as ema
        WHERE fk_peo_id = $pessoaId AND flag_deletado IS NULL
        ORDER BY ema_principal DESC, ema_id DESC";
        
        $this->data['Datasetemail'] = $this->db->query($Sqlemail)->result();        

        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);

    }

    // fim do método @email_browse

    /**
     * email_read.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function email_read($pessoaId) {
        

    }

    // fim do método @email_read
    /**
     * email_edit.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function email_edit() {
        
    }

    // fim do método @email_edit
    /**
     * email_add.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function email_add() {
        
    }

    // fim do método @email_add
    /**
     * email_disable.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function email_disable() {
        
    }

    // fim do método @email_disable



    /* # Fim Bread
      # Inicio Middleware
      ==================================================================================================================== */



    /* # Fim Middleware
      # Inicio Acessório
      ==================================================================================================================== */



    /* # Fim Acessório
      # Inicio Controladores
      ==================================================================================================================== */



      /**
       * obterPessoa.
       *
       *
       * @author Nillander
       *
       * @return objeto Pessoa
       */
    public function obterEmail($emailId) {
        return $this->model_crud->get_rowSpecificObject('tb_email', 'ema_id', $emailId);
    } // fim do metodo @obterEmail

    public function obterEmailsDaPessoa($pessoaId) {
        return $this->db->query("SELECT * FROM tb_email where fk_peo_id = $pessoaId order by ema_email asc")->result();
    } // fim do metodo @obterEmailsDaPessoa



    /* # Fim Controladores
      # Inicio Asset
      ==================================================================================================================== */



    public function MetronicAsset() {
        /* Styles */


        /* Scripts */
    }

// fim do método @MetronicAsset

    /**
     * SigepeAsset
     *
     * Carregar aqui dinamicamente JS e CSS
     *
     * @author 
     * @return array
     */
    public function SigepeAsset() {
        /* Carregando Estilos */
        /* Carregando Scripts */
        $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/email';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Perfil/FrontOffice/email';
    }

// fim do método @SigepeAsset



}

// end of Class
// end of file