<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Desenhador extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->metronicAsset();
        $this->sigepeAsset();

        $this->data['ShowSidebar'] = TRUE;
        $this->data['PageHeadTitle'] = "Desenhador de Percurso";
        $this->data['PageHeadSubtitle'] = "";


        $this->data['base_dir'] = 'Template/BackOffice/sistema/Desenhador/';
        $this->data['Class'] = $this->router->fetch_class();

        /* Controladores Externos */
#        $this->load->module('evento/BackOffice/Evento');
    }

    public function index() {
        $this->Listagem();
    }

    /**
     * desenhador_browse_bread.
     *
     *
     * @author {Author}
     *
     * @return view
     */
    public function desenhador_browse_bread() {
        $this->data['dataSet'] = $this->data['eventos'] = json_decode(file_get_contents(base_url() . "desenhador/GuestOffice/DesenhadorController/listarComOrdem/"));

        /* View */
        $this->LoadTemplate($this->data['base_dir'] . __CLASS__ . "/" . __FUNCTION__, $this->data);
    }

// fim do metodo @desenhador_browse_bread

    /**
     * classe_read
     *
     * @author {Author}
     *
     * @return view
     */
    public function classe_read($contextoId) {
    }

// fim do metodo @classe_read

    /**
     * classe_add
     *
     * @author {Author}
     *
     * @return 
     */
    public function classe_add($eventoId) {

    }

// fim do méotodo @classe_add



    /* # Fim Bread
      # Inicio Middleware
      ==================================================================================================================== */

    /**
     * processar.
     *
     * -
     *
     * @author {Author}
     *
     * @param $dataJson
     *
     * @return boolean
     */
    public function processar($dataJson = false) {

        if (isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if ($dataJson) {
            $dados = $dataJson;
        }

        if (!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }

        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if (!isset($dados['id'])) {
            $resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }
    }

// fim do metodo @processar

    /**
     * gravar.
     *
     * -
     *
     * @author {Author}
     *
     * @param $dados
     *
     * @return boolean
     */
    public function gravar($dados) {

        foreach ($dados as $key => $value) {
            if (empty($value))
                $dados[$key] = NULL;
        }

        // dados
        $dataset = array(
            'fk_aut_id' => $this->session->userdata('PessoaId'),
            'tab_coluna' => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug' => gerarSlug($dados['eve-nome-evento']),
            'criado' => date("Y-m-d H:i:s")
        );


        /* query */
        $query = $this->db->insert('tb_tabela', $dataset);

        if ($query) {
            $classeId = $this->db->insert_id();
            return true;
        } else {
            return false;
        }
    }

// fim do metodo @gravar

    /**
     * alterar.
     *
     * -
     *
     * @author {Author}
     *
     * @param $dados
     *
     * @return boolean
     */
    public function alterar($dados) {

        (!isset($dados['velocidade'])) ? $dados['velocidade'] = null : '';

        // dados
        $dataset = array(
            'tb_id' => $dados['id'],
            'fk_aut_id' => $this->session->userdata('PessoaId'),
            'tab_coluna' => gerarTimeStamp("tb_tabela", "tab_coluna"),
            'eve_slug' => gerarSlug($dados['name-objeto']),
            'criado' => date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->update('tb_tabela', $dataset, array('tb_id' => $dados['id']));
        return $query;
    }

// fim do metodo @alterar

    /**
     * validar.
     *
     * -
     *
     * @author {Author}
     *
     * @param $dados
     *
     * @return boolean
     */
    public function validar($dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    }

// fim do metodo @validar



    /* # Fim Middleware
      # Inicio Acessório
      ==================================================================================================================== */


    /* # Fim Metodo Controladores
      # Inicio Asset
      ========================================================================= */

    /**
     * metronicAsset.
     *
     * -
     *
     * @author {Author}
     *
     * @param 
     *
     * @return
     */
    public function metronicAsset() {
        /* Styles */
        $this->data['StylesFile']['Plugins']['datatables'] = TRUE;
        $this->data['StylesFile']['Plugins']['datatables-bootstrap'] = TRUE;


        /* Scripts */
        $this->data['ScriptsFile']['Plugins']['datatable'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables-bootstrap'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-buttons'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader'] = TRUE;
    }

// fim do metodo MetronicAsset

    /**
     * sigepeAsset.
     *
     * -
     *
     * @author {Author}
     *
     * @param 
     *
     * @return
     */
    public function sigepeAsset() {

        /* Carregando Estilos */

        /* Carregando Scripts */

    }

// fim do metodo sigepeAsset
}

/* End of file classe.php */
