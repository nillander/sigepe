<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Atleta extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Listagem();    	
    }


    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Listagem(){

        /* Setando variaveis de ambiente */	
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Atleta';
        $this->data['PageHeadSubtitle']   = 'Relação de todos os atletas';
        $this->data['Breadcrumbs']        = array();

        /* Taxas */
        $Sql             			= '
												SELECT * FROM tb_pessoa as pes

												INNER JOIN tb_pessoa_fisica as pef
													ON pef.fk_pes_id = pes.pes_id

												INNER JOIN tb_pessoa_fisica_atleta as pfa
													ON pfa.fk_pef_id = pef.pef_id

                                            	';
        $this->data['DatasetAtleta']   = $this->db->query($Sql)->result();


        /* Carrega View */
        $this->LoadTemplate('Template/BackOffice/sistema/Atleta/Listagem', $this->data);

    }



    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'Packages/Styles/Animal/FrontOffice/Cadastro';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Animal';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Cadastro';
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/FormWizard';


    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

