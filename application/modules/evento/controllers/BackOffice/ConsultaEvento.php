<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class ConsultaEvento extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();


        $this->ThemeComponent(); 
        $this->SigepeAsset(); 
    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Todos();
    }




    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Todos(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Consultar Eventos';
        $this->data['PageHeadSubtitle']   = '-';
        $this->data['Breadcrumbs']        = array();


        $SqlTodos                     = '
                                                select 

                                                    eve.*,
                                                    sta.sta_status,
                                                    evm.evm_modalidade


                                                from tb_evento as eve

                                                JOIN tb_status as sta 
                                                ON eve.fk_sta_id = sta.sta_id 

                                                JOIN tb_evento_modalidade as evm 
                                                ON eve.fk_evm_id = evm.evm_id
                                            ';
        $this->data['DatasetEvento']       = $this->db->query($SqlTodos)->result();


        /* Carrega View */
        //$this->loadTemplateUser('', $this->data);
        $this->LoadTemplate('Template/BackOffice/sistema/Evento/Consulta/Todos', $this->data);


    }


    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Ajax = false, $AjaxDados = false){


        $Dados  =   $_POST;
        $this->load->module('evento');


        // Setando Variaveis
        (!isset($Dados['descricao'])) ? $Dados['descricao'] = NULL : '';


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;

        $Gravar                                     =   $this->Gravar($Dados);
        if($Gravar):        

            // ID do evento Gerado
            $EventoId                               =   $Gravar;

            // Gravar Tipo do Evento
            $GravarTipoEvento                       =   $this->evento->GravarTipoEvento($Dados['tipo-evento'], $EventoId);

            // Gravar Desenhador de Percurso
            $GravarDesenhadorPercurso               =   $this->evento->GravarDesenhadorPercurso($Dados['desenhador-percurso'], $EventoId);


        endif;


        if(!$Gravar)
            return false;


        // Cadastro atleta finalizado com sucesso
        redirect(base_url() . 'evento/BackOffice/Evento/Dashboard/' . $EventoId);
        

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Validar inputs e regras de negocio do formulario. Tratar erro. 

        return true;

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){

/*        // Genero Tipo
        (empty($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';

        // Associacao
        (empty($Dados['associacao'])) ? $Dados['associacao'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';
*/

        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }


        // Flag Baia
        if($Dados['baia']=='2')
            $Dados['baia'] = NULL;


        // Flag Quarto de Sela
        if($Dados['quarto-de-sela']=='2')
            $Dados['quarto-de-sela'] = NULL;



        // Genero Tipo
        (!isset($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';


        // Dados
        $Dataset       =  array(
            
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    28, // [ Evento ] Inscrições em breve
            'fk_evm_id'                         =>    $Dados['modalidade'], // Modalidade
            'fk_pes_id'                         =>    $Dados['local'], // Entidade Filiada na qual vai sediar o evento
            'fk_evv_id'                         =>    $Dados['tipo-venda'], // Venda de Inscricao por Tipo ( Serie / Prova / Serie + Prova )
            'eve_nome'                          =>    $Dados['nome-evento'], // Nome do Evento
            'eve_data_inicio'                   =>    $this->my_data->ConverterData($Dados['data-inicio'], 'PT-BR', 'ISO'), //
            'eve_data_fim'                      =>    $this->my_data->ConverterData($Dados['data-fim'], 'PT-BR', 'ISO'), //
            'eve_data_limite_sem_acrescimo'     =>    $this->my_data->ConverterData($Dados['data-limite-sem-acrescimo'], 'PT-BR', 'ISO'), //

            'flag_baia'                         =>    $Dados['baia'], //
            'flag_quarto'                       =>    $Dados['quarto-de-sela'], //
            'flag_site'                         =>    $Dados['ativar-site'], //
            'eve_site_inscricao_inicio'         =>    $this->my_data->ConverterData($Dados['abertura-inscricoes'], 'PT-BR', 'ISO'), //
            'eve_site_inscricao_fim'            =>    $this->my_data->ConverterData($Dados['encerramento-inscricoes'], 'PT-BR', 'ISO'), //
            'eve_descricao'                     =>    $Dados['descricao'], //

            'criado'                            =>    date("Y-m-d H:i:s")

        );


        /* Query */
        $Query = $this->db->insert('tb_evento', $Dataset);

        return ($Query) ? $this->db->insert_id() : false;

    }




    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){
        $this->data['StylesFile']['Plugins']['datatables'] = TRUE;
        /* Styles */
        $this->data['StylesFile']['Plugins']['datatables-bootstrap'] = TRUE;


        /* Scripts */
        $this->data['ScriptsFile']['Plugins']['datatable'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables-bootstrap'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-buttons'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader'] = TRUE;
    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 

        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/ConsultaEvento';

    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

