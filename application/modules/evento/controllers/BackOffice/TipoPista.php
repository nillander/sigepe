<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class TipoPista extends MY_Controller {

    public $tabelaNome = "tb_evento_serie_prova_pista";
    public $columnId = "spp_id";
    public $columnDesc = "spp_pista";

    function __construct() {
        parent::__construct();
        $this->load->model('model_crud');
    }

    public function index(){
        var_dump($this->listar());
        var_dump($this->obter(1));
    }

    public function listar(){
        return $this->db->query("SELECT * from $this->tabelaNome order by $this->columnDesc")->result();
    }

    public function obter($id){
        return $this->model_crud->get_rowSpecificObject($this->tabelaNome, $this->columnId, $id);
    }

}