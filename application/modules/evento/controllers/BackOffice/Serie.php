<?php

if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Serie extends MY_BackOffice {

    public $data;

    function __construct() {
        parent::__construct();
        $this->MetronicAsset();
        $this->SigepeAsset();
        $this->data['ShowProfileEvento'] = TRUE;
        $this->data['base_dir'] = 'Template/BackOffice/sistema/Evento/';
        $this->load->module('evento/BackOffice/Evento');
    }

    public function index() {
        $this->serie_browse();
    } // fim do método @index
    
    /**
    * serie_browse_bread.
    *
    * Responsavel por carregar a relacao de todas as series do evento.
    * Existe pre-definido o id do evento na sessao do usuario. 
    *
    * @author Nillander
    *
    * @return view
    */
    public function serie_browse_bread($eventoId) {

        $SqlSerie = "SELECT * FROM
                        tb_evento_rel_serie as ers
                    INNER JOIN tb_evento as eve ON eve.eve_id = ers.fk_eve_id
                    INNER JOIN tb_evento_serie as evs ON evs.evs_id = ers.fk_evs_id
                    WHERE fk_eve_id = $eventoId ORDER BY evs_altura asc";
        $this->data['DatasetSerie'] = $this->db->query($SqlSerie)->result();
        $this->data['EventoId'] = $eventoId;

        /* View */ $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);

    } // fim do metodo @serie_browse_brd



    /**
    * serie_read.
    *
    * @author Nillander
    * @param
    * @return
    */
    public function serie_read() {
        $this->serie_browse();
    } // fim do metodo @serie_read



    /**
    * serie_edit.
    *
    * Funcao responsavel por carregar a view de edição de serie.
    *
    * @author Nillander
    *
    * @param $eventoId: id do evento, no qual a série a ser editada está registrada
    * @param $serieId: id da serie a ser editado. Referencia no banco: tb_evento_rel_serie.
    *
    * @return view
    */
    public function serie_edit($eventoId, $serieId) {
        
        $serieEditar = $this->db->query("SELECT * from tb_evento_rel_serie where ers_id = $serieId")->result(); //$serieEditar = $this->model_crud->get_rowSpecificObject('tb_evento_rel_serie', 'ers_id', $serieId);
        $this->data['serieEditar'] = $serieEditar;

        $eventoFk = $this->model_crud->get_rowSpecificObject('tb_evento', 'eve_id', $serieEditar[0]->fk_eve_id);
        $this->data['EventoId'] = $eventoFk->eve_id;
        

        $sqlSeriesDoEvento = "SELECT evs_id, fk_evm_id, evs_altura FROM tb_evento_serie as evs
        WHERE fk_evm_id = $eventoFk->fk_evm_id";
        
        $this->data['datasetAlturas'] = $this->db->query($sqlSeriesDoEvento)->result();

        $sqlCategoriasDaSerie = "   SELECT *,
                                    (select count(src_id) from tb_evento_rel_serie_rel_categoria
                                        where
                                            fk_ers_id = $serieId and
                                            fk_evc_id = tb_evento_categoria.evc_id) as categoriaContidaSerie
                                    from tb_evento_categoria
                                    where fk_evm_id = $eventoFk->fk_evm_id
                                    order by evc_sigla
                                ";

        $this->data['categoriasDaSerie'] = $this->db->query($sqlCategoriasDaSerie)->result();

        /* View */ $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);

    } // fim do metodo @serie_edit



    /**
    * serie_add.
    *
    * Funcao responsavel por carregar o formulario de cadastro de serie.
    *
    * @author Nillander
    *
    * @param @serieId   id da serie a ser editado. Referencia no banco: tb_evento_rel_serie.
    *
    * @return view
    */
    public function serie_add($eventoId) {
        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment'] = get_class($this);
        $this->data['PageHeadTitle'] = 'Cadastro';
        $this->data['PageHeadSubtitle'] = 'Para cadastrar um animal no SIGEPE preencha o formulário abaixo.';
        $this->data['Breadcrumbs'] = array();

        $evento = $this->model_crud->get_rowSpecificObject('tb_evento', 'eve_id', $eventoId);

        $sqlSerie = "SELECT evs_id, fk_evm_id, evs_altura FROM tb_evento_serie as evs WHERE fk_evm_id = $evento->fk_evm_id"; 
        /*$sqlSerie = "SELECT eve.eve_id, evs_id, evs.fk_evm_id, evs.evs_altura from tb_evento_serie evs join tb_evento eve on eve.fk_evm_id = evs.fk_evm_idwhere eve.eve_id = $EventoId";*/

        $this->data['DatasetSerie'] = $this->db->query($sqlSerie)->result();
        
        $sqlSerieCategoria = "SELECT * FROM tb_evento_categoria as evc
        WHERE fk_evm_id = $evento->fk_evm_id
        ORDER BY evc_sigla asc";
        $this->data['DatasetSerieCategoria'] = $this->db->query($sqlSerieCategoria)->result();

        $this->data['EventoId'] = $evento->eve_id;

        /* View */ $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);

    } // fim do metodo @serie_add



    /**
    * serie_disable.
    *
    * Funcao responsavel por carregar a view de edicao de serie.
    *
    * @author Nillander
    *
    * @param @serieId   id da serie a ser desativada. Referencia no banco: tb_evento_rel_serie.
    *
    * @return boolean
    */
    public function serie_disable($serieId) {
        pre($serieId);
    } // fim do metodo @serie_disable



    /* # Fim Bread
       # Inicio Middleware
    ====================================================================================================================*/



    /**
    * processar.
    *
    * -
    *
    * @author Gustavo Botega
    *
    * @param
    *
    * @return boolean
    */
    public function processar($dataJson = false) {

        /*
            Resgatando valores e tratando exceções
            O dado pode ser recebido tanto por requisição ajax quanto por parametro no metodo.
            Tratamento para cada situação.
       */
        if(isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados      =   $_POST['dataJson'];
        }

        if($dataJson) {
            $dados      =   $dataJson;
        }

        if(!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }
            

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }


        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if(!isset($dados['id'])) {
            $resultadoAcao         =   $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);
        } else {
            $resultadoAlterar      =   $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }

    } // fim do metodo @processar



    /**
    * validar
    *
    * -
    *
    * @author Nillander
    *
    * @param 
    *
    * @return boolean
    */
    public function validar() {
        return true; // please, implement this
    } // fim do metodo @validar



    /**
    * gravar
    *
    * -
    *
    * @author Nillander
    *
    * @param 
    *
    * @return boolean
    */
    public function gravar($dados) {

        // dados
        $dataset = array(
            'fk_aut_id'                                 => $this->session->userdata('PessoaId'),
            'fk_eve_id'                                 => $dados['evento-id'],
            'fk_evs_id'                                 => $dados['altura'],
            'ers_nome'                                  => $dados['nome-serie'],
            'ers_valor_ate_inscricao_definitiva'        => str_replace(',', '.', str_replace('.', '', $dados['valor-ate-inscricao-definitiva'])),
            'ers_valor_apos_inscricao_definitiva'       => str_replace(',', '.', str_replace('.', '', $dados['valor-apos-inscricao-definitiva'])),
            'criado'                                    => date("Y-m-d H:i:s")
        );

        $query = $this->db->insert('tb_evento_rel_serie', $dataset);
        if($query) {
            $serieId = $this->db->insert_id();
            $gravarCategoria = $this->GravarCategoriasDaSerie($dados['serie-categoria'], $serieId);
            return true;
        }else{
            return false;
        }

    } // fim do metodo @gravar



    /**
    * alterar
    *
    * -
    *
    * @author Nillander
    *
    * @param $
    *
    * @return boolean
    */
    public function alterar($dados) {


        $data                   =   array(
                                        'fk_evs_id'                                 => $dados['altura'],
                                        'ers_nome'                                  => $dados['nome-serie'],
                                        'ers_valor_ate_inscricao_definitiva'        => str_replace(',', '.', str_replace('.', '', $dados['valor-ate-inscricao-definitiva'])),
                                        'ers_valor_apos_inscricao_definitiva'       => str_replace(',', '.', str_replace('.', '', $dados['valor-apos-inscricao-definitiva'])),
                                        'modificado'                                => date("Y-m-d H:i:s")
                                    );

        $this->db->update('tb_evento_rel_serie', $data, array('ers_id' => $dados['id'] ));

        /*
        Mandar array de categorias selecionadas para excluir ou adicionar vínculos
        Para função responsável por excluir ou adicionar vínculos
       */
        $this->modificarVinculosDeCategoria($dados['serie-categoria'], $dados['id']);


        return true;
    } // fim do metodo @alterar



    /* # Fim Middleware
       # Inicio Metodo Acessorio 
    ====================================================================================================================*/



    /**
    * GetCategoriasDaSerie
    *
    * -
    *
    * @author Gustavo Botega
    *
    * @param 
    *
    * @return boolean
    */
    public function GetCategoriasDaSerie($serieId) {
            $sqlCategoriaSerie = "
                SELECT * FROM
                    tb_evento_rel_serie_rel_categoria as src
                INNER JOIN tb_evento_rel_serie as ers ON ers.ers_id = src.fk_ers_id
                INNER JOIN tb_evento_categoria as evc ON evc.evc_id = src.fk_evc_id
                WHERE 
                    ers.ers_id = $serieId
                ORDER BY evc.evc_id asc";
            return $this->db->query($sqlCategoriaSerie)->result();
    } 



    /**
    * GetCategoriasDaSerie
    *
    * -
    *
    * @author Gustavo Botega
    *
    * @param 
    *
    * @return boolean
    */
        public function GetProvasDaSerie($serieId) {
            $sqlProvasDaSerie = "
                SELECT * FROM tb_evento_rel_serie_rel_prova as srp
                WHERE srp.fk_ers_id = $serieId
                ORDER BY srp.srp_numero_prova ASC";
            return $this->db->query($sqlProvasDaSerie)->result();
    }



    /**
    * GravarCategoriasDaSerie
    *
    * ------------
    *
    * @author Nillander
    *
    * @param void
    *
    * @return boolean
    */
    public function GravarCategoriasDaSerie($categorias, $serieId) {

        if(is_string($categorias))
            $categorias = array($categorias);

        foreach ($categorias as $key => $value) {
            $dataset = array(
                'fk_aut_id' => $this->session->userdata('PessoaId'),
                'fk_ers_id' => $serieId,
                'fk_evc_id' => $value,
                'criado' => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('tb_evento_rel_serie_rel_categoria', $dataset);
        }

    }


    /**
    * GravarCategoriasDaSerie
    *
    * ------------
    *
    * @author Nillander
    *
    * @param void
    *
    * @return boolean
    */
    public function modificarVinculosDeCategoria($arrCategorias, $serieId) {

        if(is_string($arrCategorias)) {
            $arrCategorias = array($arrCategorias);
        }

        $sInCategoria = "";

        foreach ($arrCategorias as $key => $value) {
            $sInCategoria  =  $sInCategoria . $value .',';
        }
        $sInCategoria = substr($sInCategoria, 0, -1);


        $serie      =   $this->obterSerie($serieId);
        $evento     =   $this->evento->obterEvento($serie->fk_eve_id);

        $queryParaDeletarVinculos = "
                                    SELECT *,
                                        (
                                            select count(src_id) from tb_evento_rel_serie_rel_categoria
                                            where
                                                fk_ers_id = $serie->ers_id and
                                                fk_evc_id = tb_evento_categoria.evc_id
                                        ) as categoriaContidaSerie
                                    from tb_evento_categoria
                                    where fk_evm_id = $evento->fk_evm_id
                                    and evc_id not in ($sInCategoria)
                                    having categoriaContidaSerie = 1
                                    order by evc_sigla";
        $arrVinculosParaRemover = $this->db->query($queryParaDeletarVinculos)->result();

        foreach ($arrVinculosParaRemover as $catVin) {
            $deletarVinculo = "DELETE FROM tb_evento_rel_serie_rel_categoria where fk_evc_id = $catVin->evc_id and fk_ers_id = $serie->ers_id";
            $this->db->query($deletarVinculo);
        }


        $queryParaCriarVinculos = "
                                    SELECT *,
                                    (select count(src_id) from tb_evento_rel_serie_rel_categoria
                                        where
                                            fk_ers_id = $serie->ers_id and
                                            fk_evc_id = tb_evento_categoria.evc_id) as categoriaContidaSerie
                                    from tb_evento_categoria
                                    where fk_evm_id = $evento->fk_evm_id
                                    and evc_id in ($sInCategoria)
                                    having categoriaContidaSerie = 0
                                    order by evc_sigla";
        $arrVinculosParaCriar = $this->db->query($queryParaCriarVinculos)->result();

        foreach ($arrVinculosParaCriar as $value) {
            $dataset = array(
                'fk_aut_id' => $this->session->userdata('PessoaId'),
                'fk_ers_id' => $serie->ers_id,
                'fk_evc_id' => $value->evc_id,
                'criado' => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('tb_evento_rel_serie_rel_categoria', $dataset);
        }

        



#        pre($arrCategorias);
 #       pre($serieId);

        /*
        if(is_string($categorias)) {
            $categorias = array($categorias);
        }

        foreach ($categorias as $key => $value) {
            $dataset = array(
                'fk_aut_id' => $this->session->userdata('PessoaId'),
                'fk_ers_id' => $serieId,
                'fk_evc_id' => $value,
                'criado' => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('tb_evento_rel_serie_rel_categoria', $dataset);
        }
       */

    }






    /* # Fim Metodo Acessorios
       # Inicio Metodos Controladores 
    =========================================================================*/
    /**
    * obterSerie
    *
    * ------------
    *
    * @author Gustavo Botega
    *
    * @param void
    *
    * @return boolean
    */
    public function obterSerie($serieId) {
        $sql = "SELECT * FROM tb_evento_rel_serie WHERE ers_id = $serieId";
        $query = $this->db->query($sql)->result();
        return $query[0];
    } // fim do método @obterSerie




    /* # Fim Metodo Controladores
       # Inicio Asset 
    =========================================================================*/


    /**
    * MetronicAssets
    *
    * @author Gustavo Botega
    * @param void
    * @return boolean
    */
     public function MetronicAsset() {



        /* STYLES  */
        // PLUGINS  //
        $this->data['StylesFile']['Plugins']['bootstrap-select']                            = TRUE;
        $this->data['StylesFile']['Plugins']['multi-select']                                = TRUE;
        $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
        $this->data['StylesFile']['Plugins']['select2-bootstrap']                           = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;

        // STYLES  //
        /* {NamePackage} */
        $this->data['StylesFile']['Styles']['profile']                                      = TRUE;

        /* SCRIPTS */
        // PLUGINS  //
        /* {NamePackage} */
        $this->data['ScriptsFile']['Plugins']['jquery-validation']                          = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']       = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask']                                 = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                         = TRUE;

        /* Multiple Select */
        $this->data['ScriptsFile']['Plugins']['jquery-multi-select']                        = TRUE;
        $this->data['ScriptsFile']['Plugins']['select2-full']                               = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-select']                           = TRUE;

        /* User Profile */
        $this->data['ScriptsFile']['Plugins']['jquery-sparkline']                           = TRUE;

        // SCRIPTS //
        /* Bootstrap Sweet Alert */
        $this->data['ScriptsFile']['Scripts']['ui-sweetalert'] = TRUE;
        /* Multiple Select */
        $this->data['ScriptsFile']['Scripts']['components-multi-select'] = TRUE;
        /* User Profile */
        $this->data['ScriptsFile']['Scripts']['profile'] = TRUE;

        $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

        /*  Scripts*/
        $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;


    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega
    * @return array
    */
    public function SigepeAsset() {
        /* Carregando Estilos */

        /* Carregando Scripts */
        $this->data['PackageScripts'][] = 'Packages/Scripts/Evento/BackOffice/FormValidationSerie';

    }


    /* # Fim Asset
    =========================================================================*/

} /* End of file Serie.php */