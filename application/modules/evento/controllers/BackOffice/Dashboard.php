<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends MY_BackOffice {

    public $data;

    function __construct() {
        
        parent::__construct();
        
        $this->InformacoesTemplate();
        $this->data['Class'] = $this->router->fetch_class();
        $this->MetronicAsset();
        $this->SigepeAsset();
        $this->data['ShowProfileEvento'] = TRUE;
        $this->data['base_dir'] = 'Template/BackOffice/sistema/Evento/';
        /* Controladores */
        $this->load->module('evento/BackOffice/Evento');
        $this->load->module('status/CommonOffice/Status');
    }

    public function index() {

    }

    /**
     * dashboard_browse_bread.
     *
     *
     * @author Nillander
     *
     * @return view
     */
    public function dashboard_browse() {
        
    }

// fim do método @dashboard_browse_bread

    /**
     * dashboard_read
     *
     * @author
     *
     * @return 
     */
    public function dashboard_read_read($eventoId) {
        $evento = $this->model_crud->get_rowSpecificObject("tb_evento", "eve_id", $eventoId);
        
        $this->data['EventoId'] = $evento->eve_id;
        $this->data['Method'] = $this->router->fetch_method();
        $this->data['ShowProfileEvento'] = TRUE;

        $SqlEvento = "
            SELECT evv.evv_venda, evv.evv_descricao, sts.sta_status, moda.evm_modalidade, pes.pes_nome_razao_social as pes_autor,
                loc.pes_nome_razao_social as pes_local, eve.*
            from tb_evento as eve
            join tb_pessoa pes on eve.fk_aut_id = pes.pes_id
            join tb_pessoa loc on eve.fk_pes_id = loc.pes_id
            join tb_evento_modalidade moda on eve.fk_evm_id = moda.evm_id
            join tb_status sts on eve.fk_sta_id = sts.sta_id
            join tb_evento_venda evv on eve.fk_evv_id = evv.evv_id
            where eve.eve_id = $evento->eve_id";
        $this->data['evento'] = $this->db->query($SqlEvento)->result();

        $this->data['arrStatus'] = $this->status->obterEntre(400, 405);


        $sqlTipoEvento = "
        SELECT *,
            (select count(ert_id) from tb_evento_rel_tipo
            where fk_eve_id = $evento->eve_id and fk_evt_id = tb_evento_tipo.evt_id
            ) as tiposDoEvento
        from tb_evento_tipo
        where fk_evm_id = $evento->fk_evm_id";
        $this->data['arrTipoEvento'] = $this->db->query($sqlTipoEvento)->result();
        

        $sqlLocais = "SELECT * FROM tb_pessoa where pes_natureza = 'PJ' order by pes_nome_razao_social asc;";
        $this->data['arrLocais'] = $this->db->query($sqlLocais)->result();

        $sqlTipoVenda = "SELECT * FROM tb_evento_venda order by evv_venda asc";
        $this->data['arrTipoVenda'] = $this->db->query($sqlTipoVenda)->result();


        $SqlDesenhadores = "
        SELECT
            pes_id, fk_sta_id, pes_nome_razao_social, pes_foto,
        (select count(erd_id) from tb_evento_rel_desenhador
            where
                fk_eve_id = $evento->eve_id and
                fk_pes_id = tb_pessoa.pes_id) as desenhadorDoEvento
        from tb_pessoa
        where pes_id in (select fk_pes1_id from tb_vinculo where fk_per_id = 6)
        order by pes_nome_razao_social asc";
        $this->data['arrDesenhadorPercurso'] = $this->db->query($SqlDesenhadores)->result();

        $this->data['QuantidadeDiasEvento'] = $this->GetQuantidadeDiasEvento($evento->eve_id);

        /* View */
        $this->LoadTemplate($this->data['base_dir'].__CLASS__."/".__FUNCTION__, $this->data);
    }

// fim do método @dashboard_read

    /**
     * dashboard_add
     *
     * @author
     *
     * @return 
     */
    public function dashboard_add($eventoId) {
        
    }

// fim do méotodo @dashboard_add





    /* # Fim Bread
      # Inicio Middleware
      ==================================================================================================================== */

    /**
     * processar.
     *
     * -
     *
     * @author Gustavo Botega
     *
     * @param
     *
     * @return boolean
     */
    public function processar($dataJson = false) {
        if(isset($_POST['dataJson']) && is_array($_POST['dataJson'])) {
            $dados = $_POST['dataJson'];
        }

        if($dataJson) {
            $dados = $dataJson;
        }

        if(!isset($_POST['dataJson']) && !$dataJson) {
            echo json_encode(false);
        }

        // Validacao Formulario
        $validar = $this->validar($dados);
        if (!$validar) {
            echo json_encode(false);
            return false;
        }
        
        // Se existir algum ID por convencao entende-se que o metodo a ser direcionado e de atualizacao.
        if(!isset($dados['id'])) {
            /*$resultadoAcao = $this->gravar($dados);
            echo ($resultadoAcao) ? json_encode(true) : json_encode(false);*/
        } else {
            $resultadoAlterar = $this->alterar($dados);
            echo ($resultadoAlterar) ? json_encode(true) : json_encode(false);
        }

    } // fim do metodo @processar

// fim do metodo @processar

    /**
     * Gravar
     *
     * @author Gustavo Botega
     */
    public function gravar($Dados) {
        
    }

// fim do metodo @gravar

    /**
     * Alterar
     *
     * @author Gustavo Botega
     */
    public function alterar($dados) {
        foreach ($dados as $key => $value) {
            if(empty($value))
                $dados[$key]    =   NULL;
        }

        // Flag Baia
        if($dados['eve-baia-ativo']=='0'){
            $dados['eve-baia-ativo'] = NULL;
        }

        // Flag Quarto de Sela
        if($dados['eve-quarto-sela']=='0'){
            $dados['eve-quarto-sela'] = NULL;
        }

        // Ativo no site
        if($dados['eve-ativo-no-site']=='0') {
            $dados['eve-ativo-no-site'] = NULL;
        }

        $dados['eve-site-inscricao-inicio'] = str_replace(" 00:00:00", "", $dados['eve-site-inscricao-inicio']);
        $dados['eve-site-inscricao-fim'] = str_replace(" 00:00:00", "", $dados['eve-site-inscricao-fim']);

        if(empty($dados['eve-tipo-evento'])){
            $dados['eve-tipo-evento'] = "0";
        }

        if(empty($dados['eve-desenhadores-percurso'])){
            $dados['eve-desenhadores-percurso'] = "0";
        }

        if(empty($dados['eve-controle']) || is_null($dados['eve-controle']) || strlen($dados['eve-controle']) < 10){
            $dados['eve-controle'] = gerarTimeStamp("tb_evento", "eve_controle");
        }

        // Dados
        $dataset       =  array(
            'eve_id' => $dados['id'],
            'fk_sta_id' => $dados['eve-status-evento'],
            'eve_nome' => $dados['eve-nome-evento'],
            'eve_controle' => $dados['eve-controle'],
            'eve_slug' => gerarSlug($dados['eve-nome-evento']),
            'eve_data_inicio' => $this->my_data->ConverterData($dados['eve-data-inicio'], 'PT-BR', 'ISO'),
            'eve_data_fim' => $this->my_data->ConverterData($dados['eve-data-fim'], 'PT-BR', 'ISO'),
            'eve_quantidade_prova' => $dados['eve-quantidade-prova'],
            'eve_data_limite_sem_acrescimo' => $this->my_data->ConverterData($dados['eve-data-limite-sem-acrescimo'], 'PT-BR', 'ISO'),
            'fk_pes_id' => $dados['eve-local'],
            'flag_site' => $dados['eve-ativo-no-site'],
            'eve_site_inscricao_inicio' => $this->my_data->ConverterData($dados['eve-site-inscricao-inicio'], 'PT-BR', 'ISO'),
            'eve_site_inscricao_fim' => $this->my_data->ConverterData($dados['eve-site-inscricao-fim'], 'PT-BR', 'ISO'),
            'fk_evv_id' => $dados['eve-tipo-venda-inscricoes'],
            'flag_baia' => $dados['eve-baia-ativo'],
            'flag_quarto' => $dados['eve-quarto-sela'],
            'eve_descricao' => $dados['eve-descricao'],
            'modificado' => date("Y-m-d H:i:s")
        );

        /* Query */
        $query = $this->db->update('tb_evento', $dataset, array('eve_id' => $dados['id'] ));

        $this->modificarVinculoTipoEvento($dados['eve-tipo-evento'], $dados['id']);
        $this->modificarVinculoDesenhadorPercurso($dados['eve-desenhadores-percurso'], $dados['id']);
        return $query;

    } // fim do método @alterar

    /**
     * validar
     *
     * @author Gustavo Botega
     */
    public function validar($Dados) {
        return true; // Validar inputs e regras de negocio do formulario. Tratar erro.
    }


    public function uploadLogo(){
        if($_FILES["file"]["name"] != '') {
            $test = explode('.', $_FILES["file"]["name"]);
            $ext = end($test);
            $name = rand(100, 999) . '.' . $ext;
            //$location = "C:/Program Files/VertrigoServ/www/sigepe/assets/sigepe/Global/Images/Evento" . $name;  
            //$location = base_url()."assets/sigepe/Global/Images/Evento/" . $name;  
            $location = './assets/sigepe/Global/Images/Evento/' . $name;
            move_uploaded_file($_FILES["file"]["tmp_name"], $location);
            echo '<img src="'.base_url()."assets/sigepe/Global/Images/Evento/".$name.'" height="150" width="225" class="img-thumbnail" />';
        }
    }

// fim do método @validar




    /* # Fim Middleware
      # Inicio Acessório
      ==================================================================================================================== */

    /**
    * InformacoesTemplate
    *
    * @author Nillander
    */
    public function modificarVinculoTipoEvento($arrTipoEvento, $evento) {
        if(is_string($arrTipoEvento)) {
            $arrTipoEvento = array($arrTipoEvento);
        }

        $sInCategoria = "";

        foreach ($arrTipoEvento as $key => $value) {
            $sInCategoria  =  $sInCategoria . $value .',';
        }
        $sInCategoria = substr($sInCategoria, 0, -1);

        $evento     =   $this->evento->obterEvento($evento);

        $queryParaDeletarVinculos = "
            SELECT *,
                (select count(ert_id) from tb_evento_rel_tipo
                where fk_eve_id = $evento->eve_id and fk_evt_id = tb_evento_tipo.evt_id
                ) as tiposDoEvento
            from tb_evento_tipo
            where evt_id not in ($sInCategoria)
            having tiposDoEvento = 1";
        $arrVinculosParaRemover = $this->db->query($queryParaDeletarVinculos)->result();

        foreach ($arrVinculosParaRemover as $tipoVin) {
            $deletarVinculo = "DELETE FROM tb_evento_rel_tipo where fk_evt_id = $tipoVin->evt_id and fk_eve_id = $evento->eve_id";
            $this->db->query($deletarVinculo);
        }

        $queryParaCriarVinculos = "
            SELECT *,
                (select count(ert_id) from tb_evento_rel_tipo
                where fk_eve_id = $evento->eve_id and fk_evt_id = tb_evento_tipo.evt_id
                ) as tiposDoEvento
            from tb_evento_tipo
            where evt_id in ($sInCategoria)
            having tiposDoEvento = 0";
        $arrVinculosParaCriar = $this->db->query($queryParaCriarVinculos)->result();

        foreach ($arrVinculosParaCriar as $value) {
            $dataset = array(
                'fk_aut_id' => $this->session->userdata('PessoaId'),
                'fk_eve_id' => $evento->eve_id,
                'fk_evt_id' => $value->evt_id,
                'criado' => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('tb_evento_rel_tipo', $dataset);
        }
    }// fim do método @modificarVinculoTipoEvento



    /**
    * InformacoesTemplate
    *
    * @author Nillander
    */
    public function modificarVinculoDesenhadorPercurso($arrDesenhadorPercurso, $evento) {
        if(is_string($arrDesenhadorPercurso)) {
            $arrDesenhadorPercurso = array($arrDesenhadorPercurso);
        }

        $sInCategoria = "";

        foreach ($arrDesenhadorPercurso as $key => $value) {
            $sInCategoria  =  $sInCategoria . $value .',';
        }
        $sInCategoria = substr($sInCategoria, 0, -1);

        $evento = $this->evento->obterEvento($evento);

        $queryParaDeletarVinculos = "
            SELECT
                pes_id, fk_sta_id, pes_nome_razao_social, pes_foto,
            (select count(erd_id) from tb_evento_rel_desenhador
                where
                    fk_eve_id = $evento->eve_id and
                    fk_pes_id = tb_pessoa.pes_id) as desenhadorDoEvento
            from tb_pessoa
            where pes_id in (select fk_pes1_id from tb_vinculo where fk_per_id = 6)
            and pes_id not in ($sInCategoria)
            having desenhadorDoEvento = 1";
        $arrVinculosParaRemover = $this->db->query($queryParaDeletarVinculos)->result();

        foreach ($arrVinculosParaRemover as $tipoVin) {
            $deletarVinculo = "DELETE FROM tb_evento_rel_desenhador where fk_pes_id = $tipoVin->pes_id and fk_eve_id = $evento->eve_id";
            $this->db->query($deletarVinculo);
        }

        $queryParaCriarVinculos = "
            SELECT
                pes_id, fk_sta_id, pes_nome_razao_social, pes_foto,
            (select count(erd_id) from tb_evento_rel_desenhador
                where
                    fk_eve_id = $evento->eve_id and
                    fk_pes_id = tb_pessoa.pes_id) as desenhadorDoEvento
            from tb_pessoa
            where pes_id in (select fk_pes1_id from tb_vinculo where fk_per_id = 6)
            and pes_id in ($sInCategoria)
            having desenhadorDoEvento = 0";
        $arrVinculosParaCriar = $this->db->query($queryParaCriarVinculos)->result();

        foreach ($arrVinculosParaCriar as $value) {
            $dataset = array(
                'fk_aut_id' => $this->session->userdata('PessoaId'),
                'fk_eve_id' => $evento->eve_id,
                'fk_pes_id' => $value->pes_id,
                'criado' => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('tb_evento_rel_desenhador', $dataset);
        }
    }// fim do método @modificarVinculoTipoEvento


      /**
      * InformacoesTemplate
      *
      * Retorna pra view as informacoes que compoe o template do Evento
      *
      *
      * @author Gustavo Botega
      */
      public function InformacoesTemplate(){

          $EventoId                       =   $this->uri->segment(5);
          $ModalidadeId                   =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'fk_evm_id');

          $this->data['NomeEvento']       =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_nome');
          $this->data['Modalidade']       =   $this->model_crud->get_rowSpecific('tb_evento_modalidade', 'evm_id', $ModalidadeId, 1, 'evm_modalidade');

          $this->data['Logotipo']         =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_site_logotipo');
          $this->data['FolderId']         =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_controle');

          return $this->data;

      }



      /**
      * GetQuantidadeDiasEvento
      *
      * @param $eventoId
      * @author Gustavo Botega
      * @return int
      */
      public function GetQuantidadeDiasEvento($EventoId){


          $DataInicio     =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_data_inicio');
          $DataFim        =   $this->model_crud->get_rowSpecific('tb_evento', 'eve_id', $EventoId, 1, 'eve_data_fim');

          $QuantidadeDias =   DateDifferences($DataFim, $DataInicio, 'd') + 1;
          return $QuantidadeDias;

      }



    /* # Fim Metodo Acessório
      # Inicio Método Controladores
      ========================================================================= */



    /* # Fim Metodo Controladores
      # Inicio Asset
      ========================================================================= */

    /**
     * PackagesClass
     *
     * @author Gustavo Botega
     */
    public function MetronicAsset() {
        /* STYLES
        -----------------------------------*/


        // PLUGINS  //
        /* {NamePackage} */
        $this->data['StylesFile']['Plugins']['bootstrap-select']                            = TRUE;
        $this->data['StylesFile']['Plugins']['multi-select']                                = TRUE;
        $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
        $this->data['StylesFile']['Plugins']['select2-bootstrap']                           = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;

        /* Date & Time Pickers */
        $this->data['StylesFile']['Plugins']['daterangepicker']                             = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-datepicker3']                       = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-timepicker']                        = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-datetimepicker']                    = TRUE;
        $this->data['StylesFile']['Plugins']['clockface']                                   = TRUE;

        /* Bootstrap File Input */
        $this->data['StylesFile']['Plugins']['bootstrap-fileinput']                         = TRUE;

        /* Markdown & WYSIWYG Editors */
        $this->data['StylesFile']['Plugins']['bootstrap-wysihtml5']                         = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-markdown']                          = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-summernote']                        = TRUE;


        // STYLES  //
        /* {NamePackage} */
        $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        $this->data['StylesFile']['Styles']['profile']                                      = TRUE;


        /* SCRIPTS
        -----------------------------------*/
        // PLUGINS  //
        /* {NamePackage} */
        $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
        $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

        /* Date & Time Pickers */
        $this->data['ScriptsFile']['Plugins']['moment']                                   = TRUE;
        $this->data['ScriptsFile']['Plugins']['daterangepicker']                          = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker']                     = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-timepicker']                     = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-datetimepicker']                 = TRUE;
        $this->data['ScriptsFile']['Plugins']['clockface']                                = TRUE;

        /* Bootstrap File Input */
        $this->data['ScriptsFile']['Plugins']['bootstrap-fileinput']                      = TRUE;

        /* Markdown & WYSIWYG Editors */
        $this->data['ScriptsFile']['Plugins']['wysihtml5']                                = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-wysihtml5']                      = TRUE;
        $this->data['ScriptsFile']['Plugins']['markdown']                                 = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-markdown']                       = TRUE;
        $this->data['ScriptsFile']['Plugins']['summernote']                               = TRUE;

        /* Markdown & WYSIWYG Editors */
        $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;

        /* Multiple Select */
        $this->data['ScriptsFile']['Plugins']['jquery-multi-select']                        = TRUE;
        $this->data['ScriptsFile']['Plugins']['select2-full']                               = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-select']                           = TRUE;
        $this->data['ScriptsFile']['Scripts']['components-multi-select'] = TRUE;

        // SCRIPTS //
        /* Bootstrap Sweet Alert */
        $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                  = TRUE;

        /* Date & Time Pickers */
        $this->data['ScriptsFile']['Scripts']['components-date-time-pickers']   = TRUE;

        /* Markdown & WYSIWYG Editors */
        $this->data['ScriptsFile']['Scripts']['components-editors']             = TRUE;
        /* User Profile */

    }

// fim do método MetronicAsset

    /**
     * SigepeAsset
     *
     * Carregar aqui dinamicamente JS e CSS
     *
     * @author Gustavo Botega
     * @return array
     */
    public function SigepeAsset() {
        $this->data['PackageScripts'][]   =   'Packages/Scripts/Evento/BackOffice/FormValidationDashboard';
    }

}

// fim do método SigepeAsset

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */