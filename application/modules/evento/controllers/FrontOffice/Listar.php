<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Listar extends MY_FrontOffice {

    function __construct() {
        parent::__construct();
		
		$this->ThemeComponent();
		$this->SigepeAsset();
    }


    public function EventosPorStatus() {
        $this->data['PageHeadTitle']      = 'Eventos '; 
        $this->data['PageHeadSubtitle']   = 'Confira a relação de eventos que estão com as inscrições em aberto.';


        /*  EVENTO */
        $SqlEvento                     = '
                                            SELECT status.*,
                                            eve_id, fk_sta_id, fk_pes_id, eve_nome, eve_controle, eve_site_logotipo, eve_data_inicio,  eve_data_fim, eve_site_inscricao_inicio, eve_site_inscricao_fim,  UNIX_TIMESTAMP(eve_site_inscricao_fim) AS DATE
                                            FROM
                                            tb_evento as eve

                                            JOIN tb_status as status ON status.sta_id = eve.fk_sta_id

                                            WHERE
                                            fk_sta_id > 400 and fk_sta_id < 406
                                            ORDER BY
                                            DATE DESC
                                            ';
        $this->data['DatasetEvento']    = $this->db->query($SqlEvento)->result();        
        $this->data['Logotipo']         = $this->data['DatasetEvento'][0]->eve_site_logotipo;
        $this->data['EventoControle']   = $this->data['DatasetEvento'][0]->eve_controle;


        $this->LoadTemplateUser(
                'Template/FrontOffice/sistema/Evento/Listar/EventosPorStatus',
                $this->data
        );
    }
    


    public function Calendario() {
        $this->data['Ambiente'] = 'FrontOffice'; 
        $this->data['PageHeadTitle'] = 'Calendário de Eventos'; 
        $this->data['PageHeadSubtitle'] = 'Confira nossos eventos.';

        $this->data['eventos'] = json_decode(file_get_contents(base_url()."evento/GuestOffice/Evento/listarComOrdem/eve_data_inicio/eve_data_fim/"));

        
        $this->LoadTemplateUser('Template/FrontOffice/sistema/Evento/Listar/Calendario',$this->data);
    }
	

    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent() {


        /* StylesFile */
        /* Plugins*/
        $this->data['StylesFile']['Plugins']['select2'] = TRUE;
        $this->data['StylesFile']['Plugins']['bootstrap-sweetalert'] = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete'] = TRUE;
        $this->data['StylesFile']['Plugins']['easy-autocomplete-themes'] = TRUE;
        $this->data['StylesFile']['Styles']['todo-2'] = TRUE;
        $this->data['StylesFile']['Plugins']['datatables'] = TRUE;
        $this->data['StylesFile']['Plugins']['datatables-bootstrap'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatable'] = TRUE;

        /* Syles*/
        //$this->data['StylesFile']['Styles']['profile'] = TRUE;


        /* ScriptsFile */
        /* Plugins*/
        $this->data['ScriptsFile']['Plugins']['select2'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-wizard'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['input-mask-trigger'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['easy-autocomplete'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables'] = TRUE;
        $this->data['ScriptsFile']['Plugins']['datatables-bootstrap'] = TRUE;

        /*  Scripts*/
        $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader'] = TRUE;
        $this->data['ScriptsFile']['Scripts']['ui-sweetalert'] = TRUE;

    }

	
	
    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
//        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
//        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';


    }

	
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

