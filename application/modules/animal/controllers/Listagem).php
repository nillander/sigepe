<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Listagem extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Listagem();    	
    }


    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Listagem(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Cadastro';
        $this->data['PageHeadSubtitle']   = 'Para cadastrar um animal no SIGEPE preencha o formulário abaixo.';
        $this->data['Breadcrumbs']        = array();


        $SqlFederacao                     = '
                                                SELECT * FROM
                                                    tb_pessoa as pes
                                                INNER JOIN
                                                    tb_vinculo as vin
                                                ON
                                                    pes.pes_id = vin.fk_pes1_id
                                                WHERE
                                                    vin.fk_per_id = 3 AND
                                                    vin.fk_tip_id IS NULL AND
                                                    vin.fk_sta_id = 1 AND
                                                    pes.fk_sta_id = 1 
                                                ORDER BY
                                                    pes_id=141 desc,
                                                    pes_nome_razao_social asc
                                            ';
        $this->data['DatasetFederacao']   = $this->db->query($SqlFederacao)->result();



        /* Entidade Filiada */
        $SqlEntidadeFiliada                     = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_entidade_filiada = 1

                                                ORDER BY pes.pes_nome_razao_social ASC

                                            ';
        $this->data['DatasetEntidadeFiliada']   = $this->db->query($SqlEntidadeFiliada)->result();


        /* Escola Equitacao */
        $SqlEscolaEquitacao             = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_escola_equitacao = 1

                                                ORDER BY pes.pes_nome_razao_social ASC
                                            ';
        $this->data['DatasetEscolaEquitacao']   = $this->db->query($SqlEscolaEquitacao)->result();


        $SqlAssociacao                  = '
                                                SELECT * FROM
                                                    tb_pessoa as pes
                                                INNER JOIN
                                                    tb_vinculo as vin
                                                ON
                                                    pes.pes_id = vin.fk_pes1_id
                                                WHERE
                                                    vin.fk_per_id = 15 AND
                                                    vin.fk_tip_id IS NULL AND
                                                    vin.fk_sta_id = 1 AND
                                                    pes.fk_sta_id = 1 
                                                ORDER BY
                                                    pes.pes_nome_razao_social asc
                                            ';
        $this->data['DatasetAssociacao'] = $this->db->query($SqlAssociacao)->result();


        /* Raca */
        $SqlRaca                        = '
                                                SELECT * FROM tb_animal_raca as anr
                                                ORDER BY anr.anr_id ASC
                                            ';
        $this->data['DatasetRaca']      = $this->db->query($SqlRaca)->result();


        /* Pelagem */
        $SqlPelagem                     = '
                                                SELECT * FROM tb_animal_pelagem as anp
                                                ORDER BY anp.anp_id ASC
                                            ';
        $this->data['DatasetPelagem']   = $this->db->query($SqlPelagem)->result();


        /* Pais */
        $SqlPais                        = '
                                                SELECT * FROM tb_pais as pai
                                                ORDER BY pai.pai_posicao ASC
                                          ';
        $this->data['DatasetPais']      = $this->db->query($SqlPais)->result();


        /* Carrega View */
        $this->LoadTemplateGuest('template/user/sistema/animal/cadastro/formulario', $this->data);

    }


    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Ajax = false, $AjaxDados = false){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;


        /* Carregando modulos externos */
        $this->load->module('vinculo');


        // Setando Variaveis
        (!empty($Dados['escola-equitacao']) && $Dados['escola-equitacao'] > 0) ? $Dados['entidade-equestre'] = $Dados['escola-equitacao'] : $Dados['entidade-equestre'] = $Dados['entidade-filiada'];


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->Gravar($Dados);
        if($Gravar):        

            // ID do animal gerado
            $AnimalId                               =   $Gravar;


            // Gravar Proprietario
            $ProprietarioId     =   NULL;
            ($Dados['proprietario-flag'] == '1') ? $ProprietarioId = $this->session->userdata('PessoaId') : ''; 
            ($Dados['proprietario-flag'] == '2') ? $ProprietarioId = $Dados['proprietario-id'] : ''; 
            $GravarVinculoProprietario              =   $this->vinculo->GravarVinculoProprietarioAnimal($ProprietarioId, $AnimalId);


            // Gravar Responsavel Financeiro
            /*
            if(isset($Dados['responsavel-financeiro']))
                $this->vinculo->GravarResponsavelFinanceiroCompetidor($Pessoa['PessoaId'], NULL, $Dados['responsavel-financeiro']);
            */


            // Gravar Vinculo - Confederacao
            $GravarVinculoAtletaConfederacao        =   $this->vinculo->GravarVinculoAnimalConfederacao($Dados, $AnimalId);


            // Gravar Vinculo - Federacao
            $GravarVinculoAtletaFederacao           =   $this->vinculo->GravarVinculoAnimalFederacao($Dados, $AnimalId);


            // Gravar Vinculo - Entidade Equestre
            if($Dados['federacao'] == '141'){ // O vinculo entre atleta e entidade so acontece se a federacao escolhida por a FHBr. ID = 141 
                $GravarVinculoAtletaEntidade        =   $this->vinculo->GravarVinculoAnimalEntidade($Dados, $AnimalId);
                if(!$GravarVinculoAtletaEntidade)
                    return false;
            } 


            if(
                !$GravarVinculoProprietario ||
                !$GravarVinculoAtletaConfederacao ||
                !$GravarVinculoAtletaFederacao
            )
                return false; 



        endif;


        if(!$Gravar)
            return false;


        // Cadastro atleta finalizado com sucesso
        return true;

    }




    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->Processar(true, $Dados);
        echo json_encode($Processar);

    }


    /**
    * AjaxNumeroChipExiste
    *
    * @author Gustavo Botega 
    */
    public function AjaxNumeroChipExiste(){

        $NumeroChip         =   $_POST['NumeroChip'];
        $SqlNumeroChip      =   "
                                    SELECT * FROM tb_animal as ani
                                    WHERE ani_chip = '".$NumeroChip."'
                                ";
        $Query              =   $this->db->query($SqlNumeroChip)->result();        
        echo json_encode( (!empty($Query)) ? true : false );

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Nome Competicao

        // Matricula CBH

        // Matricula FEI

        // Federacao

        // Entidade


        return true;

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){

/*        // Genero Tipo
        (empty($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';

        // Associacao
        (empty($Dados['associacao'])) ? $Dados['associacao'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';
*/

        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }


        // Atleta Escola
        $AtletaEscola = NULL;
        if(isset($Dados['escola-equitacao'])){
            (!empty($Dados['escola-equitacao']) && $Dados['escola-equitacao'] > 0) ? $AtletaEscola = 1 : $AtletaEscola = NULL;
        }


        // Genero Tipo
        (!isset($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';




        // Dados
        $Dataset       =  array(
            
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    1,
            'fk_anr_id'                         =>    $Dados['raca'], // raca do animal
            'fk_anp_id'                         =>    $Dados['pelagem'], // pelagem do animal
            'fk_ang_id'                         =>    $Dados['genero'], // genero do animal
            'fk_agt_id'                         =>    $Dados['genero-tipo'], // genero tipo do animal
            'fk_ass_id'                         =>    $Dados['associacao'], // Associacao de Registro
            'fk_pai_id'                         =>    $Dados['pais-origem'], // Pais Origem
            'ani_chip'                          =>    $Dados['chip'], 
            'ani_passaporte'                    =>    $Dados['passaporte'], 
            'ani_data_nascimento'               =>    $this->my_data->ConverterData($Dados['data-nascimento'], 'PT-BR', 'ISO'), 
            'ani_nome_completo'                 =>    $Dados['nome-completo'], 
            'ani_nome_patrocinado'              =>    $Dados['nome-patrocinado'], 
            'ani_peso'                          =>    $Dados['peso'], 
            'ani_altura_cruz'                   =>    $Dados['altura-cruz'], 
            'ani_registro_fei'                  =>    $Dados['registro-fei'], 
            'ani_registro_capa_fei'             =>    $Dados['registro-capa-fei'], 
            'ani_registro_cbh'                  =>    $Dados['registro-cbh'], 
            'ani_registro_genealogico'          =>    $Dados['registro-genealogico'], 
            'ani_nome_pai'                      =>    $Dados['nome-pai'], 
            'ani_nome_mae'                      =>    $Dados['nome-mae'], 
            'ani_nome_avo_materno'              =>    $Dados['nome-avo-materno'], 
            'ani_escola'                        =>    $AtletaEscola, 
            'flag_permissao_autor'              =>    1, 
            'criado'                            =>    date("Y-m-d H:i:s")

        );


        /* Query */
        $Query = $this->db->insert('tb_animal', $Dataset);

        return ($Query) ? $this->db->insert_id() : false;

    }



    /**
    * JsonProcessar
    *
    * @author Gustavo Botega 
    */
    public function JsonProcessar(){

    }



    /**
    * ConsultarProprietario
    *
    * @author Gustavo Botega 
    */
    public function ConsultarProprietario(){


        $CpfCnpjProprietario        =   $_POST['CpfCnpjProprietario'];
        $CpfCnpjProprietario        =   $this->my_pessoa->RemoverPontuacaoCpfCnpj($CpfCnpjProprietario);


        $SqlConsultaCpfCnpj         =   "
                                        SELECT * FROM tb_pessoa as pes

                                        WHERE
                                            pes.pes_cpf_cnpj    = '".$CpfCnpjProprietario."' AND 
                                            pes.fk_sta_id       =  1
                                        ";
        $Query                      =   $this->db->query($SqlConsultaCpfCnpj)->result();
        
        $Dados                      =   array();
        if(!empty($Query)){
            $Dados['Status']                =   true;
            $Dados['StatusSlug']            =   'ProprietarioEncontrado';
            $Dados['PessoaId']              =   $Query[0]->pes_id;
            $Dados['PessoaNomeCompleto']    =   $Query[0]->pes_nome_razao_social;
            $Dados['PessoaCpfCnpj']         =   $this->my_pessoa->InserirPontuacaoCpfCnpj($Query[0]->pes_cpf_cnpj);

            if( $Query[0]->pes_cpf_cnpj == $this->session->userdata('PessoaCpf') )
                $Dados['StatusSlug']            =   'ProprietarioIgualAutor';


        }else{
            $Dados['Status']                =   false;
            $Dados['StatusSlug']            =   'ProprietarioNaoEncontrado';
        }


        echo json_encode($Dados);

    }





    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']   = TRUE;
        
            /*  Syles*/
    //            $this->data['StylesFile']['Styles']['profile']                 = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/cadastro-animal';
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/form-wizard-cadastro-animal';


    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

