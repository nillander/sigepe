<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';

class Dashboard extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

        $PessoaId                               =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
        $PessoaFisicaAtletaId                   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'fk_pef_id', $PessoaFisicaId, 1, 'pfa_id');
        $this->data['PessoaId']                 =   $PessoaId;        
        $this->data['PessoaFisicaId']           =   $PessoaFisicaId;        
        $this->data['PessoaFisicaAtletaId']     =   $PessoaFisicaAtletaId;        
        $this->data['DatasetPagarmeCheckout']   =   TRUE;        

        $this->data['ShowColumnLeft'] = "ProfileAnimal";

   
    }



    public function SendEmail(){

        $this->load->library('email');

        $this->email->from('your@example.com', 'Your Name');
        $this->email->to('gustavobotega@gmail.com.com');

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');

        $this->email->send();

    }





    /**
    * Dashboard
    *
    * @author Gustavo Botega 
    */
    public function Dashboard($AnimalId){

        $this->data['AnimalId']      = $AnimalId;



        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Animal';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['NavActiveSidebar']   = 'Dashboard';

#        $this->AnimalDataset();  // Carrega os valores do Perfil do Animal


       $this->data['PackageStyles'][]    =   'Packages/Styles/Animal/FrontOffice/Dashboard';
       $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Dashboard';




        $this->AnimalDatasetVinculos($AnimalId);  // Carrega dados
        $this->DatasetPagarme();  // Carrega dados para serem processados no gateway
        $this->AnimalDatasetRegistrosAtivos($AnimalId);  // Carrega dados
        $this->PermissaoAnimalNovoRegistro();  // Carrega dados
        $this->AnimalDatasetFederacaoEntidade( $this->data['PessoaId'] ,  $AnimalId, $this->data['PessoaFisicaAtletaId']);
        


        $this->data['RegistroCbh']              =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'pfa_registro_cbh');        
        $this->data['RegistroFei']              =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'pfa_registro_fei');        
        $this->data['NomeDeCompeticao']         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'pfa_nome_competicao');        

        $IdFederacaoAnimal                      =   $this->model_crud->get_rowSpecific('tb_animal', 'ani_id', $AnimalId, 1, 'fk_pjf_id');
        $this->data['AnimalFhbr']               =   ($IdFederacaoAnimal == 141) ? true : false;


        // ModalidadesRegistro
        $SqlModalidadesRegistro             =   "SELECT * FROM tb_evento_modalidade ";
        $this->data['ModalidadesRegistro']  =   $this->db->query($SqlModalidadesRegistro)->result();




        /* Carrega View */
        $this->LoadTemplate('Template/FrontOffice/sistema/Animal/Perfil/Dashboard', $this->data);

    }




    /**
    * AnimalDatasetVinculos
    *
    * @author Gustavo Botega 
    */
    public function AnimalDatasetVinculos($AnimalId){


            $PessoaId                               =   $this->session->userdata('PessoaId');
            $PessoaFisicaId                         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');
            $PessoaFisicaAtletaId                   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'fk_pef_id', $PessoaFisicaId, 1, 'pfa_id');
            $IdPessoa                               =   $PessoaId;
            $IdPessoaFisicaAtleta                   =   $PessoaFisicaAtletaId;


#            $this->data['FlagRegistroAtleta']   =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $IdPessoaFisicaAtleta, 1, 'flag_registro');
#            $this->data['AtletaEscola']         =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $IdPessoaFisicaAtleta, 1, 'pfa_escola');


            // Confederacao
            $QueryConfederacao    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id IS NOT NULL AND
                                              fk_pes2_id IS NULL AND
                                              fk_ani_id = ".$AnimalId." AND
                                              fk_per_id = 2 AND
                                              fk_tip_id = 10 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC;
                                ";
            $Confederacao  =   $this->db->query($QueryConfederacao)->result();
            $this->data['Confederacao']   =   $Confederacao;



            // Federacao
            $QueryFederacao    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id IS NOT NULL AND
                                              fk_pes2_id IS NULL AND
                                              fk_ani_id = ".$AnimalId." AND
                                              fk_per_id = 3 AND
                                              fk_tip_id = 10 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC;
                                ";
            $Federacao  =   $this->db->query($QueryFederacao)->result();
            $this->data['Federacao']   =   $Federacao;


            // Entidade 
            $QueryEntidade    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id IS NOT NULL AND
                                              fk_pes2_id IS NULL AND
                                              fk_ani_id = ".$AnimalId." AND
                                              fk_per_id = 4 AND
                                              fk_tip_id = 10 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC;
                                ";
            $Entidade  =   $this->db->query($QueryEntidade)->result();
            $this->data['Entidade']   =   $Entidade;



            /* Confederacao Atual */
            $QueryConfederacaoAtual    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id IS NOT NULL AND
                                              fk_pes2_id IS NULL AND
                                              fk_ani_id = ".$AnimalId." AND
                                              fk_per_id = 2 AND
                                              fk_tip_id = 10 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC
                                              LIMIT 1;
                                ";
            $ConfederacaoAtual                  =   $this->db->query($QueryConfederacaoAtual)->result();
            $this->data['ConfederacaoAtual']    =   $ConfederacaoAtual;


            /* Federacao Atual */
            $QueryFederacaoAtual    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id IS NOT NULL AND
                                              fk_pes2_id IS NULL AND
                                              fk_ani_id = ".$AnimalId." AND
                                              fk_per_id = 3 AND
                                              fk_tip_id = 10 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC
                                              LIMIT 1;
                                ";
            $FederacaoAtual                     =   $this->db->query($QueryFederacaoAtual)->result();
            $this->data['FederacaoAtual']       =   $FederacaoAtual;


            /* Entidade Atual */
            $QueryEntidadeAtual    =   "
                                    SELECT * FROM tb_vinculo
                                        WHERE fk_pes1_id IS NOT NULL AND
                                              fk_pes2_id IS NULL AND
                                              fk_ani_id = ".$AnimalId." AND
                                              fk_per_id = 4 AND
                                              fk_tip_id = 10 AND
                                              fk_sta_id IS NOT NULL
                                              ORDER BY vin_id DESC
                                              LIMIT 1;
                                ";
            $EntidadeAtual                     =   $this->db->query($QueryEntidadeAtual)->result();
            $this->data['EntidadeAtual']       =   $EntidadeAtual;



    }




    /**
    * AnimalDatasetRegistrosAtivos
    *
    * @author Gustavo Botega 
    */
    public function AnimalDatasetRegistrosAtivos($AnimalId){


            $QueryRegistrosAtivos    =   "
                                            SELECT * FROM
                                                tb_registro as reg

                                            INNER JOIN
                                                tb_financeiro as fin ON reg.fk_fin_id = fin.fin_id 

                                            WHERE reg.fk_sta_id IN(310, 311)  AND 
                                                  fin.fk_sta_id IN(100, 101) AND
                                                  reg.fk_rec_id = 2 AND
                                                  reg.fk_ani_id = '".$AnimalId."' AND
                                                  reg.fk_pes_id IS NULL

                                            ORDER BY reg_id DESC
                                "; 
            $this->data['RegistrosAtivos']          =   $this->db->query($QueryRegistrosAtivos)->result();


            $QueryPagamentoPendente    =   "
                                            SELECT * FROM
                                                tb_registro as reg

                                            INNER JOIN
                                                tb_financeiro as fin ON reg.fk_fin_id = fin.fin_id 

                                            WHERE reg.fk_sta_id IN(310)  AND 
                                                  fin.fk_sta_id IN(100) AND
                                                  reg.fk_rec_id = 2 AND
                                                  reg.fk_ani_id = '".$AnimalId."' AND
                                                  reg.fk_pes_id IS NULL

                                            ORDER BY reg_id DESC
                                "; 
            $Query                     =   $this->db->query($QueryPagamentoPendente)->result();
            (count($Query) > 0) ? $this->data['PagamentoPendente'] = true : $this->data['PagamentoPendente'] = false;



    }




    /**
    * PermissaoAnimalNovoRegistro
    *
    * Funcao checa se o atleta possui permissao para realizar o novo registro.
    * A regra e ter pelo menos um telefone e email associado a conta.
    *
    * @author Gustavo Botega 
    */
    public function PermissaoAnimalNovoRegistro(){


            $PessoaId                               =   $this->session->userdata('PessoaId');

            /* Telefone */
            $QueryTelefone    =   "
                                    SELECT * FROM tb_telefone
                                        WHERE fk_peo_id = ".$PessoaId." AND
                                              fk_sta_id = 1 AND
                                              flag_deletado IS NULL AND 
                                              fk_aut2_id IS NULL
                                "; 
            $DatasetTelefone          =   $this->db->query($QueryTelefone)->result();
            $this->data['QtdeTelefone'] =   count($DatasetTelefone);

            /* Email */
            $QueryEmail    =   "
                                    SELECT * FROM tb_email
                                        WHERE fk_peo_id = ".$PessoaId." AND
                                              fk_sta_id = 1 AND
                                              flag_deletado IS NULL AND 
                                              fk_aut2_id IS NULL
                                "; 
            $DatasetEmail          =   $this->db->query($QueryEmail)->result();
            $this->data['QtdeEmail'] =   count($DatasetEmail);

            /* Endereco */
            $QueryEndereco    =   "
                                    SELECT * FROM tb_endereco
                                        WHERE fk_peo_id = ".$PessoaId." AND
                                              fk_sta_id = 1 AND
                                              flag_deletado IS NULL AND 
                                              fk_aut2_id IS NULL
                                "; 
            $DatasetEndereco          =   $this->db->query($QueryEndereco)->result();
            $this->data['QtdeEndereco'] =   count($DatasetEndereco);



    }




    
    
    /*
    ---------------------------------------------------------------------
        
        TROCA DE FEDERACAO
    
    ====================================================================*/
    public function AjaxTrocaFederacao(){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->ProcessarTrocaFederacao(true, $Dados);

        echo json_encode($Processar);        
    }




    /**
    * TrocarFederacao
    *
    * @author Gustavo Botega 
    */
    public function ProcessarTrocaFederacao($Ajax = false, $AjaxDados = false){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;


        // Validacao Formulario
        $Validar                                    =   $this->ValidarTrocaFederacao($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->GravarTrocaFederacao($Dados);
        if(!$Gravar)
            return false;


        // Cadastro atualizado
        return true;        


    }

    
    /**
    * ValidarDadosAtleta
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function ValidarTrocaFederacao($Dados){

        // Federacao
        
        // Entidade

        return true;

    }


    /**
    * GravarTrocaFederacao
    *
    * @author Gustavo Botega 
    */
    public function GravarTrocaFederacao($Dados){

        $this->load->module('vinculo');

        $FederacaoAtualId              =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'fk_pjf_id');
        $FederacaoNovaId               =   $Dados['federacao'];
        $PessoaId                      =   $this->data['PessoaId'];
        $NomeCompleto                  =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social');
        
        // Obtendo o vinculo atual
        $SqlVinculoAtualFederacao                     = '
                                                SELECT * FROM
                                                    tb_vinculo as vin
                                            WHERE
                                                vin.fk_pes1_id = '.$PessoaId.' AND
                                                vin.fk_pes2_id = '.$FederacaoAtualId.' AND
                                                vin.fk_per_id = 3 AND
                                                vin.fk_tip_id = 9 AND
                                                vin.fk_sta_id IS NOT NULL
                                            ORDER BY vin.vin_id DESC 
                                            LIMIT 1
                                            ';
        $Query                              = $this->db->query($SqlVinculoAtualFederacao)->result();
        if(empty($Query)) return false;

        $VinculoAtualFederacaoId            = $Query[0]->vin_id;

        
        // Cancelando o vinculo atual
        $Dataset    =   array(
                            'vin_id'            =>  $VinculoAtualFederacaoId,
                            'fk_sta_id'         =>  49, // Vínculo cancelado pelo próprio usuário em virtude de transferencia de federacao.
                            'vih_historico'     =>  'Vinculado com a federaçao foi cancelado pelo usuario. Motivo: Transferencia de federacao. Usuario: ' . $NomeCompleto . ' ID: ' . $PessoaId,
                        );
        $this->vinculo->Atualizar($Dataset);
        

        
        // Inserindo o vinculo da nova federacao
        $Dataset    =   array(
                            'federacao'         =>  $FederacaoNovaId, 
                            'fk_sta_id'         =>  12, // [ Vínculos ] Atleta aguardando aprovação de vínculo pela Federação.
                        );
        $this->vinculo->GravarVinculoAtletaFederacao($Dataset, $PessoaId);

        
        
        // Atualizando vinculo na tabela do atleta. tb_pessoa_fisica_atleta
        $Dataset    =   array(
                            'fk_pjf_id'         =>  $FederacaoNovaId, 
                            'modificado'        =>  date("Y-m-d H:i:s"), 
                        );
        $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $Dataset, array('pfa_id' => $this->data['PessoaFisicaAtletaId'] ));        
        
        
        
        /*
            # MOVIMENTO DE SAIDA 
            A federacao atual e FHBr. Logo o movimento e de saida, ou seja, transferencia de federacao. Saindo de FHBr para outra federacao.
            Nessa situacao o procedimento e cancelar vinculo com ultima entidade valida e cancelar todos os registros
        */
        if($FederacaoAtualId    ==    '141'):  
            
            // Rotina para cancelar a ultima entidade equestre
            // Obtendo o vinculo atual
            $SqlVinculoAtualEntidadeEquestre   =    '
                                                        SELECT * FROM
                                                                tb_vinculo as vin
                                                        WHERE
                                                            vin.fk_pes1_id = '.$PessoaId.' AND
                                                            vin.fk_pes2_id IS NOT NULL AND
                                                            vin.fk_ani_id IS NULL AND
                                                            vin.fk_per_id = 4 AND
                                                            vin.fk_tip_id = 9 AND
                                                            vin.fk_sta_id IS NOT NULL
                                                        ORDER BY vin.vin_id DESC 
                                                        LIMIT 1
                                                    ';
            $Query                              =   $this->db->query($SqlVinculoAtualEntidadeEquestre)->result();                
            $VinculoAtualEntidadeEquestreId     =   $Query[0]->vin_id;
            
            // Cancelando vinculo
            $DatasetCancelarEntidade    =   array(
                                'vin_id'            =>  $VinculoAtualEntidadeEquestreId,
                                'fk_sta_id'         =>  49, // Vínculo cancelado pelo próprio usuário em virtude da transferencia de federacao.
                                'vih_historico'     =>  'Vinculado com a entidade equestre foi cancelado pelo usuario. Motivo: Transferencia de federação. Anula o vinculo com a entidade equestre. Usuario: ' . $NomeCompleto . ' ID: ' . $PessoaId,
                            );
            $this->vinculo->Atualizar($DatasetCancelarEntidade);

        
            // Atualizando vinculo na tabela do atleta. tb_pessoa_fisica_atleta
            $DatasetCancelarAtleta    =   array(
                                'fk_pje_id'         =>  NULL, 
                                'modificado'        =>  date("Y-m-d H:i:s"), 
                            );
            $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $DatasetCancelarAtleta, array('pfa_id' => $this->data['PessoaFisicaAtletaId'] ));        

        
        
            // Rotina para cancelar todos os registros ativos
            // Implementar
            
        endif;
        
        
        
        /*
            # MOVIMENTO DE ENTRADA
            A federacao nova sera a FHBr.
            Nessa situacao o procedimento e realizar o vinculo com a entidade selecionada.
        */
        if($FederacaoNovaId    ==    '141'): // FHBr ID 
            
            $EntidadeEquestreId         =   $Dados['federacao-entidade-equestre'];

            // Inserindo o vinculo da nova federacao
            $Dataset    =   array(
                                'entidade-equestre'         =>  $EntidadeEquestreId, 
                                'fk_sta_id'                 =>  20, // [ Vínculos ] Atleta aguardando aprovação de vínculo pela Entidade Equestre.
                            );
            $this->vinculo->GravarVinculoAtletaEntidade($Dataset, $PessoaId);


            // Atualizando vinculo na tabela do atleta. tb_pessoa_fisica_atleta
            $DatasetAtleta    =   array(
                                'fk_pje_id'         =>  $EntidadeEquestreId, 
                                'modificado'        =>  date("Y-m-d H:i:s"), 
                            );
            $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $DatasetAtleta, array('pfa_id' => $this->data['PessoaFisicaAtletaId'] ));        
        
        endif;
        
        return true;
        
    }



    
    
    
    /*
    ---------------------------------------------------------------------
        
        TROCA DE ENTIDADE
    
    ====================================================================*/
    public function AjaxTrocaEntidade(){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->ProcessarTrocaEntidade(true, $Dados);

        echo json_encode($Processar);        
    }




    /**
    * ProcessarTrocaEntidade
    *
    * @author Gustavo Botega 
    */
    public function ProcessarTrocaEntidade($Ajax = false, $AjaxDados = false){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;


        // Validacao Formulario
        $Validar                                    =   $this->ValidarTrocaEntidade($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->GravarTrocaEntidade($Dados);
        if(!$Gravar)
            return false;


        // Cadastro atualizado
        return true;        


    }

    
    /**
    * ValidarTrocaEntidade
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function ValidarTrocaEntidade($Dados){

        // Entidade - entidade deve ser diferente da atual, nao pode ser null, deve ser uma que existe.

        return true;

    }


    /**
    * GravarTrocaEntidade
    *
    * @author Gustavo Botega 
    */
    public function GravarTrocaEntidade($Dados){

        $this->load->module('vinculo');

        $EntidadeAtualId               =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica_atleta', 'pfa_id', $this->data['PessoaFisicaAtletaId'], 1, 'fk_pje_id');
        $EntidadeNovaId                =   $Dados['entidade-equestre'];
        $PessoaId                      =   $this->data['PessoaId'];
        $NomeCompleto                  =   $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social');
        
        // Obtendo o vinculo atual
        $SqlVinculoAtualEntidade       = '
                                                SELECT * FROM
                                                    tb_vinculo as vin
                                            WHERE
                                                vin.fk_pes1_id = '.$PessoaId.' AND
                                                vin.fk_pes2_id = '.$EntidadeAtualId.' AND
                                                vin.fk_per_id = 4 AND
                                                vin.fk_tip_id = 9 AND
                                                vin.fk_sta_id IS NOT NULL
                                            ORDER BY vin.vin_id DESC 
                                            LIMIT 1
                                            ';
        $Query                              = $this->db->query($SqlVinculoAtualEntidade)->result();
        if(empty($Query)) return false;

        $VinculoAtualEntidadeId            = $Query[0]->vin_id;

        
        // Cancelando o vinculo atual
        $Dataset    =   array(
                            'vin_id'            =>  $VinculoAtualEntidadeId,
                            'fk_sta_id'         =>  49, // Vínculo cancelado pelo próprio usuário em virtude de transferencia de entidade.
                            'vih_historico'     =>  'Vinculado com a federaçao foi cancelado pelo usuario. Motivo: Transferencia de entidade. Usuario: ' . $NomeCompleto . ' ID: ' . $PessoaId,
                        );
        $this->vinculo->Atualizar($Dataset);
        

        
        // Inserindo o vinculo da nova entidade
        $Dataset    =   array(
                            'entidade-equestre' =>  $EntidadeNovaId, 
                            'fk_sta_id'         =>  20, // [ Vínculos ] Atleta aguardando aprovação de vínculo pela Entidade Equestre.
                        );
        $this->vinculo->GravarVinculoAtletaEntidade($Dataset, $PessoaId);

        
        
        // Atualizando vinculo na tabela do atleta. tb_pessoa_fisica_atleta
        $Dataset    =   array(
                            'fk_pje_id'         =>  $EntidadeNovaId, 
                            'modificado'        =>  date("Y-m-d H:i:s"), 
                        );
        $Query    =   $this->db->update('tb_pessoa_fisica_atleta', $Dataset, array('pfa_id' => $this->data['PessoaFisicaAtletaId'] ));        
        
        
        
        return true;
        
    }












    /**
    * AnimalDatasetFederacaoEntidade
    *
    * @author Gustavo Botega 
    */
    public function AnimalDatasetFederacaoEntidade($PessoaId, $AnimalId, $PessoaFisicaAtletaId){

        
        $FederacaoAtualId   =   $this->model_crud->get_rowSpecific('tb_animal', 'ani_id', $AnimalId, 1, 'fk_pjf_id');
        $EntidadeAtualId    =   $this->model_crud->get_rowSpecific('tb_animal', 'ani_id', $AnimalId, 1, 'fk_pje_id');
        
        
        
        //  Lista todas as federacoes exceto a que ele esta associado.
        $SqlFederacao                     = '
                                                SELECT * FROM
                                                    tb_pessoa as pes
                                                INNER JOIN
                                                    tb_vinculo as vin
                                                ON
                                                    pes.pes_id = vin.fk_pes1_id
                                            WHERE
                                                    vin.fk_per_id = 3 AND
                                                    vin.fk_tip_id IS NULL AND
                                                    vin.fk_sta_id = 1 AND
                                                    pes.fk_sta_id = 1 AND
                                                    pes.pes_id != "'.$FederacaoAtualId.'"
                                                ORDER BY
                                                    pes_nome_razao_social asc
                                            ';
        $this->data['DatasetFederacao']   = $this->db->query($SqlFederacao)->result();



        /* Entidade Filiada */
        $SqlFederacaoEntidadeFiliada      = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_escola_equitacao = 1

                                                ORDER BY pes.pes_nome_razao_social ASC

                                            ';
        $this->data['DatasetFederacaoEntidadeFiliada']   = $this->db->query($SqlFederacaoEntidadeFiliada)->result();

        
        /* Entidade Filiada */
        if(!is_null($EntidadeAtualId)):
        $SqlEntidadeFiliada                     = '
                                                SELECT * FROM tb_pessoa as pes

                                                INNER JOIN tb_vinculo as vin
                                                ON pes.pes_id = vin.fk_pes1_id

                                                INNER JOIN tb_pessoa_juridica as pej
                                                ON pej.fk_pes_id = pes.pes_id


                                                INNER JOIN tb_pessoa_juridica_entidade as pje
                                                ON pje.fk_pej_id = pej.pej_id


                                                WHERE vin.fk_per_id = 4 AND 
                                                vin.fk_sta_id = 1 AND
                                                vin.fk_tip_id IS NULL AND
                                                pes.fk_sta_id = 1 AND
                                                pje.flag_escola_equitacao = 1 AND
                                                pes.pes_id != '.$EntidadeAtualId.'

                                                ORDER BY pes.pes_nome_razao_social ASC

                                            ';
        $this->data['DatasetEntidadeFiliada']   = $this->db->query($SqlEntidadeFiliada)->result();
        endif;


        
        


    }








    /**
    * JsonProcessar
    *
    * @author Gustavo Botega 
    */
    public function JsonProcessar(){

    }





    /**
    * DatasetPagarme ( levar esse bloco pra camada do pagarme )
    *
    * @author Gustavo Botega 
    */
    public function DatasetPagarme(){

        $PessoaId                               =       $this->data['PessoaId'];

        $this->data['PagarmeNome']              =       $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_nome_razao_social');
        $this->data['PagarmeCpf']               =       $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_cpf_cnpj');
        $this->data['PagarmeDataNascimento']    =       $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $PessoaId, 1, 'pes_data_nascimento_fundacao');

        // Obtendo o Email Principal
        $this->data['PagarmeEmail']             =       '';
        $SqlPagarmeEmail         =   "
                                        SELECT * FROM tb_email as ema

                                        WHERE
                                            ema.fk_peo_id    = '".$PessoaId."' AND 
                                            ema.flag_deletado IS NULL and 
                                            ema.fk_sta_id = 1 AND 
                                            ema.fk_aut2_id IS NULL

                                        LIMIT 1
                                        ";
        $QueryPagarmeEmail       =   $this->db->query($SqlPagarmeEmail)->result();
        if(!empty($QueryPagarmeEmail))
            $this->data['PagarmeEmail']             =       $QueryPagarmeEmail[0]->ema_email;



        // Obtendo o Telefone Principal
        $this->data['PagarmeTelefonePrincipal']             =       '';
        $SqlPagarmeTelefone         =   "
                                        SELECT * FROM tb_telefone as tel

                                        WHERE
                                            tel.fk_peo_id    = '".$PessoaId."' AND 
                                            tel.flag_deletado IS NULL and 
                                            tel.fk_sta_id = 1 AND 
                                            tel.fk_aut2_id IS NULL

                                        LIMIT 1
                                        ";
        $QueryPagarmeTelefone       =   $this->db->query($SqlPagarmeTelefone)->result();
        if(!empty($QueryPagarmeTelefone)){
            $Ddd                                                =       (string)$QueryPagarmeTelefone[0]->tel_ddd;
            $Telefone                                           =       str_replace("-", "", $QueryPagarmeTelefone[0]->tel_telefone);
            $this->data['PagarmeTelefonePrincipal']             =       $Ddd . (string)$Telefone ;
        }



        // Obtendo o Endereco Principal
        $this->data['PagarmeEndereco']          =       '';
        $SqlPagarmeEndereco         =   "
                                        SELECT * FROM tb_endereco as end

                                        WHERE
                                            end.fk_peo_id    = '".$PessoaId."' AND 
                                            end.flag_deletado IS NULL and 
                                            end.fk_sta_id = 1 AND 
                                            end.fk_aut2_id IS NULL

                                        LIMIT 1
                                        ";
        $QueryPagarmeEndereco       =   $this->db->query($SqlPagarmeEndereco)->result();
        if(!empty($QueryPagarmeEndereco)){
            $this->data['PagarmeCep']                   =       $QueryPagarmeEndereco[0]->end_cep;
            $this->data['PagarmeEstadoSigla']           =       $this->model_crud->get_rowSpecific('tb_estado', 'est_id', $QueryPagarmeEndereco[0]->fk_est_id, 1, 'est_sigla');
            $this->data['PagarmeCidade']                =       $this->model_crud->get_rowSpecific('tb_cidade', 'cid_id', $QueryPagarmeEndereco[0]->fk_cid_id, 1, 'cid_nome');
            $this->data['PagarmeBairro']                =       $QueryPagarmeEndereco[0]->end_bairro;
            $this->data['PagarmeLogradouro']            =       $QueryPagarmeEndereco[0]->end_logradouro;
            $this->data['PagarmeNumero']                =       $QueryPagarmeEndereco[0]->end_numero;
            $this->data['PagarmeComplemento']           =       $QueryPagarmeEndereco[0]->end_complemento;
        }






    }






    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                   = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }




    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 

        /* Carregando Estilos */
       $this->data['PackageStyles'][]    =   'Packages/Styles/Perfil/FrontOffice/Atleta';

        /* Carregando Scripts */
       $this->data['PackageScripts'][]      =   'Packages/Scripts/Perfil/FrontOffice/Atleta';
       $this->data['PackageScripts'][]      =   'Packages/Scripts/Competidor/FrontOffice/Vinculo';
       $this->data['PackageScripts'][]      =   'Packages/Scripts/Competidor/FrontOffice/VinculoFederacao';
       $this->data['PackageScripts'][]      =   'Packages/Scripts/Competidor/FrontOffice/VinculoEntidade';
       $this->data['PackageScripts'][]      =   'Packages/Scripts/Competidor/FrontOffice/Gateway';
       $this->data['PackageScripts'][]      =   'Packages/Scripts/Competidor/FrontOffice/Registro';

    }


}
