<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
//require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
class Perfil extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 


    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
        $this->Dashboard();
    }


    /**
    * Resetar Senha
    *
    * @author Gustavo Botega 
    */
    public function ResetarSenha() {

        $this->load->library('email');

        $subject = 'This is a test';
        $message = '<p>This message has been sent for testing purposes.</p>';

        // Get full html:
        $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
            <title>' . html_escape($subject) . '</title>
            <style type="text/css">
                body {
                    font-family: Arial, Verdana, Helvetica, sans-serif;
                    font-size: 16px;
                }
            </style>
        </head>
        <body>
        ' . $message . '
        </body>
        </html>';
        // Also, for getting full html you may use the following internal method:
        //$body = $this->email->full_html($subject, $message);

        $result = $this->email
            ->from('gmbotega@gmail.com')
            ->to('gustavobotega@gmail.com')
            ->subject($subject)
            ->message($body)
            ->send();


        var_dump($result);
        echo '<br />';
        echo $this->email->print_debugger();

        exit;

    }




    /**
    * Resetar Senha
    *
    * @author Gustavo Botega 
    */
    public function Resetar() {


        var_dump("ts4");



        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'mendesbotega@gmail.com',
            'smtp_pass' => 'brew6k3jgg',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1'
        );
        $this->load->library('email', $config);

        $this->email->set_newline("\r\n");


        $this->email->from("mendesbotega@gmail.com", "FHBr - Mensagem de boas vindas " . date("Y-m-d H:i:s"));
        $this->email->to("gustavobotega@gmail.com");


        $this->email->subject('Finalmente');

        $mesg = $this->load->view('Template/email','',true);

        $this->email->message($mesg);


        
        $this->email->send();



        var_dump($this->email->print_debugger());

    }



    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Dashboard(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['PageHeadTitle']      = 'Meu Perfil';
        $this->data['PageHeadSubtitle']   = 'Visualize e gerencie seu perfil. Mantenha seus dados sempre atualizados.';
        $this->data['Breadcrumbs']        = array();
        $this->data['NavActiveSidebar']   = 'Dashboard';


        /* Carrega View */
        $this->LoadTemplateProfilePessoa('Template/FrontOffice/sistema/Perfil/Dashboard', $this->data);

    }





    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Ajax = false, $AjaxDados = false){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;




        /* Carregando modulos externos */
        $this->load->module('vinculo');


        // Setando Variaveis
        (!empty($Dados['escola-equitacao']) && $Dados['escola-equitacao'] > 0) ? $Dados['entidade-equestre'] = $Dados['escola-equitacao'] : $Dados['entidade-equestre'] = $Dados['entidade-filiada'];


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->Gravar($Dados);
        if($Gravar):        

            // ID do animal gerado
            $AnimalId                               =   $Gravar;


            // Gravar Proprietario
            $ProprietarioId     =   NULL;
            ($Dados['proprietario-flag'] == '1') ? $ProprietarioId = $this->session->userdata('PessoaId') : ''; 
            ($Dados['proprietario-flag'] == '2') ? $ProprietarioId = $Dados['proprietario-id'] : ''; 
            $GravarVinculoProprietario              =   $this->vinculo->GravarVinculoProprietarioAnimal($ProprietarioId, $AnimalId);


            // Gravar Responsavel Financeiro
            /*
            if(isset($Dados['responsavel-financeiro']))
                $this->vinculo->GravarResponsavelFinanceiroCompetidor($Pessoa['PessoaId'], NULL, $Dados['responsavel-financeiro']);
            */



            // Gravar Passaporte
            $GravarPassaporte                       =   $this->GravarPassaporte($Dados, $AnimalId);





            // Gravar Vinculo - Confederacao
            $GravarVinculoAtletaConfederacao        =   $this->vinculo->GravarVinculoAnimalConfederacao($Dados, $AnimalId);


            // Gravar Vinculo - Federacao
            $GravarVinculoAtletaFederacao           =   $this->vinculo->GravarVinculoAnimalFederacao($Dados, $AnimalId);


            // Gravar Vinculo - Entidade Equestre
            if($Dados['federacao'] == '141'){ // O vinculo entre atleta e entidade so acontece se a federacao escolhida por a FHBr. ID = 141 
                $GravarVinculoAtletaEntidade        =   $this->vinculo->GravarVinculoAnimalEntidade($Dados, $AnimalId);
                if(!$GravarVinculoAtletaEntidade)
                    return false;
            } 




            if(
                !$GravarVinculoProprietario ||
                !$GravarPassaporte ||
                !$GravarVinculoAtletaConfederacao ||
                !$GravarVinculoAtletaFederacao
            )
                return false; 



        endif;


        if(!$Gravar)
            return false;


        // Cadastro atleta finalizado com sucesso
        return true;

    }




    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->Processar(true, $Dados);


        echo json_encode($Processar);

    }


    /**
    * AjaxNumeroChipExiste
    *
    * @author Gustavo Botega 
    */
    public function AjaxNumeroChipExiste(){

        $NumeroChip         =   $_POST['NumeroChip'];
        $SqlNumeroChip      =   "
                                    SELECT * FROM tb_animal as ani
                                    WHERE ani_chip = '".$NumeroChip."'
                                ";
        $Query              =   $this->db->query($SqlNumeroChip)->result();        
        echo json_encode( (!empty($Query)) ? true : false );

    }


    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Nome Competicao

        // Matricula CBH

        // Matricula FEI

        // Federacao

        // Entidade


        return true;

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){

/*        // Genero Tipo
        (empty($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';

        // Associacao
        (empty($Dados['associacao'])) ? $Dados['associacao'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';

        // Pais de Origem
        (empty($Dados['pais-origem'])) ? $Dados['pais-origem'] = NULL : '';
*/

        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }


        // Atleta Escola
        $AtletaEscola = NULL;
        if(isset($Dados['escola-equitacao'])){
            (!empty($Dados['escola-equitacao']) && $Dados['escola-equitacao'] > 0) ? $AtletaEscola = 1 : $AtletaEscola = NULL;
        }


        // Genero Tipo
        (!isset($Dados['genero-tipo'])) ? $Dados['genero-tipo'] = NULL : '';

        // Genero 
        (!isset($Dados['genero'])) ? $Dados['genero'] = NULL : '';




        // Dados
        $Dataset       =  array(
            
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    1,
            'fk_anr_id'                         =>    $Dados['raca'], // raca do animal
            'fk_anp_id'                         =>    $Dados['pelagem'], // pelagem do animal
            'fk_ang_id'                         =>    $Dados['genero'], // genero do animal
            'fk_agt_id'                         =>    $Dados['genero-tipo'], // genero tipo do animal
            'fk_ass_id'                         =>    $Dados['associacao'], // Associacao de Registro
            'fk_pai_id'                         =>    $Dados['pais-origem'], // Pais Origem
            'ani_chip'                          =>    $Dados['chip'], 
            'ani_data_nascimento'               =>    $this->my_data->ConverterData($Dados['data-nascimento'], 'PT-BR', 'ISO'), 
            'ani_nome_completo'                 =>    $Dados['nome-completo'], 
            'ani_nome_patrocinado'              =>    $Dados['nome-patrocinado'], 
            'ani_peso'                          =>    $Dados['peso'], 
            'ani_altura_cruz'                   =>    $Dados['altura-cruz'], 
            'ani_registro_fei'                  =>    $Dados['registro-fei'], 
            'ani_registro_capa_fei'             =>    $Dados['registro-capa-fei'], 
            'ani_registro_cbh'                  =>    $Dados['registro-cbh'], 
            'ani_registro_genealogico'          =>    $Dados['registro-genealogico'], 
            'ani_nome_pai'                      =>    $Dados['nome-pai'], 
            'ani_nome_mae'                      =>    $Dados['nome-mae'], 
            'ani_nome_avo_materno'              =>    $Dados['nome-avo-materno'], 
            'ani_escola'                        =>    $AtletaEscola, 
            'flag_permissao_autor'              =>    1, 
            'criado'                            =>    date("Y-m-d H:i:s")

        );

 
        /* Query */
        $Query = $this->db->insert('tb_animal', $Dataset);

        return ($Query) ? $this->db->insert_id() : false;

    }





    /**
    * GravarPassaporte
    *
    * @author Gustavo Botega 
    */
    public function GravarPassaporte($Dados, $AnimalId){


        // Dados
        $Dataset       =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_ani_id'                         =>    $AnimalId,
            'fk_sta_id'                         =>    1,
            'anp_passaporte'                    =>    $Dados['passaporte'],
            'criado'                            =>    date("Y-m-d H:i:s")
        );
        $Query = $this->db->insert('tb_animal_passaporte', $Dataset);

        if($Query){
            $PassaporteId   =   $this->db->insert_id();
        }else{
            return false;
        }


        /* Atualizando na coluna fk_pas_id da tabela do animal o ID do passaporte gerado */
        $SqlUpdate         =   "
                               UPDATE tb_animal SET fk_pas_id = ".$PassaporteId." WHERE ani_id = ".$AnimalId."
                               ";
        $this->db->query($SqlUpdate);

        return true;


    }








    /**
    * JsonProcessar
    *
    * @author Gustavo Botega 
    */
    public function JsonProcessar(){

    }



    /**
    * ConsultarProprietario
    *
    * @author Gustavo Botega 
    */
    public function ConsultarProprietario(){


        $CpfCnpjProprietario        =   $_POST['CpfCnpjProprietario'];
        $CpfCnpjProprietario        =   $this->my_pessoa->RemoverPontuacaoCpfCnpj($CpfCnpjProprietario);


        $SqlConsultaCpfCnpj         =   "
                                        SELECT * FROM tb_pessoa as pes

                                        WHERE
                                            pes.pes_cpf_cnpj    = '".$CpfCnpjProprietario."' AND 
                                            pes.fk_sta_id       =  1
                                        ";
        $Query                      =   $this->db->query($SqlConsultaCpfCnpj)->result();
        
        $Dados                      =   array();
        if(!empty($Query)){
            $Dados['Status']                =   true;
            $Dados['StatusSlug']            =   'ProprietarioEncontrado';
            $Dados['PessoaId']              =   $Query[0]->pes_id;
            $Dados['PessoaNomeCompleto']    =   $Query[0]->pes_nome_razao_social;
            $Dados['PessoaCpfCnpj']         =   $this->my_pessoa->InserirPontuacaoCpfCnpj($Query[0]->pes_cpf_cnpj);

            if( $Query[0]->pes_cpf_cnpj == $this->session->userdata('PessoaCpf') )
                $Dados['StatusSlug']            =   'ProprietarioIgualAutor';


        }else{
            $Dados['Status']                =   false;
            $Dados['StatusSlug']            =   'ProprietarioNaoEncontrado';
        }


        echo json_encode($Dados);

    }



    /**
    * JsonProprietario
    *
    * @author Gustavo Botega 
    */
    public function JsonProprietario(){


        $QueryJson         =   "
                                        SELECT * FROM tb_pessoa as pes

                                        WHERE
                                            pes.pes_natureza    =  'PF' AND
                                            pes.fk_sta_id       =  1
                                        ";
        $Query                      =   $this->db->query($QueryJson)->result();
                

        $array  = array();
        foreach ($Query as $key => $value) {

            $array[]       =  array(
                                'name'      => strtoupper($value->pes_nome_razao_social),
                                'id'        => $value->pes_id,
                            ) ;

        }


/*
            $array[]['name']    =   $value->pes_nome_razao_social;
            $array[]['id']      =   $value->pes_id;

        foreach ($Query as $key => $value) {
            array(
                'name'  => $value->pes_nome_razao_social,
                'id'    => $value->pes_id
            );
        }
*/

        echo json_encode($array);

       // echo json_encode($Dados);

    }





    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
        
            /*  Syles*/
               $this->data['StylesFile']['Styles']['profile']                                   = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                            = TRUE;

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
//        $this->data['PackageStyles'][]    =   'Packages/Styles/Animal/FrontOffice/Cadastro';


        /* Carregando Scripts */
//        $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Animal';




    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

