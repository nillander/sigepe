<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';

class Listagem extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        
    }


    public function SendEmail(){

        $this->load->library('email');

        $this->email->from('your@example.com', 'Your Name');
        $this->email->to('gustavobotega@gmail.com.com');

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');

        $this->email->send();

    }


    public function index() {

       	$this->data['PageHeadTitle']      = 'Meus Animais';
		$this->data['PageHeadSubtitle']   = 'Relação de todos os animais que você possui algum vínculo.';
		$UsuarioId 						=	$this->session->userdata('PessoaId');


		$SqlAnimaisAutor 				=	'
                                                SELECT * FROM tb_animal as ani

                                                WHERE ani.fk_aut_id = '.$UsuarioId.' 
                                                ORDER BY ani.ani_id DESC
											';

        $this->data['AnimaisAutor']   	= $this->db->query($SqlAnimaisAutor)->result();




		$SqlAnimaisProprietario 				=	'

	                                                SELECT * FROM tb_animal as ani

	                                                INNER JOIN tb_vinculo as vin
	                                                ON ani.ani_id = vin.fk_ani_id

	                                                WHERE vin.fk_pes1_id = '.$UsuarioId.' 
	                                                ORDER BY ani.ani_id DESC

												';

        $this->data['AnimaisProprietario']   	= $this->db->query($SqlAnimaisProprietario)->result();



        $this->LoadTemplateUser('Template/FrontOffice/sistema/Animal/Listagem/Index', $this->data);

    }




}
