<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class PerfilCompleto extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 


    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
    }



    /**
    * AjaxProcessar
    *
    * @author Gustavo Botega 
    */
    public function AjaxProcessar(){


        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);    

        $Processar  =   $this->Processar(true, $Dados);


        echo json_encode($Processar);

    }




    /**
    * Processar
    *
    * @author Gustavo Botega 
    */
    public function Processar($Ajax = false, $AjaxDados = false){

        $ArrayDataSerialized        =   $_POST['DataSerialized'];
        parse_str($ArrayDataSerialized, $Dados);                       

        if($AjaxDados)
            $Dados  =   $AjaxDados;


        // Validacao Formulario
        $Validar                                    =   $this->Validar($Dados);
        if(!$Validar)
            return false;


        $Gravar                                     =   $this->Gravar($Dados);

        if(!$Gravar)
            return false;


        // Cadastro atualizado
        return true;

    }



    /**
    * Validar
    *
    *
    * @author Gustavo Botega 
    * @return {type}
    */
    public function Validar($Dados){

        // Apelido

        // Estado Civil

        // Nacionalidade

        // Naturalidade

        // Tipo Sanguineo

        // Escolaridade

        // Nome Pai

        // Nome Mae

        return true;

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($Dados){


        foreach ($Dados as $key => $value) {
            if(empty($value))
                $Dados[$key]    =   NULL;
        }


        // Dados
        $Dataset       =  array(

            'pef_apelido'                       =>    $Dados['apelido'], // Apelido
            'fk_nac_id'                         =>    $Dados['nacionalidade'], // Nacionalidade
            'fk_esc_id'                         =>    $Dados['escolaridade'], // Escolaridade
            'fk_tip_id'                         =>    $Dados['tipo-sanguineo'], // Tipo Sanguineo
            'pef_naturalidade'                  =>    $Dados['naturalidade'], // Naturalidade
            'fk_est_id'                         =>    $Dados['estado-civil'], // Estado Civil
            'pef_nome_pai'                      =>    $Dados['nome-pai'], // Nome Pai
            'pef_nome_mae'                      =>    $Dados['nome-mae'], // Nome Mae

            'modificado'                            =>    date("Y-m-d H:i:s")

        );

 
        $PessoaId                           =   $this->session->userdata('PessoaId');
        $PessoaFisicaId                     =   $this->model_crud->get_rowSpecific('tb_pessoa_fisica', 'fk_pes_id', $PessoaId, 1, 'pef_id');

        /* Query */
        $Query = $this->db->update('tb_pessoa_fisica', $Dataset, "pef_id = " . $PessoaFisicaId);

        return ($Query) ? true : false;

    }


}
