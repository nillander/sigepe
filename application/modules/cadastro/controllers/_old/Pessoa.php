<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_GuestOffice.php';
//require APPPATH."modules/events/controllers/events.php";

class Pessoa extends MY_Controller {

    function __construct() {
        parent::__construct();

    }


    public function index() {}







    /*
     * PeopleInsertGeneralInfo
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function PeopleInsertGeneralInfo() {

        // Carregando modulo
        $this->load->module('authentication');
        $authentication     =   $this->authentication->settings();
        $token              =   $authentication['TokenAuthentication'];


        /**
         * Documentar - Unserialize Data ( Convert string to Arr Object PHP in $searchArray )
         */
        $dataSerialized      =  $_POST['DataSerialized'];
        parse_str($dataSerialized, $dataSet);


        /* Percorrer todo dataSet, onde houver vazio setar como Null */
        foreach ($dataSet as $key => $value) {
            if(empty($value)){
                $value = NULL;
            }
            $dataSet[$key]   =   $value;
        }


        /* Gender */
        if( $dataSet['gender'] == '1'){
            $avatar     =   'avatar-male.png';
        }elseif ( $dataSet['gender'] == '2') {
            $avatar     =   'avatar-female.png';
        }else{
            $avatar     =   'avatar-default.png';
        }


        $dataSetInsert       =  array(
                'peo_cpf'           =>    MaskCpf( $dataSet['cpf'], false ),
                'peo_birthDate'     =>    $this->my_date->date( $dataSet['birthDate'], 'ptbr', 'convertPtbrToIso' ),
                'peo_password'      =>    md5( $token . $dataSet['password'] ),
                'peo_fullName'      =>    $dataSet['fullName'],
                'peo_nickname'      =>    $dataSet['nickname'],
                'peo_avatar'        =>    $avatar,
                'peo_nameFather'    =>    $dataSet['nameFather'],
                'peo_nameMother'    =>    $dataSet['nameMother'],
                'peo_occupation'    =>    $dataSet['occupation'],
                'fk_nat_id'         =>    $this->model_crud->get_rowSpecific('tb_nationality', 'nat_acronym', $dataSet['nationality'], 1, 'nat_id'),
                'fk_blo_id'         =>    $dataSet['bloodType'],
                'fk_gen_id'         =>    $dataSet['gender'],
                'fk_mar_id'         =>    $dataSet['maritalStatus'],
                'fk_sch_id'         =>    $dataSet['scholarity'],
                'fk_sta_id'         =>    1,
                /*'fk_bir_id'         =>    $dataSet['birthplace'],*/
                'createdAt'         =>    date("Y-m-d H:i:s")
        );



        /*
            Gravar na ultima etapa o email
        */


        /* ANTES DE EXECUTAR A QUERY CHECAR SE O CPF JA EXISTE, SE SIM, RETORNAR ERRO */


        /**
         * Query
         */
        $query = $this->db->insert('tb_people', $dataSetInsert);
        if($query)
        {

            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("d/m/Y") . " às " . date("H:i:s"),
                'ip'        =>  $this->input->ip_address(),
                'peo_id'    =>  $this->db->insert_id()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed',
                'datetime'  =>  date("d/m/Y") . " às " . date("H:i:s"),
                'ip'        =>  $this->input->ip_address(),
            );
        }        
        echo json_encode($array);

    }






    /*
     * PeopleUpdateGeneralInfo
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function PeopleUpdateGeneralInfo() {


        // Carregando modulo
        $this->load->module('authentication');
        $authentication     =   $this->authentication->settings();
        $token              =   $authentication['TokenAuthentication'];


        /**
         * Documentar - Unserialize Data ( Convert string to Arr Object PHP in $searchArray )
         */
        $dataSerialized      =  $_POST['DataSerialized'];
        parse_str($dataSerialized, $dataSet);
        $PeopleId            =  $_POST['PeopleId'];


        /* Percorrer todo dataSet, onde houver vazio setar como Null */
        foreach ($dataSet as $key => $value) {
            if(empty($value)){
                $value = NULL;
            }
            $dataSet[$key]   =   $value;
        }

        /* Gender */
        if( $dataSet['gender'] == '1'){
            $avatar     =   'avatar-male.png';
        }elseif ( $dataSet['gender'] == '2') {
            $avatar     =   'avatar-female.png';
        }

        $dataSetUpdate       =  array(
                'peo_password'      =>    md5( $token . $dataSet['password'] ),
                'peo_fullName'      =>    $dataSet['fullName'],
                'peo_nickname'      =>    $dataSet['nickname'],
                'peo_nameFather'    =>    $dataSet['nameFather'],
                'peo_nameMother'    =>    $dataSet['nameMother'],
                'peo_occupation'    =>    $dataSet['occupation'],
                'peo_avatar'        =>    $avatar,
                'fk_nat_id'         =>    $this->model_crud->get_rowSpecific('tb_nationality', 'nat_acronym', $dataSet['nationality'], 1, 'nat_id'),
                'fk_blo_id'         =>    $dataSet['bloodType'],
                'fk_gen_id'         =>    $dataSet['gender'],
                'fk_mar_id'         =>    $dataSet['maritalStatus'],
                'fk_sch_id'         =>    $dataSet['scholarity'],
                'modifiedAt'        =>    date("Y-m-d H:i:s")
        );

    

        /**
         * Query
         */
        $query = $this->db->update( 'tb_people', $dataSetUpdate, array('peo_id' => $PeopleId ) );
        if($query)
        {

            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        

        echo json_encode($array);

    }




    /*
     * RecordAddress
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function RecordAddress() {

        /**
         * Documentar - Unserialize Data ( Convert string to Arr Object PHP in $searchArray )
         */
        $dataSerialized      =  $_POST['DataSerialized'];
        parse_str($dataSerialized, $dataSet);
        
        $peocomId            =  $_POST['PeocomId'];
        $peocom              =  $_POST['Peocom'];
        $author              =  $_POST['Author'];


        /* Percorrer todo dataSet, onde houver vazio setar como Null */
        foreach ($dataSet as $key => $value) {
            if(empty($value)){
                $value = NULL;
            }
            $dataSet[$key]   =   $value;
        }


        $dataSet       =  array(

                'fk_typ_id'         =>    1,
                'fk_sts_id'         =>    1,
                'fk_aut_id'         =>    $author,
                'fk_sta_id'         =>    $dataSet['state'],
                'fk_cit_id'         =>    $dataSet['city'],
                'add_main'          =>    1,
                'add_zipcode'       =>    $dataSet['zipCode'],
                'add_neighborhood'  =>    $dataSet['neighborhood'],
                'add_street'        =>    $dataSet['street'],
                'add_number'        =>    $dataSet['number'],
                'add_complement'    =>    $dataSet['complement'],

                'createdAt'         =>    date("Y-m-d H:i:s")
        );


        /* Nao trocar a ordem do switch. Depois do $dataSet */
        switch ($peocom) {

            case 'people':
                $dataSet['fk_peo_id']     =   $peocomId;   
                break;

            case 'company':
                $dataSet['fk_com_id']     =   $peocomId;   
                break;

        }


        /**
         * Query
         */

        $query = $this->db->insert( 'tb_address', $dataSet );
        if($query)
        {
            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address(),
                'add_id'    =>  $this->db->insert_id()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        



        echo json_encode($array);

    }



    /*
     * UpdateAddress
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function UpdateAddress() {

        /**
         * Documentar - Unserialize Data ( Convert string to Arr Object PHP in $searchArray )
         */
        $dataSerialized      =  $_POST['DataSerialized'];
        parse_str($dataSerialized, $dataSet);
        
        $peocomId            =  $_POST['PeocomId'];
        $peocom              =  $_POST['Peocom'];
        $author              =  $_POST['Author'];
        $adressId            =  $_POST['AdressId'];


        /* Percorrer todo dataSet, onde houver vazio setar como Null */
        foreach ($dataSet as $key => $value) {
            if(empty($value)){
                $value = NULL;
            }
            $dataSet[$key]   =   $value;
        }


        $dataSet       =  array(

                'fk_typ_id'         =>    1,
                'fk_sts_id'         =>    1,
                'fk_sta_id'         =>    $dataSet['state'],
                'fk_cit_id'         =>    $dataSet['city'],
                'add_main'          =>    1,
                'add_zipcode'       =>    $dataSet['zipCode'],
                'add_neighborhood'  =>    $dataSet['neighborhood'],
                'add_street'        =>    $dataSet['street'],
                'add_number'        =>    $dataSet['number'],
                'add_complement'    =>    $dataSet['complement'],

                'modifiedAt'         =>    date("Y-m-d H:i:s")
        );


        /* Nao trocar a ordem do switch. Depois do $dataSet */
        switch ($peocom) {

            case 'people':
                $dataSet['fk_peo_id']     =   $peocomId;   
                break;

            case 'company':
                $dataSet['fk_com_id']     =   $peocomId;   
                break;

        }


        /**
         * Query
         */
        $query = $this->db->update( 'tb_address', $dataSet, array( 'add_id' => $adressId ) );
        if($query)
        {
            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        


        echo json_encode($array);

    }






























    /*
     * RecordAddress
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function RecordTelephone() {

        /**
         * Documentar - Unserialize Data ( Convert string to Arr Object PHP in $searchArray )
         */
        $dataSerialized      =  $_POST['DataSerialized'];
        parse_str($dataSerialized, $dataSet);
        
        $peocomId            =  $_POST['PeocomId'];
        $peocom              =  $_POST['Peocom'];
        $author              =  $_POST['Author'];


        /* Percorrer todo dataSet, onde houver vazio setar como Null */
        foreach ($dataSet as $key => $value) {
            if(empty($value)){
                $value = NULL;
            }
            $dataSet[$key]   =   $value;
        }


        $dataSet       =  array(

                'fk_typ_id'         =>    $dataSet['typeTelephone'],
                'fk_sta_id'         =>    1,
                'fk_aut_id'         =>    $author,
                'tel_main'          =>    1,
                'tel_telephone'     =>    $dataSet['telephone'],
                'createdAt'         =>    date("Y-m-d H:i:s")
        );


        /* Nao trocar a ordem do switch. Depois do $dataSet */
        switch ($peocom) {

            case 'people':
                $dataSet['fk_peo_id']     =   $peocomId;   
                break;

            case 'company':
                $dataSet['fk_com_id']     =   $peocomId;   
                break;

        }


        /**
         * Query
         */

        $query = $this->db->insert( 'tb_telephone', $dataSet );
        if($query)
        {
            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address(),
                'tel_id'    =>  $this->db->insert_id()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        



        echo json_encode($array);

    }



    /*
     * UpdateTelephone
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function UpdateTelephone() {

        /**
         * Documentar - Unserialize Data ( Convert string to Arr Object PHP in $searchArray )
         */
        $dataSerialized      =  $_POST['DataSerialized'];
        parse_str($dataSerialized, $dataSet);
        
        $peocomId            =  $_POST['PeocomId'];
        $peocom              =  $_POST['Peocom'];
        $author              =  $_POST['Author'];
        $telephoneId         =  $_POST['TelephoneId'];


        /* Percorrer todo dataSet, onde houver vazio setar como Null */
        foreach ($dataSet as $key => $value) {
            if(empty($value)){
                $value = NULL;
            }
            $dataSet[$key]   =   $value;
        }


        $dataSet       =  array(

                'fk_typ_id'         =>    $dataSet['typeTelephone'],
                'fk_sta_id'         =>    1,
                'fk_aut_id'         =>    $author,
                'tel_main'          =>    1,
                'tel_telephone'     =>    $dataSet['telephone'],
                'modifiedAt'        =>    date("Y-m-d H:i:s")
        );


        /* Nao trocar a ordem do switch. Depois do $dataSet */
        switch ($peocom) {

            case 'people':
                $dataSet['fk_peo_id']     =   $peocomId;   
                break;

            case 'company':
                $dataSet['fk_com_id']     =   $peocomId;   
                break;

        }


        /**
         * Query
         */
        $query = $this->db->update( 'tb_telephone', $dataSet, array( 'tel_id' => $telephoneId ) );
        if($query)
        {
            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        


        echo json_encode($array);

    }






    /*
     * RecordEmail
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function RecordEmail() {

        /**
         * Documentar - Unserialize Data ( Convert string to Arr Object PHP in $searchArray )
         */
        $dataSerialized      =  $_POST['DataSerialized'];
        parse_str($dataSerialized, $dataSet);
        
        $peocomId            =  $_POST['PeocomId'];
        $peocom              =  $_POST['Peocom'];
        $author              =  $_POST['Author'];


        /* Percorrer todo dataSet, onde houver vazio setar como Null */
        foreach ($dataSet as $key => $value) {
            if(empty($value)){
                $value = NULL;
            }
            $dataSet[$key]   =   $value;
        }


        $dataSet       =  array(

                'fk_typ_id'         =>    '1',
                'fk_sta_id'         =>    1,
                'fk_aut_id'         =>    $author,
                'ema_main'          =>    1,
                'ema_email'         =>    $dataSet['email'],
                'createdAt'         =>    date("Y-m-d H:i:s")
        );


        /* Nao trocar a ordem do switch. Depois do $dataSet */
        switch ($peocom) {

            case 'people':
                $dataSet['fk_peo_id']     =   $peocomId;   
                break;

            case 'company':
                $dataSet['fk_com_id']     =   $peocomId;   
                break;

        }


        /**
         * Query
         */

        $query = $this->db->insert( 'tb_email', $dataSet );
        if($query)
        {
            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address(),
                'ema_id'    =>  $this->db->insert_id()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        



        echo json_encode($array);

    }



    /*
     * UpdateEmail
     * @author Gustavo Botega 
     * @param     
     * @return boolean  retorna TRUE se o CPF existir
    */
    public function UpdateEmail() {

        /**
         * Documentar - Unserialize Data ( Convert string to Arr Object PHP in $searchArray )
         */
        $dataSerialized      =  $_POST['DataSerialized'];
        parse_str($dataSerialized, $dataSet);
        
        $peocomId            =  $_POST['PeocomId'];
        $peocom              =  $_POST['Peocom'];
        $author              =  $_POST['Author'];
        $emailId             =  $_POST['EmailId'];


        /* Percorrer todo dataSet, onde houver vazio setar como Null */
        foreach ($dataSet as $key => $value) {
            if(empty($value)){
                $value = NULL;
            }
            $dataSet[$key]   =   $value;
        }


        $dataSet       =  array(

                'fk_typ_id'         =>    '1',
                'fk_sta_id'         =>    1,
                'fk_aut_id'         =>    $author,
                'ema_main'          =>    1,
                'ema_email'         =>    $dataSet['telephone'],
                'modifiedAt'        =>    date("Y-m-d H:i:s")
        );


        /* Nao trocar a ordem do switch. Depois do $dataSet */
        switch ($peocom) {

            case 'people':
                $dataSet['fk_peo_id']     =   $peocomId;   
                break;

            case 'company':
                $dataSet['fk_com_id']     =   $peocomId;   
                break;

        }


        /**
         * Query
         */
        $query = $this->db->update( 'tb_email', $dataSet, array( 'ema_id' => $emailId ) );
        if($query)
        {
            $array = array(
                'status'    =>  'processed',
                'datetime'  =>  date("Y-m-d H:i:s"),
                'ip'        =>  $this->input->ip_address(),
                'ema_id'    =>  $this->db->insert_id()
            );
        }
        else
        {
            $array = array(
                'status'    =>  'unprocessed'
            );
        }        


        echo json_encode($array);

    }












    /***********************************************    
      getDropdownFederation
    ***********************************************/
    public function getDropdownFederation() {

       $query = "
            SELECT com_id, com_companyName, com_fantasyName, fk_typ_id FROM tb_company
            WHERE fk_typ_id = 14
            order by com_id=15 desc, com_id asc
        ";        
        $query = $this->db->query($query)->result();

        return $this->mountingDropdownToCodeigniter(
                    $query, // databulk
                    'Escolha sua Federação', // firstOption
                    array( /* columnIndex */
                        'value'=>'com_id',
                        'data-federation'=>'com_fantasyName'
                    ),
                    array( /* columnValue */
                        'com_fantasyName'=>' - ',
                        'com_companyName' => ''
                    ),
                    'federation', /* nameSelect */
                    'class="form-control select2" id="federation" ' /* attributesSelect*/
                );        
    }


    /***********************************************    
      getDropdownTypeEntry
    ***********************************************/
    public function getDropdownTypeEntry() {

        $query = $this->model_crud->select(
                    'tb_typeEntry',
                    array('typ_id', 'typ_title', 'typ_position', 'typ_slug'),
                    array(),
                    array('typ_position'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione um Tipo de Participação', array('value'=>'typ_id', 'data-athletetypeentryslug'=>'typ_slug', 'data-identificationFederation'=>'typ_id'), array('typ_title'=>''), 'athleteTypeEntry', 'class="form-control select2" id="athleteTypeEntry" style="width:100%;" ');        
    }

    /***********************************************    
      dropdownTypeEntryYearly
    ***********************************************/
    public function dropdownTypeEntrySlug($typeEntry) {

        switch ($typeEntry) {
            case 'yearly':
                $idTypeEntry  =   '1';
               break;
            
            case 'cup':
                $idTypeEntry  =   '2';
            break;
            
            case 'single':
                $idTypeEntry  =   '3';
               break;
        }



        /*---------------------------------
            QUERY - TYPE ENTRY PRICE
        -----------------------------------*/
       $query = "
            SELECT m.mod_id, m.mod_title, m.mod_slug, m.mod_status,
                   p.pri_id, p.fk_typ_id, p.fk_mod_id, p.pri_before, p.pri_after, p.pri_status, p.fk_com_id,
                   e.typ_id
            FROM tb_typeEntryPrice              as p
            INNER JOIN tb_modality              as m ON     p.fk_mod_id = m.mod_id    
            INNER JOIN tb_typeEntryCompetitor   as c ON     p.fk_com_id = c.com_id     
            INNER JOIN tb_typeEntry             as e ON     p.fk_typ_id = e.typ_id      
            WHERE 
                p.pri_status = 1 AND
                m.mod_status = 1 AND
                p.fk_typ_id = ".$idTypeEntry." AND
                p.fk_com_id = 1
        ";        
        $query = $this->db->query($query)->result();


        /*---------------------------------
            QUERY DATE LIMIT
        -----------------------------------*/
        $typeEntryDateLimit = $this->model_crud->select(
                    'tb_typeEntryDateLimit',
                    array('*'),
                    array('lim_year =' => date('Y'), 'lim_status =' => '1'),
                    array(),
                    1   
                );
        $dateLimit    =   $typeEntryDateLimit[0]->lim_dateLimit;


        /*---------------------------------
            CHECKING DATE LIMIT 
        -----------------------------------*/
        $dateCurrent     =   date("Y-m-d");
        if( strtotime($dateCurrent) > strtotime($dateLimit) ){
            // passou a data limite
            $dateLimitColumn  =   'pri_after';
        }else{
            // nao passou a data limite
            $dateLimitColumn  =   'pri_before';
        }





        /*---------------------------------
            FOREACH 
        -----------------------------------*/
        foreach ($query as $key => $value) {
            $price  =   '';
           $arrAux[$key]['mod_title'] =   $value->mod_title;
           $arrAux[$key]['pri_id']    =   $value->pri_id;

           /* Environment Variables - Scope Function */
           $idModality          =   $value->mod_id;
           $idTypeEntry         =   $value->typ_id;
           $idTypeCompetitor    =   $value->fk_com_id;


        /*---------------------------------
            QUERY DISCOUNTS
            - Leitura da Query:
            Banco de Dados, traga todos os descontos de acordo com:
            a modalidade, tipo de competidor e tipo de registro. 
            Exiba somente os descontos do mes corrente.
            Exiba somente as modalidades atidadades.
            Exiba somente de competidores tipo atleta
        -----------------------------------*/
       $queryTypeEntryMonthDiscount = "
            SELECT *
            FROM tb_typeEntryMonthDiscount      as d
            INNER JOIN tb_modality              as m    ON d.fk_mod_id = m.mod_id /* modalidade */
            INNER JOIN tb_typeEntryCompetitor   as c    ON d.fk_com_id = c.com_id /* competidor ( pessoa ou atleta ) */
            INNER JOIN tb_typeEntry             as e    ON d.fk_typ_id = e.typ_id /* tipo de registro */
            WHERE 
                d.fk_mod_id = '".$idModality."' AND
                m.mod_status = 1 AND
                d.fk_typ_id = '".$idTypeEntry."' AND /* ser dinamico esse fk_typ_id */
                d.fk_com_id = '".$idTypeCompetitor."' AND
                month(d.dis_month) = month(curdate())   /* selecionando apenas os descontos para o mes atual */
        ";
        $queryMonthDiscount = $this->db->query($queryTypeEntryMonthDiscount)->result();

            $discountReal   =   0;
            if(!empty($queryMonthDiscount)){
            /*
                se passar nesse bloco quer dizer que existe algum desconto
                [ nao permitir o admin cadastrar descontos iguais . criterios ( tipo de registro, modalidade e tipo de pessoa )]
                [ se tiver desconto, tratar caso quando o valor do preco for zero ]
                [ nesse bloco de if pode ter apenas um registro ]
            */

            $discountPercentage     =   $queryMonthDiscount[0]->dis_discountPercentage;
            $discountReal         =    ( $discountPercentage / 100 ) * $value->$dateLimitColumn;
        }



        /*---------------------------------
            DISCOUNTS PER DATE ( AFTER, BEFORE )
        -----------------------------------*/
           $arrAux[$key]['pri_price'] =   $value->$dateLimitColumn - $discountReal;
        }

        return $arrAux;        
    }


    /***********************************************    
      getDropdownTypeTelephone
    ***********************************************/
    public function getDropdownTypeTelephone() {

        $query = $this->model_crud->select(
                    'tb_telephone_type',
                    array('*'),
                    array(),
                    array('typ_position'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, '- Selecione o Tipo -', 'typ_id', 'typ_title');        
    }


    /***********************************************    
      getDropdownScholarity
    ***********************************************/
    public function getDropdownScholarity() {

        $query = $this->model_crud->select(
                    'tb_people_scholarity',
                    array('*'),
                    array(),
                    array('sch_position'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione uma opção', 'sch_id', 'sch_title');        
    }


    /***********************************************    
      getDropdownNaturalness
    ***********************************************/
    public function getDropdownNaturalness() {

        $query = $this->model_crud->select(
                    'tb_city',
                    array('cit_id', 'cit_title'),
                    array(),
                    array('cit_id'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione uma opção', 'cit_id', 'cit_title');        
    }


    /***********************************************    
      getDropdownNationality
    ***********************************************/
    public function getDropdownNationality() {

        $query = $this->model_crud->select(
                    'tb_nationality',
                    array('nat_acronym', 'nat_title', 'nat_position'),
                    array(),
                    array('nat_position'=>'asc'),
                    NULL
                );


        return $this->mountingDropdownToCodeigniter($query, 'Selecione uma Nacionalidade', 'nat_acronym', 'nat_title');        
    }


    /***********************************************    
      getDropdownMaritalStatus
    ***********************************************/
    public function getDropdownMaritalStatus() {

        $query = $this->model_crud->select(
                    'tb_people_maritalstatus',
                    array('mar_id', 'mar_title', 'mar_position'),
                    array(),
                    array('mar_position'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione uma opção', 'mar_id', 'mar_title');        
    }


    /**
     * getDropdownBloodType
     *
     * Funcao e chamada por algum formulario de cadastro: generico, atleta, proprietario de animal, federacao, entidade filiada
     * Monta uma tupla de dados no formato que seja lida pela funcao form_dropdown() do codeigniter
     *
     * @author Gustavo Botega 
     * @param  
     * @return array
     */      
    public function getDropdownBloodType() {
        $query = $this->model_crud->select(
                    'tb_people_bloodtype',
                    array('*'),
                    array(),
                    array('blo_position'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione uma opção', 'blo_id', 'blo_title');        

    }



    /**
     * GetDropdownRace
     *
     * Funcao e chamada por algum formulario de cadastro: generico, atleta, proprietario de animal, federacao, entidade filiada
     * Monta uma tupla de dados no formato que seja lida pela funcao form_dropdown() do codeigniter
     *
     * @author Gustavo Botega 
     * @param  
     * @return array
     */      
    public function GetDropdownRace() {
        $query = $this->model_crud->select(
                    'tb_animal_race',
                    array('*'),
                    array(),
                    array('rac_id'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione uma opção', 'rac_id', 'rac_race');        

    }



    /**
     * GetDropdownFur
     *
     * Funcao e chamada por algum formulario de cadastro: generico, atleta, proprietario de animal, federacao, entidade filiada
     * Monta uma tupla de dados no formato que seja lida pela funcao form_dropdown() do codeigniter
     *
     * @author Gustavo Botega 
     * @param  
     * @return array
     */      
    public function GetDropdownFur() {
        $query = $this->model_crud->select(
                    'tb_animal_fur',
                    array('*'),
                    array(),
                    array('fur_id'=>'asc'),
                    NULL
                );

        return $this->mountingDropdownToCodeigniter($query, 'Selecione uma opção', 'fur_id', 'fur_fur');        

    }



    /**
     * getDropdownState
     *
     * Funcao e chamada por algum formulario de cadastro: generico, atleta, proprietario de animal, federacao, entidade filiada
     * Monta uma tupla de dados no formato que seja lida pela funcao form_dropdown() do codeigniter
     *
     * @author Gustavo Botega 
     * @param  
     * @return array
     */      
    public function getDropdownState() {

        $query = $this->model_crud->select(
                    'tb_state',
                    array('*'),
                    array(),
                    array('sta_title'=>'asc'),
                    NULL
                );
        return $this->mountingDropdownToCodeigniter($query, 'Selecione uma opção', array('value'=>'sta_id', 'data-stateAcronym'=>'sta_acronym'), array('sta_title'=>' - ', 'sta_acronym' => '' ), 'state', 'class="form-control select2" id="state" ');        
    }


    /**
     * getDropdownCity
     *
     * Funcao e chamada por algum formulario de cadastro: generico, atleta, proprietario de animal, federacao, entidade filiada
     * Monta uma tupla de dados no formato que seja lida pela funcao form_dropdown() do codeigniter
     *
     * @author Gustavo Botega 
     * @param  
     * @return array
     */      
    public function getDropdownCity() {

        header( 'Cache-Control: no-cache' );
        header( 'Content-type: application/xml; charset="utf-8"', true );

        $cod_estados        = $_REQUEST['cod_estados'];
        $query              = $this->model_crud->select(
                                'tb_city',
                                array('*'),
                                array('fk_sta_id =' => $cod_estados),
                                array('cit_title'=>'asc'),
                                NULL
                              );

        $arrAux             = array();
        foreach ($query as $key => $value) {
            $arrAux[] = array(
                'cit_id'        =>   $value->cit_id,
                'cit_title'     =>   $value->cit_title,
            );
        }

        echo( json_encode( $arrAux ) );

    }



    /**
     * getResultZipCode
     *
     *
     * @author Gustavo Botega 
     * @param  
     * @return json
     */      
    public function getResultZipCode() {
        $zipCode = $_POST['zipCode'];
        $webServiceResult       = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $zipCode);         
        $data['status']         = (int) $webServiceResult->resultado;
        $data['message']        = (string) $webServiceResult->resultado;
        $data['street']         = (string) $webServiceResult->tipo_logradouro . ' ' . $webServiceResult->logradouro;
        $data['neighborhood']   = (string) $webServiceResult->bairro;
        $data['city']           = (string) $webServiceResult->cidade;
        $data['state']          = (string) $webServiceResult->uf;

        /* Getting id City */
        $idState = '';
        $idCity = '';

        $query              = $this->model_crud->select(
                                'tb_state',
                                array('*'),
                                array('sta_acronym =' => $data['state']),
                                array(),
                                1
                              );
        if(!empty($query))
            $idState            = $query[0]->sta_id;


        $query              = $this->model_crud->select(
                                'tb_city',
                                array('*'),
                                array('cit_title =' => $data['city'], 'fk_sta_id =' => $idState),
                                array('cit_title'=>'asc'),
                                1
                              );
        if(!empty($query))
            $idCity             = $query[0]->cit_id;
        

        $data['idState']    =   $idState;
        $data['idCity']     =   $idCity;

        echo json_encode($data);   
    }





    /**
     * getDropdownEntityAndSchool
     *
     * @author Gustavo Botega 
     * @param  
     * @return array
     */      
    public function getDropdownEntityAndSchool() {

        header( 'Cache-Control: no-cache' );
        header( 'Content-type: application/xml; charset="utf-8"', true );


        $entitySchoolType   =   $_POST['entitySchoolType'];
        $federationId       =   $_POST['federationId'];
        $federationSlug     =   $_POST['federationSlug'];


        switch ($entitySchoolType) {
            case 'entity':
                $entitySchoolType = 15;
                break;
            case 'school':
                $entitySchoolType = 16;
                break;
        }

       $query = "
            select *
                from (
                    SELECT 1 as fk_aut_id, fk_com_id, fk_fed_id, fk_sta_id, ent_acronym, fk_typ_id, ent_id, ent_title  from tb_company_entityschool where fk_fed_id = ".$federationId." and fk_typ_id = ".$entitySchoolType." and fk_sta_id = 1
                    union all
                    SELECT 2 as fk_aut_id, fk_com_id, fk_fed_id, fk_sta_id, ent_acronym, fk_typ_id, ent_id, ent_title from tb_company_entityschool where fk_fed_id = ".$federationId." and fk_typ_id = 17 and fk_sta_id = 1
                ) a
            order by ent_title asc
        ";        
        $query = $this->db->query($query)->result();


        $arrAux             = array();
        foreach ($query as $key => $value) {
            $arrAux[] = array(
                'ent_id'        =>   $value->ent_id,
                'ent_title'     =>   $value->ent_title,
                'fk_com_id'     =>   $value->fk_com_id,
                'ent_acronym'   =>   $value->ent_acronym,
            );
        }

        echo( json_encode( $arrAux ) );

    }

    public function getDropdownAffiliatedEntity() {

        header( 'Cache-Control: no-cache' );
        header( 'Content-type: application/xml; charset="utf-8"', true );

        $federationId       =   $_GET['federationId'];
        $federationSlug     =   $_GET['federationSlug'];

        $query = $this->model_crud->select(
                    'tb_companyEntitySchool',
                    array('*'),
                    array('fk_fed_id =' => $federationId, 'fk_typ_id =' => '1'),
                    array('aff_title'=>'asc'),
                    NULL
                );

        $arrAux     =   array('' => 'Selecione uma Opção');
        foreach ($query as $key => $value) {
            $arrAux[] = array(
                'aff_id'        =>   $value->aff_id,
                'aff_title'     =>   $value->aff_title,
                'aff_acronym'   =>   $value->aff_acronym,
            );
        }  

        echo( json_encode( $arrAux ) );

    }




    /***********************************************    
      domNationality
    ***********************************************/
    public function domNationality() {

        $data = '
            <option value="BR">Brasil</option>
            <option value="AF">Afghanistan</option>
            <option value="AL">Albania</option>
            <option value="DZ">Algeria</option>
            <option value="AS">American Samoa</option>
            <option value="AD">Andorra</option>
            <option value="AO">Angola</option>
            <option value="AI">Anguilla</option>
            <option value="AQ">Antarctica</option>
            <option value="AR">Argentina</option>
            <option value="AM">Armenia</option>
            <option value="AW">Aruba</option>
            <option value="AU">Australia</option>
            <option value="AT">Austria</option>
            <option value="AZ">Azerbaijan</option>
            <option value="BS">Bahamas</option>
            <option value="BH">Bahrain</option>
            <option value="BD">Bangladesh</option>
            <option value="BB">Barbados</option>
            <option value="BY">Belarus</option>
            <option value="BE">Belgium</option>
            <option value="BZ">Belize</option>
            <option value="BJ">Benin</option>
            <option value="BM">Bermuda</option>
            <option value="BT">Bhutan</option>
            <option value="BO">Bolivia</option>
            <option value="BA">Bosnia and Herzegowina</option>
            <option value="BW">Botswana</option>
            <option value="BV">Bouvet Island</option>
            <option value="IO">British Indian Ocean Territory</option>
            <option value="BN">Brunei Darussalam</option>
            <option value="BG">Bulgaria</option>
            <option value="BF">Burkina Faso</option>
            <option value="BI">Burundi</option>
            <option value="KH">Cambodia</option>
            <option value="CM">Cameroon</option>
            <option value="CA">Canada</option>
            <option value="CV">Cape Verde</option>
            <option value="KY">Cayman Islands</option>
            <option value="CF">Central African Republic</option>
            <option value="TD">Chad</option>
            <option value="CL">Chile</option>
            <option value="CN">China</option>
            <option value="CX">Christmas Island</option>
            <option value="CC">Cocos (Keeling) Islands</option>
            <option value="CO">Colombia</option>
            <option value="KM">Comoros</option>
            <option value="CG">Congo</option>
            <option value="CD">Congo, the Democratic Republic of the</option>
            <option value="CK">Cook Islands</option>
            <option value="CR">Costa Rica</option>
            <option value="CI">Cote d Ivoire</option>
            <option value="HR">Croatia (Hrvatska)</option>
            <option value="CU">Cuba</option>
            <option value="CY">Cyprus</option>
            <option value="CZ">Czech Republic</option>
            <option value="DK">Denmark</option>
            <option value="DJ">Djibouti</option>
            <option value="DM">Dominica</option>
            <option value="DO">Dominican Republic</option>
            <option value="EC">Ecuador</option>
            <option value="EG">Egypt</option>
            <option value="SV">El Salvador</option>
            <option value="GQ">Equatorial Guinea</option>
            <option value="ER">Eritrea</option>
            <option value="EE">Estonia</option>
            <option value="ET">Ethiopia</option>
            <option value="FK">Falkland Islands (Malvinas)</option>
            <option value="FO">Faroe Islands</option>
            <option value="FJ">Fiji</option>
            <option value="FI">Finland</option>
            <option value="FR">France</option>
            <option value="GF">French Guiana</option>
            <option value="PF">French Polynesia</option>
            <option value="TF">French Southern Territories</option>
            <option value="GA">Gabon</option>
            <option value="GM">Gambia</option>
            <option value="GE">Georgia</option>
            <option value="DE">Germany</option>
            <option value="GH">Ghana</option>
            <option value="GI">Gibraltar</option>
            <option value="GR">Greece</option>
            <option value="GL">Greenland</option>
            <option value="GD">Grenada</option>
            <option value="GP">Guadeloupe</option>
            <option value="GU">Guam</option>
            <option value="GT">Guatemala</option>
            <option value="GN">Guinea</option>
            <option value="GW">Guinea-Bissau</option>
            <option value="GY">Guyana</option>
            <option value="HT">Haiti</option>
            <option value="HM">Heard and Mc Donald Islands</option>
            <option value="VA">Holy See (Vatican City State)</option>
            <option value="HN">Honduras</option>
            <option value="HK">Hong Kong</option>
            <option value="HU">Hungary</option>
            <option value="IS">Iceland</option>
            <option value="IN">India</option>
            <option value="ID">Indonesia</option>
            <option value="IR">Iran (Islamic Republic of)</option>
            <option value="IQ">Iraq</option>
            <option value="IE">Ireland</option>
            <option value="IL">Israel</option>
            <option value="IT">Italy</option>
            <option value="JM">Jamaica</option>
            <option value="JP">Japan</option>
            <option value="JO">Jordan</option>
            <option value="KZ">Kazakhstan</option>
            <option value="KE">Kenya</option>
            <option value="KI">Kiribati</option>
            <option value="KP">Korea, Democratic People s Republic of</option>
            <option value="KR">Korea, Republic of</option>
            <option value="KW">Kuwait</option>
            <option value="KG">Kyrgyzstan</option>
            <option value="LA">Lao People s Democratic Republic</option>
            <option value="LV">Latvia</option>
            <option value="LB">Lebanon</option>
            <option value="LS">Lesotho</option>
            <option value="LR">Liberia</option>
            <option value="LY">Libyan Arab Jamahiriya</option>
            <option value="LI">Liechtenstein</option>
            <option value="LT">Lithuania</option>
            <option value="LU">Luxembourg</option>
            <option value="MO">Macau</option>
            <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
            <option value="MG">Madagascar</option>
            <option value="MW">Malawi</option>
            <option value="MY">Malaysia</option>
            <option value="MV">Maldives</option>
            <option value="ML">Mali</option>
            <option value="MT">Malta</option>
            <option value="MH">Marshall Islands</option>
            <option value="MQ">Martinique</option>
            <option value="MR">Mauritania</option>
            <option value="MU">Mauritius</option>
            <option value="YT">Mayotte</option>
            <option value="MX">Mexico</option>
            <option value="FM">Micronesia, Federated States of</option>
            <option value="MD">Moldova, Republic of</option>
            <option value="MC">Monaco</option>
            <option value="MN">Mongolia</option>
            <option value="MS">Montserrat</option>
            <option value="MA">Morocco</option>
            <option value="MZ">Mozambique</option>
            <option value="MM">Myanmar</option>
            <option value="NA">Namibia</option>
            <option value="NR">Nauru</option>
            <option value="NP">Nepal</option>
            <option value="NL">Netherlands</option>
            <option value="AN">Netherlands Antilles</option>
            <option value="NC">New Caledonia</option>
            <option value="NZ">New Zealand</option>
            <option value="NI">Nicaragua</option>
            <option value="NE">Niger</option>
            <option value="NG">Nigeria</option>
            <option value="NU">Niue</option>
            <option value="NF">Norfolk Island</option>
            <option value="MP">Northern Mariana Islands</option>
            <option value="NO">Norway</option>
            <option value="OM">Oman</option>
            <option value="PK">Pakistan</option>
            <option value="PW">Palau</option>
            <option value="PA">Panama</option>
            <option value="PG">Papua New Guinea</option>
            <option value="PY">Paraguay</option>
            <option value="PE">Peru</option>
            <option value="PH">Philippines</option>
            <option value="PN">Pitcairn</option>
            <option value="PL">Poland</option>
            <option value="PT">Portugal</option>
            <option value="PR">Puerto Rico</option>
            <option value="QA">Qatar</option>
            <option value="RE">Reunion</option>
            <option value="RO">Romania</option>
            <option value="RU">Russian Federation</option>
            <option value="RW">Rwanda</option>
            <option value="KN">Saint Kitts and Nevis</option>
            <option value="LC">Saint LUCIA</option>
            <option value="VC">Saint Vincent and the Grenadines</option>
            <option value="WS">Samoa</option>
            <option value="SM">San Marino</option>
            <option value="ST">Sao Tome and Principe</option>
            <option value="SA">Saudi Arabia</option>
            <option value="SN">Senegal</option>
            <option value="SC">Seychelles</option>
            <option value="SL">Sierra Leone</option>
            <option value="SG">Singapore</option>
            <option value="SK">Slovakia (Slovak Republic)</option>
            <option value="SI">Slovenia</option>
            <option value="SB">Solomon Islands</option>
            <option value="SO">Somalia</option>
            <option value="ZA">South Africa</option>
            <option value="GS">South Georgia and the South Sandwich Islands</option>
            <option value="ES">Spain</option>
            <option value="LK">Sri Lanka</option>
            <option value="SH">St. Helena</option>
            <option value="PM">St. Pierre and Miquelon</option>
            <option value="SD">Sudan</option>
            <option value="SR">Suriname</option>
            <option value="SJ">Svalbard and Jan Mayen Islands</option>
            <option value="SZ">Swaziland</option>
            <option value="SE">Sweden</option>
            <option value="CH">Switzerland</option>
            <option value="SY">Syrian Arab Republic</option>
            <option value="TW">Taiwan, Province of China</option>
            <option value="TJ">Tajikistan</option>
            <option value="TZ">Tanzania, United Republic of</option>
            <option value="TH">Thailand</option>
            <option value="TG">Togo</option>
            <option value="TK">Tokelau</option>
            <option value="TO">Tonga</option>
            <option value="TT">Trinidad and Tobago</option>
            <option value="TN">Tunisia</option>
            <option value="TR">Turkey</option>
            <option value="TM">Turkmenistan</option>
            <option value="TC">Turks and Caicos Islands</option>
            <option value="TV">Tuvalu</option>
            <option value="UG">Uganda</option>
            <option value="UA">Ukraine</option>
            <option value="AE">United Arab Emirates</option>
            <option value="GB">United Kingdom</option>
            <option value="US">United States</option>
            <option value="UM">United States Minor Outlying Islands</option>
            <option value="UY">Uruguay</option>
            <option value="UZ">Uzbekistan</option>
            <option value="VU">Vanuatu</option>
            <option value="VE">Venezuela</option>
            <option value="VN">Viet Nam</option>
            <option value="VG">Virgin Islands (British)</option>
            <option value="VI">Virgin Islands (U.S.)</option>
            <option value="WF">Wallis and Futuna Islands</option>
            <option value="EH">Western Sahara</option>
            <option value="YE">Yemen</option>
            <option value="ZM">Zambia</option>
            <option value="ZW">Zimbabwe</option>';



            $doc = new DOMDocument();
            @$doc->loadHTML($data);

            $tags = $doc->getElementsByTagName('option');

            $contador = 1;
            foreach ($tags as $tag) {
                   $nome = $tag->nodeValue;
                   $value = $tag->getAttribute('value');
                   $data = array('nat_title'=>$nome, 'nat_acronym'=>$value, 'nat_position'=>$contador);
                  // $this->db->insert('tb_nationality', $data);
                   $contador++;
            }
    }






    /**
     * Settings
     */
    public function settings()
    {
        $this->data['variable']                     =   FALSE;
        return $this->data;
    }


}
