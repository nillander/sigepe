<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class QuartoDeSela extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();



        $this->load->module('evento');
        $this->data     =   $this->evento->InformacoesTemplate();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Listagem();    	
    }



    /**
    * Listagem
    *
    * @author Gustavo Botega 
    */
    public function Listagem($EventoId){




        $this->data['EventoId']         =   $EventoId;



        /* Carrega View */
        $this->LoadTemplateEvento('template/user/sistema/evento/quarto-de-sela/Listagem', $this->data);

    }













    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 



        /* STYLES
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
                $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
        
                /* Date & Time Pickers */
                $this->data['StylesFile']['Plugins']['daterangepicker']                             = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datepicker3']                       = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-timepicker']                        = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-datetimepicker']                    = TRUE;
                $this->data['StylesFile']['Plugins']['clockface']                                   = TRUE;

                /* Bootstrap File Input */
                $this->data['StylesFile']['Plugins']['bootstrap-fileinput']                         = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['StylesFile']['Plugins']['bootstrap-wysihtml5']                         = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-markdown']                          = TRUE;
                $this->data['StylesFile']['Plugins']['bootstrap-summernote']                        = TRUE;


            // STYLES  //

                /* {NamePackage} */
                $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
                $this->data['StylesFile']['Styles']['profile']                                      = TRUE;







        /* SCRIPTS
        -----------------------------------*/

            // PLUGINS  //

                /* {NamePackage} */
                $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
                $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;
                
                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Plugins']['moment']                                   = TRUE;
                $this->data['ScriptsFile']['Plugins']['daterangepicker']                          = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-timepicker']                     = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-datetimepicker']                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['clockface']                                = TRUE;

                /* Bootstrap File Input */
                $this->data['ScriptsFile']['Plugins']['bootstrap-fileinput']                      = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['wysihtml5']                                = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-wysihtml5']                      = TRUE;
                $this->data['ScriptsFile']['Plugins']['markdown']                                 = TRUE;
                $this->data['ScriptsFile']['Plugins']['bootstrap-markdown']                       = TRUE;
                $this->data['ScriptsFile']['Plugins']['summernote']                               = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
                $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;



            // SCRIPTS // 

                /* Bootstrap Sweet Alert */
                $this->data['ScriptsFile']['Scripts']['ui-sweetalert']                  = TRUE;
                
                /* Date & Time Pickers */
                $this->data['ScriptsFile']['Scripts']['components-date-time-pickers']   = TRUE;

                /* Markdown & WYSIWYG Editors */
                $this->data['ScriptsFile']['Scripts']['components-editors']             = TRUE;

                /* User Profile */
/*                $this->data['ScriptsFile']['Scripts']['profile']                        = TRUE;
                $this->data['ScriptsFile']['Scripts']['timeline']                       = TRUE;*/

    }



    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
        $this->data['PackageStyles'][]    =   'packages/frontoffice/style/cadastro-animal';


        /* Carregando Scripts */
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/animal';
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/cadastro-animal';
        $this->data['PackageScripts'][]   =   'packages/frontoffice/script/form-validation-cadastro-evento';


    }





}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

