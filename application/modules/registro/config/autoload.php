<?php
    $autoload = array(
        'helper'    => array('url', 'form'),
        'libraries' => array( 'MY_Data', 'MY_Pessoa', 'MY_Pessoa_Telefone', 'MY_Pessoa_Email', 'MY_Pessoa_Fisica', 'MY_Pessoa_Fisica_Atleta', 'MY_Moeda'),
        'model' => array( 'model_crud'),
        'core'	=> array('my_controller')
    );
	
?>