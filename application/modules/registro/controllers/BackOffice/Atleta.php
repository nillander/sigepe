<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_BackOffice.php';
class Atleta extends MY_BackOffice {

    public $data;

    function __construct() {

        parent::__construct();

        $this->ThemeComponent(); 
        $this->SigepeAsset(); 

    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
		$this->Tabela();    	
    }


    /**
    * Formulario
    *
    * @author Gustavo Botega 
    */
    public function Tabela(){

        /* Setando variaveis de ambiente */
        $this->data['ClassEnvironment']   = get_class($this);
        $this->data['go']      = 'Registro';
        $this->data['PageHeadSubtitle']   = 'Relação de registros de atletas';
        $this->data['Breadcrumbs']        = array();


        $this->data['PageHeadTitle']      = 'Atletas Federados';
        $this->data['PageHeadSubtitle']   = 'Relação geral de registros de atletas';




        /* Taxas */
        $Sql                        = '
                                            SELECT * FROM tb_evento_modalidade as evm
                                            ORDER BY FIELD(evm_id, "5", "1") DESC, evm_id
                                                ';
        $this->data['DatasetModalidade']   = $this->db->query($Sql)->result();




        /* Atletas Ativos */
        $Sql                        = '
                                            SELECT pes.pes_id, 
                                                   pes.pes_nome_razao_social, 
                                                   clube.pes_nome_razao_social as nomeclube,
                                                   
                                                   reg.reg_id,
                                                   reg.fk_tip_id,
                                                   reg.fk_sta_id,
                                                   reg.fk_fin_id,
                                                   reg.criado as reg_criado,

                                                   fin.fin_id,
                                                   fin.fk_fip_id,
                                                   fin.fk_sta_id as status_financeiro,
                                                   fin.fin_valor_liquido,
                                                   fin.fin_transacao,

                                                   ret.ret_id,
                                                   ret.ret_tipo,

                                                   fip.fip_id,
                                                   fip.fip_pagamento,

                                                   pef.pef_id,
                                                   pef.fk_pes_id,

                                                   pfa.pfa_id,
                                                   pfa.fk_pef_id,
                                                   pfa.fk_pje_id,

                                                   evm.evm_id,
                                                   evm.evm_modalidade,

                                                   sta.sta_id,
                                                   sta.sta_status

                                             FROM tb_registro as reg

                                            LEFT JOIN tb_registro_tipo as ret
                                            ON reg.fk_tip_id = ret.ret_id

                                            LEFT JOIN tb_pessoa as pes
                                            ON reg.fk_pes_id = pes.pes_id

                                            LEFT JOIN tb_pessoa_fisica as pef
                                            ON pef.fk_pes_id = pes.pes_id

                                            LEFT JOIN tb_pessoa_fisica_atleta as pfa
                                            ON pfa.fk_pef_id = pef.pef_id


                                            LEFT JOIN tb_pessoa as clube
                                            ON clube.pes_id = pfa.fk_pje_id


                                            LEFT JOIN tb_evento_modalidade as evm
                                            ON reg.fk_mod_id = evm.evm_id

                                            LEFT JOIN tb_financeiro as fin
                                            ON fin.fin_id = reg.fk_fin_id

                                            LEFT JOIN tb_status as sta
                                            ON sta.sta_id = reg.fk_sta_id

                                            LEFT JOIN tb_financeiro_pagamento as fip
                                            ON fip.fip_id = fin.fk_fip_id


                                                ';
        $this->data['DatasetAtleta']   = $this->db->query($Sql)->result();




        /* Taxas */
        /*
        $Sql                        = '
                                            SELECT pes.pes_id, 
                                                   pes.pes_nome_razao_social, 
                                                   
                                                   reg.reg_id,
                                                   reg.fk_tip_id,
                                                   reg.fk_sta_id,
                                                   reg.fk_fin_id,

                                                   fin.fin_id,
                                                   fin.fk_fip_id,
                                                   fin.fk_sta_id as status_financeiro,
                                                   fin.fin_valor_liquido,

                                                   ret.ret_id,
                                                   ret.ret_tipo,

                                                   fip.fip_id,
                                                   fip.fip_pagamento,

                                                   pef.pef_id,
                                                   pef.fk_pes_id,

                                                   pfa.pfa_id,
                                                   pfa.fk_pef_id,

                                                   evm.evm_id,
                                                   evm.evm_modalidade,

                                                   sta.sta_id,
                                                   sta.sta_status

                                             FROM tb_registro as reg

                                            INNER JOIN tb_registro_tipo as ret
                                            ON reg.fk_tip_id = ret.ret_id

                                            INNER JOIN tb_pessoa as pes
                                            ON reg.fk_pes_id = pes.pes_id

                                            INNER JOIN tb_pessoa_fisica as pef
                                            ON pef.fk_pes_id = pes.pes_id

                                            INNER JOIN tb_pessoa_fisica_atleta as pfa
                                            ON pfa.fk_pef_id = pef.pef_id

                                            INNER JOIN tb_evento_modalidade as evm
                                            ON reg.fk_mod_id = evm.evm_id

                                            INNER JOIN tb_financeiro as fin
                                            ON fin.fin_id = reg.fk_fin_id

                                            INNER JOIN tb_status as sta
                                            ON sta.sta_id = reg.fk_sta_id

                                            INNER JOIN tb_financeiro_pagamento as fip
                                            ON fip.fip_id = fin.fk_fip_id

                                                ';
        $this->data['DatasetAtleta']   = $this->db->query($Sql)->result();
        */
//        var_dump($this->data['DatasetAtleta']);



        /* Carrega View */
        $this->LoadTemplate('Template/BackOffice/sistema/Registro/Atleta', $this->data);

    }




    /**
    * PackagesClass
    *
    * @author Gustavo Botega 
    */
    public function ThemeComponent(){ 


        /* 
            StylesFile 
        */
            /*  Plugins*/
            $this->data['StylesFile']['Plugins']['select2']                                     = TRUE;
            $this->data['StylesFile']['Plugins']['bootstrap-sweetalert']                        = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete']                           = TRUE;
            $this->data['StylesFile']['Plugins']['easy-autocomplete-themes']                    = TRUE;
            $this->data['StylesFile']['Plugins']['datatables']                                   = TRUE;
            $this->data['StylesFile']['Plugins']['datatables-bootstrap']                         = TRUE;
        
            /*  Syles*/
            $this->data['StylesFile']['Styles']['todo-2']                                       = TRUE;
    //            $this->data['StylesFile']['Styles']['profile']                                = TRUE;



        /* 
            ScriptsFile 
        */
            /*  Plugins*/
            $this->data['ScriptsFile']['Plugins']['select2']                                  = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['jquery-validation-additional-methods']     = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-wizard']                         = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask']                               = TRUE;
            $this->data['ScriptsFile']['Plugins']['input-mask-trigger']                       = TRUE;
            $this->data['ScriptsFile']['Plugins']['easy-autocomplete']                        = TRUE;
            $this->data['ScriptsFile']['Plugins']['bootstrap-sweetalert']                     = TRUE;



            $this->data['ScriptsFile']['Plugins']['datatable']                     = TRUE;
            $this->data['ScriptsFile']['Plugins']['datatables']                     = TRUE;
            $this->data['ScriptsFile']['Plugins']['datatables-bootstrap']                     = TRUE;

            /*  Scripts*/
            $this->data['ScriptsFile']['Scripts']['table-datatables-buttons']                            = TRUE;
            $this->data['ScriptsFile']['Scripts']['table-datatables-fixedheader']                            = TRUE;


    }




    /**
    * SigepeAsset
    *
    * Carregar aqui dinamicamente JS e CSS
    *
    * @author Gustavo Botega 
    * @return array
    */
    public function SigepeAsset()
    { 
        /* Carregando Estilos */
#        $this->data['PackageStyles'][]    =   'Packages/Styles/Animal/FrontOffice/Cadastro';


        /* Carregando Scripts */
 #       $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Animal';
 #       $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/Cadastro';
 #       $this->data['PackageScripts'][]   =   'Packages/Scripts/Animal/FrontOffice/FormWizard';


    }



     /*
             // @@@ Implementar permissões por usuario
        
        // AUDITORIA:
            [0]=> Slug Module
            [1]=> Table Database
            [2]=> Column Database
            [3]=> Value Deleted
            [4]=> Folder Thumb
            [5]=> Delete Gallery
            [6]=> Table Gallery
            [7]=> Column Table Gallery
            [8]=> Data/hora
            [9]=> IP
            [10]=> ID Usuário  
            [11]=> Type of Operation  
            */

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

