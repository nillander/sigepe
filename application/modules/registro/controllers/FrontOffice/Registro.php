<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once APPPATH . '/core/MY_FrontOffice.php';
class Registro extends MY_FrontOffice {

    public $data;

    function __construct() {

        parent::__construct();


    }


    /**
    * Index
    *
    * @author Gustavo Botega 
    */
    public function index() {
    	return true;
    }


    /*
       X [C] TB_FINANCEIRO
       X [C] TB_FINANCEIRO_HISTORICO
       X [C] TB_FINANCEIRO_ITEM
       X [C] TB_FINANCEIRO_DESCONTO

       X [C] TB_REGISTRO
       X [C] TB_REGISTRO_HISTORICO
        [C] TB_FINANCEIRO_REGISTRO
        [C] TB_FINANCEIRO_REL_REGISTRO

       ~ [U] TB_FINANCEIRO ( atualizar numero pagar-me transacao )
       ~ [U] TB_FINANCEIRO / TB_FINANCEIRO_HISTORICO ( se for cartão atualizar status da transacao )
    */



    /**
    * Processar
    *
    *
    *
    *
    *
    *
    *
    * @author Gustavo Botega 
    */
    public function Processar(){

        // Inicializando dados. Prevent errors.
        $NaturezaOperacao                                =  NULL;

        // Carregando modulos externos
        $this->load->module('financeiro/FrontOffice/Transacao'); // Carregando a classe transacao do modulo financeiro do ambiente FrontOffice
        $this->load->module('financeiro/FrontOffice/FinanceiroRegistro'); // Carregando a classe registro do modulo financeiro do ambiente FrontOffice
        $this->load->module('registro/FrontOffice/Taxa'); // Obtendo taxas do registro selecionado na view

        // Resgatando valores da view
        parse_str($_POST['FormSerialized'], $DadosUnserialized);            
        $ModalidadeId                           =   $DadosUnserialized['modalidade'];       // Referencia: tb_evento_modalidade
        $TipoRegistroId                         =   $DadosUnserialized['tipo-registro'];    // Referencia: tb_registro_tipo
        $TipoPagamentoId                        =   $DadosUnserialized['tipo-pagamento'];   // Referencia: tb_financeiro_pagamento
        $TipoCompetidorId                       =   $DadosUnserialized['tipo-competidor'];  // Referencia: tb_registro_competidor 


        // Obtendo valores de taxas do registro a partir das informacoes enviadas na view
        $DatasetValores                         =    json_decode( $this->taxa->GetValores($ModalidadeId, $TipoRegistroId, $TipoCompetidorId) );


        // Gravando a Transacao
        $DatasetComplementar                    =   array(
                                                        'tipo-pagamento'            =>  $TipoPagamentoId,
                                                        'responsavel-financeiro'    =>  $this->session->userdata('PessoaId')
                                                    );
        $Transacao                              =    $this->GravarTransacao($DatasetValores, $DatasetComplementar );


        // Gravando o Registro
        $Registro                               =    $this->Gravar($DatasetValores, $Transacao, $DadosUnserialized);


        // Gravando Registro no Financeiro
        $this->financeiroregistro->Gravar( $Transacao['FinanceiroId'],  $Registro['Id'], $DatasetValores );


        // Gravando Relacionamento Financeiro - Registro
       $this->financeiroregistro->GravarRelacionamento( $Transacao['FinanceiroId'],  $Registro['Id'] );


       // Financeiro - Se for boleto faz a Transaction no pagarme
//       $this->


        // Tratando situacoes de erro.
        echo json_encode(
                        array(
                            'Transacao'         =>  $Transacao['Transacao'],
                            'FinanceiroId'      =>  $Transacao['FinanceiroId'],
                            'RegistroId'        =>  $Registro['Id']
                        )
                    );

    }




    /**
    * Gravar
    *
    * @author Gustavo Botega 
    */
    public function Gravar($DatasetValores = NULL, $Transacao = NULL, $DadosUnserialized){

        // Iniciando variavel. Prevent error.
        $DatasetResultado   =   array();


        // Tipo de competidor. Se 1 status x se 2 status y
        switch ($DatasetValores->Info->TipoCompetidorId) {
            case '1':
                $StatusId   =   300; // [ Registro Atleta ] Inativo
                break;
            
            case '2':
                $StatusId   =   310; // [ Registro Animal ] Inativo
                break;
        }


        // Tratando casos onde nao existe o valor
        ($DatasetValores->TaxaExtra->Flag) ? $TaxaExtraId = $DatasetValores->TaxaExtra->Id : $TaxaExtraId = NULL;
        ($DatasetValores->Desconto->Flag) ? $DescontoId = $DatasetValores->Desconto->Id : $DescontoId = NULL;


        if(is_null($DatasetValores))
            parse_str($_POST['FormSerializedGravarRegistro'], $DadosUnserialized);        

        // Dados
        $Arr       =   array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_pes_id'                         =>    (!isset($DadosUnserialized['animal-id'])) ? $this->session->userdata('PessoaId') : NULL, // ID Pessoa ( Atleta ). Referencia: tb_pessoa
            'fk_ani_id'                         =>    (!isset($DadosUnserialized['animal-id'])) ? NULL : $DadosUnserialized['animal-id'], // Responsavel Financeiro
            'fk_fin_id'                         =>    $Transacao['FinanceiroId'], // ID da transacao financeira. Referencia: tb_financeiro
            'fk_mod_id'                         =>    $DatasetValores->Info->ModalidadeId, // Id da Modalidade
            'fk_tip_id'                         =>    $DatasetValores->Info->TipoRegistroId, // Id do Tipo do Registro ( 1 Anual / 2 Copa / 3 Unica )
            'fk_rec_id'                         =>    $DatasetValores->Info->TipoCompetidorId, // Id Tipo de Competidor ( 1 Atleta / 2 Animal )
            'fk_sta_id'                         =>    $StatusId, // Id Status do Registro
            'fk_ret_id'                         =>    $DatasetValores->Info->Id, // Id Registro Taxa. Referencia: tb_registro_taxa
            'fk_rtd_id'                         =>    $DescontoId, // Id Registro Taxa Desconto. Referencia: tb_registro_taxa_desconto
            'fk_rte_id'                         =>    $TaxaExtraId, // Id Registro Taxa Extra. Referencia: tb_registro_taxa_extra
            'criado'                            =>    date("Y-m-d H:i:s")
        );
        $Query = $this->db->insert('tb_registro', $Arr);


        if($Query){
            $RegistroId         =   $this->db->insert_id();
            $this->GravarHistorico($RegistroId, $DatasetValores, $StatusId);
            $DatasetResultado['Status']         =   true;
            $DatasetResultado['Id']             =   $RegistroId;
        }else{
            $DatasetResultado['Status']         = false;
        }        


        if(isset($_POST['FormSerializedGravarRegistro']))
            echo json_encode(array());

        return $DatasetResultado;

    }



    /**
    * GravarHistorico
    *
    * @author Gustavo Botega 
    */
    public function GravarHistorico($RegistroId, $Dataset = FALSE, $StatusId) {

        // Dados
        $Arr       =  array(
            'fk_aut_id'                         =>    $this->session->userdata('PessoaId'),
            'fk_sta_id'                         =>    $StatusId, // Status 
            'fk_reg_id'                         =>    $RegistroId, // ID do Registro
#            'reh_historico'                     =>    (isset($Dataset['historico'])) ? $Dataset['historico'] : NULL, // 
            'criado'                            =>    date("Y-m-d H:i:s")
        );
        $Query = $this->db->insert('tb_registro_historico', $Arr);

        /* Verificando operacao */
        ($Query) ?  $DatasetResultado['Status']     =   true : $DatasetResultado['Status']  =   false;


        return $DatasetResultado;

    }



    /**
    * GravarTransacao
    *
    * @author Gustavo Botega 
    */
    public function GravarTransacao($DatasetValores, $DatasetComplementar) {

        // Inicializando variaveis. Prevent errors.
        $Dataset    =   array();


        /*
            Tratando a natureza da Operacao. Se o tipo de competidor for 1 (atleta) a natureza da operação é 1 - Registro de Atleta.
            Se o tipo de competidor for 2 (animal) a natureza da operação é 2.
            Tabela referência: tb_registro_competidor 
        */
        ($DatasetValores->Info->TipoCompetidorId == '1') ? $NaturezaOperacao = '1' : ''; // Natureza Operecao 1 = Registro de Atleta
        ($DatasetValores->Info->TipoCompetidorId == '2') ? $NaturezaOperacao = '2' : ''; // Natureza Operecao 2 = Registro de Atleta


        // Transacao
        $Dataset['Transacao']              =   array(
                                                        'status-sigepe'                 =>      '100', // [ SIGEPE ] Transação - Aguardando Pagamento
                                                        'status-pagarme'                =>      '200', // [ Pagarme ] Aguardando Pagamento
                                                        'responsavel-financeiro'        =>      $DatasetComplementar['responsavel-financeiro'], // implementar futuramente pelo responsalve-financeiro id. criar vinculos etc
                                                        'tipo-pagamento'                =>      $DatasetComplementar['tipo-pagamento'],  
                                                        'natureza-operacao'             =>      $NaturezaOperacao,
                                                        'valor-bruto'                   =>      $DatasetValores->Total->Bruto,
                                                        'valor-liquido'                 =>      $DatasetValores->Total->Liquido,
                                                    );

        // Item da Transacao
        $Dataset['Item']                   =   $this->GetItem($DatasetValores);


        // Desconto na Transacao
        $Dataset['Desconto']               =   $this->GetDesconto($DatasetValores);

        // Historico da Transacao
        $Dataset['Historico']              =   array(
                                                        'tipo'                 =>      '1', // [ SIGEPE ] Transação - Aguardando Pagamento
                                                        'historico'            =>      'Criação da fatura. Motivo: Novo registro. Metodo GravarTransacao. Class: registro/FrontOffice/Registro.'
                                                    );


        // Criando a transacao e tratamento de erros
        $Transacao                                  =   $this->transacao->Gravar( $Dataset );
        if(!$Transacao['Status'])
            return false;


        // Nesse momento a transacao foi realizada com sucesso. Recuperando dados da transacao.
        return array(
                        'FinanceiroId'      =>  $Transacao['FinanceiroId'],
                        'Transacao'         =>  $Transacao['Transacao']
                    );
    }


    /**
    * GetItem
    *
    * @author Gustavo Botega 
    */
    public function GetItem($DatasetValores) {

        // Inicializando variaveis. Prevent errors.
        $Item    =   array();

        // Inserindo a Taxa Principal como Item no array.
        $Item[0]    =   array(
                            'item'              =>      $DatasetValores->Taxa->Titulo,
                            'descricao'         =>      $DatasetValores->Taxa->Descricao,
                            'quantidade'        =>      1,
                            'valor-unitario'    =>      $DatasetValores->Taxa->Valor
                        );   


        // Inserindo a Taxa Extra (se houver) como Item no array.
        if($DatasetValores->TaxaExtra->Flag):

        $Item[1]    =   array(
                            'item'              =>      $DatasetValores->TaxaExtra->Titulo,
                            'descricao'         =>      $DatasetValores->TaxaExtra->Descricao,
                            'quantidade'        =>      1,
                            'valor-unitario'    =>      $DatasetValores->TaxaExtra->Valor
                        );   

        endif;


        return $Item;

    }



    /**
    * GetDesconto
    *
    * @author Gustavo Botega 
    */
    public function GetDesconto($DatasetValores) {

        // Inicializando variaveis. Prevent errors.
        $Desconto    =   array();

        // Inserindo um desconto como Item no array.
        if($DatasetValores->Desconto->Flag):

            $Desconto[0]    =   array(
                                'tipo'              =>      $DatasetValores->Desconto->Tipo,
                                'item'              =>      $DatasetValores->Desconto->Titulo,
                                'descricao'         =>      $DatasetValores->Desconto->Descricao,
                                'valor'             =>      $DatasetValores->Desconto->Valor
                            );
        endif;   

        return $Desconto;

    }





    /**
    * SetStatusAtivo
    *
    * @author Gustavo Botega 
    */
    public function SetStatusAtivo($RegistroId = NULL) {


        $TipoCompetidorId     =   $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $RegistroId, 1, 'fk_rec_id');


        if($TipoCompetidorId == '1')
            $StatusId     =   '301';

        if($TipoCompetidorId == '2')
            $StatusId     =   '311';


        $Dataset        =   array( 'fk_sta_id'  =>  $StatusId );

        $this->db->where( 'reg_id', $RegistroId);
        $query = $this->db->update('tb_registro', $Dataset);


        // Gravando historico
        $this->GravarHistorico($RegistroId, '', $StatusId);

    }



    /**
    * SetStatusCancelado
    *
    * @author Gustavo Botega 
    */
    public function SetStatusCancelado($RegistroId = NULL) {


        $DatasetRegistro        =   array( 'fk_sta_id'  =>  303 ); // Registro - Cancelado

        $this->db->where( 'reg_id', $RegistroId);
        $query = $this->db->update('tb_registro', $DatasetRegistro);


        // Gravando historico
        $this->GravarHistorico($RegistroId, '', $StatusId);


        if(!$Query)
            return false;


        // Carregando modulo transacao
        $this->load->module('financeiro/Transacao');


        // Gravando o historico da transacao no modulo transcao. 
        $DatasetFinanceiro             =   array( 
                                            'Historico'     =>  array(
                                                                    'tipo'      =>  4, // Tipo 4. Cancelado pelo usuario. 
                                                                    'historico' =>  'Janela de pagamento cartão de crédito (pagar.me) fechada pelo usuário. Contexto: Registro de competidor.'
                                                                )
                                            );

        // Obtendo o ID financeiro
        $FinanceiroId       =      $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $RegistroId, 1, 'fk_fin_id');


        // Cancelando a transacao
        $this->transacao->SetTransacaoCancelada( $FinanceiroId, NULL, $DatasetFinanceiro );


        return true;

    }




    /**
    * SetStatusCanceladoJson
    *
    * @author Gustavo Botega 
    */
    public function SetStatusCanceladoJson() {

        $RegistroId     =    $_POST['SigepeRegistroId'];

        $DatasetRegistro        =   array( 'fk_sta_id'  =>  303 ); // Registro - Cancelado
        $this->db->where( 'reg_id', $RegistroId);
        $Query = $this->db->update('tb_registro', $DatasetRegistro);


        if(!$Query)
            return false;


        // Gravando historico
        $this->GravarHistorico($RegistroId, '', 303);


        // Carregando modulo transacao
        $this->load->module('financeiro/FrontOffice/Transacao');


        // Gravando o historico da transacao no modulo transcao. 
        $DatasetFinanceiro             =   array( 
                                            'Historico'     =>  array(
                                                                    'tipo'      =>  4, // Tipo 4. Cancelado pelo usuario. 
                                                                    'historico' =>  'Janela de pagamento cartão de crédito (pagar.me) fechada pelo usuário. Contexto: Registro de competidor.'
                                                                )
                                            );

        // Obtendo o ID financeiro
        $FinanceiroId       =      $this->model_crud->get_rowSpecific('tb_registro', 'reg_id', $RegistroId, 1, 'fk_fin_id');


        // Cancelando a transacao
        $this->transacao->SetTransacaoCancelada( $FinanceiroId, NULL, $DatasetFinanceiro );


        return true;

    }









}
/* End of file */

