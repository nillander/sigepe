<?php


/**
 * Verifica se um usuario esta logado. Retorna TRUE para um usuario logado e FALSE para usuario que nao esta autenticado no sistema  
 *
 * Exempplo de utilizacao:
 *
 * if( is_auth() )
 * {
 * 	 // usuario autenticado no sistema
 * }
 * 
 * @return array
 */



 function IsAuthenticated(){
	
	// # instancia do codeIgniter 
	$sigepe =& get_instance();
	$ModuleAuthentication				=	$sigepe->load->module('autenticacao');
	$ModuleAuthenticationSettingsToken	=	$sigepe->autenticacao->TokenAutenticacao();
        

	// # resgatando os valores que sao gravados na sessao do usuario quando autenticado. 
	$PessoaId 					=	$sigepe->session->userdata('PessoaId');
	$Autenticado 				=	$sigepe->session->userdata('Autenticado');
	$TokenAutenticacao 			=	$sigepe->session->userdata('TokenAutenticacao');

	// # caso algum dos itens nao sejam validos redireciona para a tela de login 
	if( is_null( $PessoaId ) )
	{
		redirect(base_url() . 'logout', 'refresh');
	}
	else if( is_null( $Autenticado ) || !$Autenticado )
	{
		redirect(base_url() . 'logout', 'refresh');
	}
	else if( is_null( $TokenAutenticacao ) || $TokenAutenticacao != $ModuleAuthenticationSettingsToken )
	{
		redirect(base_url() . 'logout', 'refresh');
	} 
	else
	{
		// # usuario logado no sistema, nao precisa de nenhuma acao. 
		// # no  momento do login ele ja e redirecionado para a pagina inicial.
		
	}
		
}

	
/*
	Areas onde somente pessoas nao logadas podem acessar. Exemplo> Formulario de Registro de Atleta estando logado. : http://localhost/sigepe/register/RegisterAthlete
*/
 function IsGuest(){
	
	$sigepe =& get_instance();
	$Authenticated 				=	$sigepe->session->userdata('UserAuthenticated');
	if( $Authenticated )
		redirect(base_url() . 'logout', 'refresh');

		
}


 function IsOwnerAuthenticated(){

 	/* Developing... */

}




function GetSaudacao(){

    $hr = date(" H ");
    if($hr >= 12 && $hr<18) {
    $resp = "boa tarde!";}
    else if ($hr >= 0 && $hr <12 ){
    $resp = "bom dia!";}
    else {
    $resp = "boa noite!";}
    return "$resp";
    
}


 function CheckPermission(){

	$sigepe =& get_instance();
	$DataBulk 	=	array();

	$ArrayQtyArgs	 		=	func_get_args();
	if($ArrayQtyArgs > 0){

	    $ArrayPermission 	= func_get_args();
	    $UserPermission 	=	$sigepe->session->userdata('UserTypePeocom');

		$ArrayIntersect 	= count(array_intersect($UserPermission, $ArrayPermission));
		if(!$ArrayIntersect){

			$DataBulk['fk_aut_id']		=	$sigepe->session->userdata('UserId');
			$DataBulk['Message']		=	'Violação de permissão de acesso!';

			$DataSet 	=	array(
				'fk_aut_id'		=>	$DataBulk['fk_aut_id'],	
				'log_url'		=>	$_SERVER['REQUEST_URI'],
				'log_content'	=>	$DataBulk['Message'],
				'createdAt'		=>  date("Y-m-d H:i:s")
			);
		    $sigepe->db->insert( 'tb_log' , $DataSet);

	        $sigepe->session->sess_destroy(); 

			// Redirecionar usuario para tela de login com aviso
			redirect(base_url() . 'authentication/authentication/AccessViolation', 'refresh');
			return false;
		}
	}





}

function FuncGetArgsArray(  ){

    $arg_list = func_get_args();
    for ($i = 0; $i < $numargs; $i++) {
        echo "Argument $i is: " . $arg_list[$i] . "\n";
    }


}





 function IsMajority( $DateEntry = NULL ){
	
	if( is_null($DateEntry) )
		return false;

	$DateToday 	=	date("Y-m-d");

	$DifferenceYears 	=	DateDifferences( $DateToday, $DateEntry, 'y' );
	if( $DifferenceYears >= 18 ){
		return true;
	}else{
		return false;
	}

  }


	function DateDifferences( $DateEnd, $DateBegin, $TypeInterval )
	{

		$NewDateEnd = new DateTime( $DateEnd );
		$NewDateBegin = new DateTime( $DateBegin );

		$DateInterval = $NewDateEnd->diff( $NewDateBegin );

		switch ($TypeInterval) {
			case 'y':
				return $DateInterval->y;
				break;

			case 'm':
				return $DateInterval->m;
				break;

			case 'd':
				return $DateInterval->d;
				break;
			
			default:
				return $DateInterval->y;
				break;
		}

	}








function is_auth()
{
	// # instancia do codeIgniter 
	$ci =& get_instance();
	
	// # resgatando os valores que sao gravados na sessao do usuario quando autenticado. 
	$userId 		= $ci->session->userdata('userId');
	$authenticated 	= $ci->session->userdata('authenticated');
	$token 			= $ci->session->userdata('token');

	// # caso algum dos itens nao sejam validos redireciona para a tela de login 
	if( is_null( $userId ) )
	{
		redirect(base_url() . 'authentication/authentication/', 'refresh');
	}
	else if( is_null( $authenticated ) || !$authenticated )
	{
		redirect(base_url() . 'authentication/authentication/', 'refresh');
	}
	else if( is_null( $token ) || $token != 't7nr' )
	{
		redirect(base_url() . 'authentication/authentication/', 'refresh');
	} 
	else
	{
		// # usuario logado no sistema, nao precisa de nenhuma acao. 
		// # no  momento do login ele ja e redirecionado para a pagina inicial.
		
	}
		
}


/**
 * Gera um codigo de seguranca com 4 letras. 
 * 
 * Exempplo de utilizacao:
 * 
 * $captcha = captcha(); 
 * $captcha['cap_word']; // cadeia de caracteres para guardar na sessao do usuario. Sera utilizado no backend
 * $captcha['cap_image']; // string que contem a tag img completa para ser colocado no HTML
 * 
 * @return array
 */
function captcha()
{
	/* CAPTCHA */
	$ci =& get_instance();
	$ci->load->helper('captcha');

	$vals = array(
			'img_path'	 => './captcha/',
			'img_url'	 => base_url().'captcha/',
			'img_width'	 => 78,
			'img_height' => 33,
			'expiration' => 3600
	);

	$cap = create_captcha( $vals );

	return array('cap_image' => $cap['image'] , 'cap_word' => $cap['word'] );
}

function pre( $arg )
{
	if( is_null( $arg ) )
	{
		echo '<pre>';
			var_dump( $arg ); 
		echo '</pre>';
	}
	else
	{
		echo '<pre>';
			print_r( $arg ); 
		echo '</pre>';
	}
}


if ( ! function_exists('cftDropdown'))
{
	/**
	 * Drop-down Menu
	 *
	 * @param	mixed	$data
	 * @param	mixed	$options
	 * @param	mixed	$selected
	 * @param	mixed	$extra
	 * @return	string
	 */
	function cftDropdown($dataBulk, $selected = array())
	{


		$defaults = array();

		/* sizeArrayOption */
		if(count($dataBulk['index']) != count($dataBulk['content'])){
			return 'erro - size array - verify entry data';
		}else{
			$sizeArrayOption 	= count($dataBulk['index']);
		}

		/* dataBulk empty or not array */
		if( !is_array($dataBulk) || empty($dataBulk) ){
			return 'erro';
		}

		$firstOption	= 	$dataBulk['firstOption'];
		$data 			= 	$dataBulk['nameSelect'];
		$extra 			= 	$dataBulk['attributesSelect'];
		$options 		=	array();

		if (is_array($data))
		{
			if (isset($data['selected']))
			{
				$selected = $data['selected'];
				unset($data['selected']); // select tags don't have a selected attribute
			}

			if (isset($data['options']))
			{
				$options = $data['options'];
				unset($data['options']); // select tags don't use an options attribute
			}
		}
		else
		{
			$defaults = array('name' => $data);
		}

		is_array($selected) OR $selected = array($selected);
		is_array($options) OR $options = array($options);

		// If no selected state was submitted we will attempt to set it automatically
		if (empty($selected))
		{
			if (is_array($data))
			{
				if (isset($data['name'], $_POST[$data['name']]))
				{
					$selected = array($_POST[$data['name']]);
				}
			}
			elseif (isset($_POST[$data]))
			{
				$selected = array($_POST[$data]);
			}
		}

		$extra = _attributes_to_string($extra);

		$multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

		$form = '<select '.rtrim(_parse_form_attributes($data, $defaults)).$extra.$multiple.">\n";
		$form .= '<option value="">'. $firstOption .'</option>' ;

		$arrIndex   = array();
		foreach ($dataBulk['index'] as $key => $value) {
			foreach ($value as $keyItem => $keyValue) {

				if ( ! isset($arrIndex[$key])) {
				   $arrIndex[$key] = null;
				}

				$arrIndex[$key] .=  $keyItem . '=' . "'" . $keyValue. "'" . " ";
			}
		}

		$arrContent   = array();
		foreach ($dataBulk['content'] as $key => $value) {
			foreach ($value as $keyItem => $keyValue) {

				if ( ! isset($arrContent[$key])) {
				   $arrContent[$key] = null;
				}

				$arrContent[$key] .=  $keyValue . $keyItem;
			}
		}

		for ($i=0; $i < $sizeArrayOption; $i++) { 
			$form .= '<option '.$arrIndex[$i].'> 
						'.(string) $arrContent[$i].
					 "</option>\n";
		}

		return $form."</select>\n";
	}
}

function arrayToSelect( $arr = array() , $nomeChave = '', $nomeValor = '')
{
	$arraySelect = array(); 
	$arraySelect[''] = '--';
	if( is_array( $arr ) && count( $arr ) > 0 )
	{
		foreach( $arr as $array )
		{
			$arraySelect[ $array[$nomeChave] ] = $array[$nomeValor];   
		}
	}
	
	return $arraySelect; 
}

/**
 * Cria uma array com a mensagem e o tipo com o retorno do tipo json para um alert no js do modulo 
 */
function ajaxAlert( $message, $type = SUCCESS )
{
	$dados = array(); 
	$dados['verifica'] = $type;
	$dados['message'] = $message;
	echo json_encode( $dados );
	exit();
}


/**
 * Recebe a mensagem $str e mostra em um alert javascript
 * @param string $str
 */
function alert( $str = '', $tipo )
{
	if( !empty( $str ) )
	{
		$alert = 
		'
			<div class="alert alert-'.$tipo.'" hide">
				<button class="close" data-dismiss="alert"></button>
				<span>'.$str.'</span>
			</div>
		';
		
		return $alert; 
	}
	
	return false; 
}




/**
 * FirstWord
 * 
 * @return 
 */
function FirstWord( $string = null, $capitalize = false )
{

	if(is_null($string))
		return null;

	$explode 	=	explode(" ", $string);
	if($capitalize){
		return ucfirst($explode[0]);
	}else{
		return ucfirst($explode[0]);
	}
			
}



/**
 * LastWord
 * 
 * @return 
 */
function LastWord( $string = null, $capitalize = false )
{

	if(is_null($string))
		return null;

	$last_word_start = strrpos ( $string , " ") + 1;
	$last_word_end = strlen($string) - 1;
	$last_word = substr($string, $last_word_start, $last_word_end);

	return $last_word;
			
}



/**
 * MaskCpf
 * 
 * @return 
 */
function MaskCpf( $cpf = null, $mask = false )
{

	if(is_null($cpf))
		return null;

	if( $mask ){

		/* Esta sendo solicitado para inserir a mascara porem o cpf ja contem elementos da mascara como ponto ou hifen, retorna nulo */
		if(preg_match('/-/', $cpf)){
			return null;
		}

		$cpf = substr_replace($cpf, ".", 3, 0);
		$cpf = substr_replace($cpf, ".", 7, 0);
		$cpf = substr_replace($cpf, "-", 11, 0);
		return $cpf;
	}else{
		$cpf 	=	explode(".", $cpf);
		$cpf 	=	implode('', $cpf);
		$cpf 	=	explode("-", $cpf);
		$cpf 	=	implode('', $cpf);
		return $cpf;
	}

	return 'error';
			
}







/**
 * MaskCnpj
 * 
 * @return 
 */
function MaskCnpj( $cnpj = null, $mask = false )
{

	if(is_null($cnpj))
		return null;

	if( $mask ){

		/* Esta sendo solicitado para inserir a mascara porem o cnpj ja contem elementos da mascara como ponto ou hifen, retorna nulo */
		if(preg_match('/-/', $cpf)){
			return null;
		}


		$cnpj = substr_replace($cnpj, ".", 2, 0);
		$cnpj = substr_replace($cpf, ".", 6, 0);
		$cnpj = substr_replace($cpf, "/", 10, 0);
		$cnpj = substr_replace($cpf, "-", 14, 0);
		return $cnpj;
	}else{
		$cnpj 	=	explode(".", $cnpj);
		$cnpj 	=	implode('', $cnpj);
		$cnpj 	=	explode("-", $cnpj);
		$cnpj 	=	implode('', $cnpj);
		$cnpj 	=	explode("/", $cnpj);
		$cnpj 	=	implode('', $cnpj);
		return $cnpj;
	}

	return 'error';
			
}