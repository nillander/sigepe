<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe base do sistema. 
 * 
 * @author Gustavo Botega
 *
 */
class MY_Login extends CI_Controller
{
	
	/**
	 * Array de dados utilizado nas views 
	 * @var Array
	 */
	function __construct()
	{
		parent::__construct();
	}


	/**
	 * Mostra a estrutura do template 
	 * @param string $pathView
	 */
	protected function loadAuthentication( $pathView = 'template/authentication/', $settings = NULL )
	{
		$data = array();

		$settings['base_url']	=	base_url();
		$data['settings']		= 	$settings;

		/*Settings variables */
		$this->parser->parse('template/authentication/header', $data['settings']);
		$this->parser->parse( $pathView, $data['settings']);
		$this->parser->parse('template/authentication/footer', $data['settings']);
	}


}