<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe base do Sistema
 *
 */

include_once APPPATH . '/core/MY_CommonOffice.php';

class MY_BackOffice extends MY_CommonOffice
{

	/**
	 * Array de dados utilizado nas views
	 * @var Array
	 */

    public $data;
    public $settings;

	function __construct()
	{
		parent::__construct();

        $this->data = array();


        // Camada de Autenticacao
        IsAuthenticated();

	}


    /**
     * Mostra a estrutura do template do Usuario
     *
     * @author Gustavo Botega
     * @return void
     */
    public function NillTemplate( $pathView = NULL, $settings = NULL, $external = array() )
    {

        $data = array();
        $settings['base_url']               =   base_url();
        $settings['ShowHeaderNavigation']   =   TRUE;


        if(!isset($settings['ShowSidebar']))
            $settings['ShowSidebar']            =   FALSE;



        $settings['ShowTopMenu']            =   TRUE;
        $settings['ShowPreFooter']          =   FALSE;
        $settings['ShowQuickNav']           =   FALSE;
        $packages               =   $this->packages();
        $data['settings']       =   $settings;
        $data['external']       =   $external;


        /*Settings variables */
        $merge  =   array_merge ( $packages, $settings, $external );


        $this->Template('html-begin', $merge);
            $this->Head($merge);
            $this->Template('body-begin', $merge);




                    $this->Header($merge);


                    $this->Template('begin-page-container', $merge);
                        $this->NillSidebar($merge);
                        $this->Environment($merge, $pathView);
                    $this->Template('end-page-container', $merge);

                    $this->ContentFooter($merge);



                $this->Footer($merge);

            $this->Template('body-end', $merge);

        $this->Template('html-end', $merge);

    }


    /**
     * Sidebar
     *
     * @author Gustavo Botega
     * @return void
     */
    public function NillSidebar($merge){
        $sql = "SELECT * from vanguarda_tools.vw_navitem where nav_fk_nav_id is null order by nav_ordem";
        $merge['sbHeading'] = $this->db->query($sql)->result();
        
        /* header */
        $this->parser->parse('Template/BackOffice/Content/Sidebar/NillSidebar', $merge); // bloco de cabecalho


    }


    /**
     * Mostra a estrutura do template do Usuario
     *
     * @author Gustavo Botega
     * @return void
     */
    public function LoadTemplate( $pathView = NULL, $settings = NULL, $external = array() )
    {

        $data = array();
        $settings['base_url']               =   base_url();
        $settings['ShowHeaderNavigation']   =   TRUE;


        if(!isset($settings['ShowSidebar']))
            $settings['ShowSidebar']            =   FALSE;



        $settings['ShowTopMenu']            =   TRUE;
        $settings['ShowPreFooter']          =   FALSE;
        $settings['ShowQuickNav']           =   FALSE;
        $packages               =   $this->packages();
        $data['settings']       =   $settings;
        $data['external']       =   $external;


        /*Settings variables */
        $merge  =   array_merge ( $packages, $settings, $external );


        $this->Template('html-begin', $merge);
            $this->Head($merge);
            $this->Template('body-begin', $merge);




                    $this->Header($merge);


                    $this->Template('begin-page-container', $merge);
                        $this->Sidebar($merge);
                        $this->Environment($merge, $pathView);
                    $this->Template('end-page-container', $merge);

                    $this->ContentFooter($merge);



                $this->Footer($merge);

            $this->Template('body-end', $merge);

        $this->Template('html-end', $merge);

    }













    /**
     * Environment
     *
     * @author Gustavo Botega
     * @return void
     */
    public function Environment($merge, $pathView){

        $this->Template('begin-page-content-wrapper', $merge);
            $this->Template('begin-page-content', $merge);

            /* PAGE-HEAD */
            $this->parser->parse('Template/BackOffice/Content/Environment/PageHead/PageHead', $merge);


            /* Breadcrumbs */  $this->parser->parse('Template/BackOffice/Content/Environment/page-content-body/breadcrumbs/breadcrumbs', $merge); // estilos externos

            /* Content Inner */
            if( !isset($merge['ShowProfileEvento']) && !isset($merge['ShowProfilePessoa'])):


                if(is_array($pathView))
                {
                    foreach ($pathView as $view) {
                        $this->parser->parse( $view, $merge );
                    }
                }
                else
                {
                    $this->parser->parse( $pathView, $merge );
                }
            endif;


            /*********************************************/
            if( isset($merge['ShowProfileEvento']) ):

                $this->load->module('evento/BackOffice/Evento');
                $arr = $this->evento->InformacoesTemplate();
                foreach ($arr as $key => $value) {
                    $merge[$key]    =   $value;
                }
                $merge['StylesFile']['Styles']['profile']   =   true;

                $this->parser->parse('Template/FrontOffice/sistema/Evento/Estrutura/Header', $merge);
                $this->parser->parse('Template/FrontOffice/sistema/Evento/Estrutura/Sidebar', $merge);
                if(is_array($pathView))
                {
                    foreach ($pathView as $view) {
                        $this->parser->parse( $view, $merge );
                    }
                }
                else
                {
                    $this->parser->parse( $pathView, $merge );
                }
                $this->parser->parse('Template/FrontOffice/sistema/Evento/Estrutura/Footer', $merge);

            endif;
            /*********************************************/
            if( isset($merge['ShowProfilePessoa']) ):


                $merge['StylesFile']['Styles']['profile']   =   true;

                $this->parser->parse('Template/BackOffice/sistema/Pessoa/Estrutura/Sidebar', $merge);
                if(is_array($pathView))
                {
                    foreach ($pathView as $view) {
                        $this->parser->parse( $view, $merge );
                    }
                }
                else
                {
                    $this->parser->parse( $pathView, $merge );
                }
                $this->parser->parse('Template/BackOffice/sistema/Pessoa/Estrutura/Footer', $merge);

            endif;
            /*********************************************/


            $this->Template('end-page-content', $merge);
        $this->Template('end-page-content-wrapper', $merge);

    }








    /**
     * Head
     *
     * @author Gustavo Botega
     * @return void
     */
    public function Head($merge){

        /* INICIO HEAD*/
        $this->parser->parse('Template/BackOffice/head/begin', $merge);


            /* METATAGS*/
            $this->parser->parse('Template/BackOffice/head/metatags', $merge);  // metatags da head

            /* MANDATORY / PLUGINS */
            $this->parser->parse('Template/BackOffice/head/theme/global-mandatory-styles', $merge); // estilos requeridos do tema
            $this->parser->parse('Template/BackOffice/head/page-level/plugins', $merge); // estilos requeridos do tema

            /* GLOBAL / PLUGINS */
            $this->parser->parse('Template/BackOffice/head/theme/theme-global-styles', $merge); // estilos externos
            $this->parser->parse('Template/BackOffice/head/page-level/styles', $merge); // estilos requeridos do tema

            /* THEME LAYOUT STYLES */
            $this->parser->parse('Template/BackOffice/head/theme/theme-layout-styles', $merge); // estilos requeridos do tema

            /* SIGEPE */
            $this->parser->parse('Template/BackOffice/head/sigepe/sigepe-global', $merge); // estilos requeridos do tema
            $this->parser->parse('Template/BackOffice/head/sigepe/sigepe-modules', $merge); // estilos requeridos do tema


        /* FIM HEAD*/
        $this->parser->parse('Template/BackOffice/head/end', $merge);

    }




    /**
     * Header
     *
     * @author Gustavo Botega
     * @return void
     */
    public function Header($merge){


        /* header */
        $this->parser->parse('Template/BackOffice/Content/Header/Header', $merge); // bloco de cabecalho


    }



    /**
     * Sidebar
     *
     * @author Gustavo Botega
     * @return void
     */
    public function Sidebar($merge){


        /* header */
        $this->parser->parse('Template/BackOffice/Content/Sidebar/Sidebar', $merge); // bloco de cabecalho


    }









    /**
     * ContentFooter
     *
     * @author Gustavo Botega
     * @return void
     */
    public function ContentFooter($merge){

        $this->parser->parse('Template/BackOffice/Content/Footer/begin', $merge);

            if($merge['ShowPreFooter'] == TRUE)
            $this->parser->parse('Template/BackOffice/Content/Footer/pre-footer', $merge);

            $this->parser->parse('Template/BackOffice/Content/Footer/inner-footer', $merge);

        $this->parser->parse('Template/BackOffice/Content/Footer/end', $merge);

    }



    /**
     * Footer
     *
     * @author Gustavo Botega
     * @return void
     */
    public function Footer($merge){

        /* quicknav */
        if($merge['ShowQuickNav'] == TRUE)
        $this->parser->parse('Template/BackOffice/Footer/quicknav', $merge);

        /* core-plugins */
        $this->parser->parse('Template/BackOffice/Footer/sigepe/environment-variable', $merge);

        /* core-plugins */
        $this->parser->parse('Template/BackOffice/Footer/theme/core-plugins', $merge);
        $this->parser->parse('Template/BackOffice/Footer/page-level/plugins', $merge);

        /* theme-global-scripts */
        $this->parser->parse('Template/BackOffice/Footer/theme/theme-global-scripts', $merge); /* app.min.js */
        $this->parser->parse('Template/BackOffice/Footer/page-level/scripts', $merge);

        /* theme-layouts-scripts */
        $this->parser->parse('Template/BackOffice/Footer/theme/theme-layouts-scripts', $merge);
        $this->parser->parse('Template/BackOffice/Footer/page-level/others', $merge);

        /* sigepe */
        $this->parser->parse('Template/BackOffice/Footer/sigepe/sigepe-global', $merge);
        $this->parser->parse('Template/BackOffice/Footer/sigepe/sigepe-modules', $merge);

    }



    public function Template($type, $merge){

        switch ($type) {
            case 'html-begin':
                $this->parser->parse('Template/BackOffice/html-begin', $merge); // abertura tag html
                break;

            case 'body-begin':
                $this->parser->parse('Template/BackOffice/body-begin', $merge); // abertura tag body
                break;

            case 'content-begin':
                $this->parser->parse('Template/BackOffice/Content/Begin', $merge);  // abertura tag page-wrapper
                break;

            case 'content-end':
                $this->parser->parse('Template/BackOffice/Content/End', $merge); // fechamento tag page-wrapper
                break;


            case 'begin-page-container':
                $this->parser->parse('Template/BackOffice/Content/BeginPageContainer', $merge);  // abertura tag page-wrapper
                break;

            case 'end-page-container':
                $this->parser->parse('Template/BackOffice/Content/EndPageContainer', $merge); // fechamento tag page-wrapper
                break;


            case 'begin-page-content-wrapper':
                $this->parser->parse('Template/BackOffice/Content/BeginPageContentWrapper', $merge);  // abertura tag page-wrapper
                break;

            case 'end-page-content-wrapper':
                $this->parser->parse('Template/BackOffice/Content/EndPageContentWrapper', $merge);  // abertura tag page-wrapper
                break;


            case 'begin-page-content':
                $this->parser->parse('Template/BackOffice/Content/BeginPageContent', $merge); // fechamento tag page-wrapper
                break;

            case 'end-page-content':
                $this->parser->parse('Template/BackOffice/Content/EndPageContent', $merge); // fechamento tag page-wrapper
                break;


                /*
                $this->Template('begin-page-content-wrapper', $merge);
                    $this->Template('begin-page-content', $merge);
                */



            case 'body-end':
                $this->parser->parse('Template/BackOffice/body-end', $merge);  // abertura tag page-wrapper
                break;

            case 'html-end':
                $this->parser->parse('Template/BackOffice/end-html', $merge); // fechamento tag html
                break;

        }


    }















}
