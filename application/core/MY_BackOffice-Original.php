<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe base do Sistema
 *
 */
include_once APPPATH . '/core/MY_CommonOffice.php';

class MY_BackOffice extends MY_CommonOffice
{

	/**
	 * Array de dados utilizado nas views
	 * @var Array
	 */

    public $data;
    public $settings;

	function __construct()
	{
		parent::__construct();

        $this->data = array();

        // Camada de Autenticacao
        IsAuthenticated();

	}




	public function index(){
    }




    /**
     * Mostra a estrutura do template
     *
     * @author Gustavo Botega
     * @return void
     */
    public function LoadTemplateProfilePeople( $pathView = 'template/content', $settings = NULL, $external = array() )
    {
		$merge          =	array_merge ( $settings, $external );
		$this->LoadParser( $pathView, $merge, 'ProfilePeople', 'BackOffice' );
    }



    /**
     * Mostra a estrutura do template
     *
     *  Modulos que usam esse metodo: dashboard
     *
     * @author Gustavo Botega
     * @return void
     */
    public function LoadTemplate( $pathView = 'template/content', $settings = NULL, $external = array() )
    {

		$merge          =	array_merge ( $settings, $external );
		$this->LoadParser( $pathView, $merge, NULL, 'BackOffice' );

	}












    /**
     * Mostra a estrutura do template do Usuario
     *
     * @author Gustavo Botega
     * @return void
     */
    protected function loadTemplateAdmin( $pathView = 'template/content', $settings = NULL, $external = array() )
    {

        $data = array();

        $settings['base_url']           =   base_url();
        /*
        $settings['peopleFirstName']    =   $this->session->userdata('peopleFirstName');
        $settings['peopleId']           =   $this->session->userdata('peopleId');
        $settings['userId']             =   $this->session->userdata('userId');
        $settings['peopleThumb']        =   $this->session->userdata('peopleThumb');
        */


        $packages               =   $this->packages();
        $data['settings']       =   $settings;
        $data['external']       =   $external;


        $merge  =   array_merge ( $packages, $settings, $external );

        /*Settings variables */
        $this->parser->parse('template/admin/header', $merge);
        //$this->parser->parse('template/admin/footer-componentsStyles', $merge);
        //$this->parser->parse('template/admin/sidebar', $merge);
        //$this->parser->parse('template/admin/content-begin', $merge);

        if(is_array($pathView))
        {
            foreach ($pathView as $view) {
                $this->parser->parse( $view, $merge );
            }
        }
        else
        {
            $this->parser->parse( $pathView, $merge );
        }

    //  $this->parser->parse('template/admin/content-end', $merge);
        $this->parser->parse('template/admin/footer', $merge);
    //  $this->parser->parse('template/admin/footer-modals', $merge);
    //  $this->parser->parse('template/admin/footer-environment', $merge);
    //  $this->parser->parse('template/admin/footer-scripts', $merge);
    //  $this->parser->parse('template/admin/footer-componentsScripts', $merge);
    //  $this->parser->parse('template/admin/footer-triggers', $merge);
    }




}
