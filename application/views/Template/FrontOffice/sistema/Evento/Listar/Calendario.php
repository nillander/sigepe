<div class="col-sm-12">
        <div class="table-responsive">          

            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item active">
                <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Próximos Eventos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Eventos Anteriores</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <?php 
                //  pre($eventos);
                ?>
                <table class="table table-striped  no-margin table-bordered table-hover" id="sample_1"> <!-- adicionar na class "tabela" -->
                    <thead style="background: #0badad; color: white;font-weight: bold;">
                        <tr>
                            <th>ID</th>
                            <th>Nome do Evento</th>
                            <th>Mês</th>
                            <th>Data</th>
                            <th>Modalidade</th>
                            <th>Local</th>
                            <th>Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'UTF8', 'pt_BR.UTF-8', 'portuguese');
                        date_default_timezone_set('America/Sao_Paulo');

                        $date_now = date("Y-m-d"); // this format is string comparable
                        foreach ($eventos as $evento):
                                if($date_now <= $evento->eve_data_fim): // próximos eventos
                        ?>
                        <tr style="border-bottom: 1px solid #ccc;">
                            <td> <a href="{base_url}evento/{Ambiente}/Evento/Dashboard/<?php echo $evento->eve_id; ?>"> #<?php echo $evento->eve_id;// id ?> </a>
                            <td> <a href="{base_url}evento/{Ambiente}/Evento/Dashboard/<?php echo $evento->eve_id; ?>"><?php echo $evento->eve_nome; ?> </a>
                            <td><?php  echo "<small style='color: transparent; font-size: 0px;'>". date("m", strtotime($evento->eve_data_inicio)). "</small> ". ucfirst(strftime('%B', strtotime($evento->eve_data_inicio))); ?></td>
                            <td><?php
                                echo date("d/m/Y", strtotime($evento->eve_data_inicio)) . "<small> até </small>". date("d/m/Y", strtotime($evento->eve_data_fim));
                            ?></td>
                            <td><?php echo $evento->eve_modalidade ; ?></td>
                            <td><?php echo $evento->eve_local ; ?></td>
                            <td><?php echo str_replace("[ Evento ] ", "", $evento->eve_status) ; ?></td>
                        </tr>
                        <?php
                                endif;
                            endforeach;
                        ?>
                    </tbody>
                </table>

              </div>
              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                <table class="table table-striped  no-margin table-bordered table-hover" id="sample_2"> <!-- adicionar na class "tabela" -->
                    <thead style="background: #0badad; color: white;font-weight: bold;">
                        <tr>
                            <th>ID</th>
                            <th>Nome do Evento</th>
                            <th>Mês</th>
                            <th>Ano</th>
                            <th>Modalidade</th>
                            <th>Local</th>
                            <th>Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($eventos as $evento):
                                if($date_now > $evento->eve_data_fim): // próximos eventos
                        ?>
                        <tr style="border-bottom: 1px solid #ccc;">
                            <td> <a href="{base_url}evento/{Ambiente}/Evento/Dashboard/<?php echo $evento->eve_id; ?>"> #<?php echo $evento->eve_id;// id ?> </a>
                            <td> <a href="{base_url}evento/{Ambiente}/Evento/Dashboard/<?php echo $evento->eve_id; ?>"><?php echo $evento->eve_nome; ?> </a>
                            <td><?php echo "<small style='color: transparent; font-size: 0px;'>". date("m", strtotime($evento->eve_data_inicio)). "</small> ".ucfirst(strftime('%B', strtotime($evento->eve_data_inicio))); ?></td>
                            <td><?php echo date("Y",strtotime($evento->eve_data_inicio)) ; ?></td>
                            <td><?php echo $evento->eve_modalidade ; ?></td>
                            <td><?php echo $evento->eve_local ; ?></td>
                            <td><?php echo str_replace("[ Evento ] ", "", $evento->eve_status) ; ?></td>
                        </tr>
                        <?php
                                endif;
                            endforeach;
                        ?>
                    </tbody>
                </table>
              </div>
            </div>

        </div>


    </div>