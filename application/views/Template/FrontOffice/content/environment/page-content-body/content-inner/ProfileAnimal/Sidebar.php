
<style type="text/css">
    .profile-usermenu-perfis{
        margin-top: 0px;
    }
</style>

<div class="row">
    
    <div class="col-sm-12">

        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="http://www.racingzone.com.au/images/unknown_horse.png" class="img-responsive" alt=""> </div>
                <!-- END SIDEBAR USERPIC -->


                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> <?php echo $this->model_crud->get_rowSpecific('tb_animal', 'ani_id', $AnimalId, 1, 'ani_nome_completo'); ?> </div>
                    <!-- <div class="profile-usertitle-job"> Developer </div> -->
                </div>
                <!-- END SIDEBAR USER TITLE -->

                <!-- SIDEBAR BUTTONS -->
    <!-- 
                <div class="profile-userbuttons">
                    <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                    <button type="button" class="btn btn-circle red btn-sm">Message</button>
                </div>
    -->
                <!-- END SIDEBAR BUTTONS -->

                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Dashboard' ) ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Animal/Dashboard/{AnimalId}"> <i class="fa fa-caret-right"></i> Início </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'PerfilCompleto') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Animal/PerfilCompleto/{AnimalId}"> <i class="fa fa-caret-right"></i> Perfil Completo </a>
                        </li>

                        <!-- 
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'HistoricoRegistro') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Telefone"> <i class="fa fa-caret-right"></i> Histórico de Registro </a>
                        </li>
                        
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'HistoricoVinculo') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Telefone"> <i class="fa fa-caret-right"></i> Histórico de Vínculo </a>
                        </li>

                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'HistoricoPropriedade') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Email"> <i class="fa fa-caret-right"></i> Histórico de Propriedade </a>
                        </li>
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'HistoricoParticipacao') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/Endereco"> <i class="fa fa-map-marker"></i> Histórico de Participação </a>
                        </li>
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Financeiro') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/TrocarSenha"> <i class="fa fa-key"></i> Financeiro </a>
                        </li>
                        <li class="<?php echo (isset($NavActiveSidebar) && $NavActiveSidebar == 'Passaporte') ? 'active' : ''; ?>">
                            <a href="{base_url}FrontOffice/Perfil/TrocarSenha"> <i class="fa fa-key"></i> Passaporte </a>
                        </li>
                         -->
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->

        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
