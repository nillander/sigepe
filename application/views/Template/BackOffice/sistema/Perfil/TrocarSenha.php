

    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content" id="perfil-trocar-senha">

        <div class="row">

            <div class="col-md-12">
            
                <div class="portlet light porlet-main">

                    <div class="portlet-title">
                        
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> TROCAR SENHA</span>
                            <!-- <span class="caption-helper">Preencha o formulário para trocar sua senha.</span> -->
                        </div>
                        
                    </div>

                    <div class="portlet-body porlet-body-main">


                        <!-- Formulario -->
                        <form action="#" role="form" id="form-trocar-senha" class="form-horizontal form-view">
                            <div class="form-body">

                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-yellow-crusta">
                                            <i class="icon-share font-yellow-crusta"></i>
                                            <span class="caption-subject bold uppercase"> Formulário</span>
                                            <span class="caption-helper">Preencha o formulário abaixo para trocar a senha da sua conta no SIGEPE.</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">

                                        <div class="alert alert-danger display-hide">
                                            <button class="close" data-close="alert"></button>
                                            Preencha todos os campos do formulário.
                                        </div>
                                        
                                        <div class="alert alert-success display-hide">
                                            <button class="close" data-close="alert"></button>
                                            Formulário validado!
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Senha Atual
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="password" class="form-control" name="senha-atual" placeholder=""> </div>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nova Senha
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="password" class="form-control" name="nova-senha" id="nova-senha" placeholder=""> </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Confirmar Nova Senha
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <input type="password" class="form-control" name="confirmar-senha" id="confirmar-senha" placeholder=""> </div>
                                            </div>
                                        </div>



                                    </div>
                                    <!-- /.portlet-body -->

                                </div>
                                <!-- /.portlet -->
 
                            </div>
                            <!-- /form-body -->

                            <div class="form-actions text-left">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <a class="btn blue btn-submit">
                                            <i class="fa fa-change" aria-hidden="true"></i>
                                            Trocar Senha
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <!-- /form-->

                    </div>
                    <!-- /portlet-body -->

                </div>
                <!-- /.portlet -->

            </div>
            <!-- /.col-md-12 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- END PROFILE CONTENT -->



    </div>  
    <!-- div aberta em sidebar.php ( /.col-sm-12 ) -->

</div>
<!-- div aberta em sidebar.php ( /.row ) -->