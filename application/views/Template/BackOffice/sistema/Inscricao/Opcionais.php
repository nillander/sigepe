<div class="container page-inscricao bloco-series-provas">

    <div class="row inscricao-titulo-secao">
      <div class="col-sm-12">
        <ul>
          <li class="numero-bloco">03</li>
          <li class="titulo">ESCOLHA <b>OPCIONAIS</b></li>
        </ul>
      </div>

    </div>

    <div class="row">
      <div class="col-sm-12">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
        <hr class="separador-inscricao">
      </div>
    </div>


    <div class="row">

      <div class="col-sm-12 aviso-selecione-conjunto">
        <div class="alert alert-warning">
          <strong>Atenção!</strong> Selecione um conjunto para continuar a inscrição.
        </div>
      </div>

      <div class="col-sm-12 aviso-baia-qs-desabilitado" style="display: none;">
        <div class="alert alert-warning">
          <strong>Atenção!</strong> Para este evento baia e quarto de sela estão inativos.
        </div>
      </div>



    </div>

</div>
