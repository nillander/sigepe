<?php 
?>

<div class="profile-content" id="meu-perfil">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light " style="width: 100%; float: left;">


                    <div class="portlet-title">
                        <div class="caption font-blue-madison">
                            <span class="caption-subject bold uppercase"> Todos Atletas</span>
                            <span class="caption-helper">Informações dos atletas</span>
                        </div>
                        <div class="actions">
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">

                                <form role="form" action="#" class="form-view" id="form-perfil">
                                    <div class="table-scrollable table-scrollable-borderless">
                                        <table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_2">
                                        
                                            <thead>
                                                <tr class="uppercase">
                                                    <th>ID Vínculo</th>
                                                    <th>Tipo</th>
                                                    <th>Agente</th>
                                                    <th>Autor</th>
                                                    <th>Criado</th>
                                                    <th>Status</th>
                                                    <th>Ação</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                <?php foreach ($dataset as $key => $ob): ?>
                                                <tr>

                                                    <td> <?php echo $ob->vin_id; ?> </td> <!-- ID Vínculo -->
                                                    <td> <?php echo $ob->vin_tipo_vinculo; ?> </td> <!-- Tipo -->
                                                    <td> <?php echo $ob->fk_agente_chave; ?> </td> <!-- Agente -->
                                                    <td> <?php echo $ob->vin_autor; ?> </td> <!-- Autor -->
                                                    <td> <?php echo date('m/d/Y H:m:s',strtotime($ob->vin_criado)); ?> </td> <!-- Criado -->
                                                    <td>
                                                        <?php $color = "dark";
                                                        switch ($ob->vin_fk_sta_id) {
                                                            case 1:
                                                                $color = "green";
                                                                break;
                                                            case 2:
                                                                $color = "red";
                                                                break;

                                                            default:
                                                                # code...
                                                                break;
                                                        }
                                                        ?>

                                                        <span class="badge badge-<?php echo $color; ?>">
                                                            <?php 
                                                                $labelStatus = explode("] ", $ob->vin_status);
                                                                if (sizeof($labelStatus) == 2) {
                                                                    $labelStatus = explode(" ", $labelStatus[1]);
                                                                    foreach ($labelStatus as $palavra) {
                                                                        echo $palavra."<br>";
                                                                    }
                                                                } else {
                                                                    echo $ob->vin_status;
                                                                }
                                                            ?>
                                                                
                                                        </span>
                                                    </td> <!-- Status -->
                                                    <td>
                                                        <!-- <button type="button" class="btn btn-default"><i class="fa fa-check"></i></button> -->
                                                        <button type="button" class="btn btn-default"><i class="fa fa-remove"></i></button>
                                                    </td> <!-- Ação -->
                                                    
                                                </tr>
                                                <?php endforeach; ?>

                                            </tbody>

                                        </table>
                                    </div>
                                </form>
                            </div>
                            <!-- END PERSONAL INFO TAB -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>