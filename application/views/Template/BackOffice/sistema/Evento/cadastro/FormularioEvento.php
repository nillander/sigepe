<style type="text/css">

.easy-autocomplete-container{
    top: 25px;
}
.easy-autocomplete{
    width: 100% !important;
}
#nome-proprietario{
    text-transform: uppercase;
}
.page-header .page-header-menu.fixed{
    position: relative;
}
.form-actions{
}
.form .form-actions, .portlet-form .form-actions{
    padding: 20px !important;
    margin: 0 !important;
    background-color: #f5f5f5 !important;
    border-top: 1px solid #e7ecf1 !important;     
}
#baia-error,
#quarto-de-sela-error,
#ativar-site-error{
    display: block;
    width: 100%;
    float: left;    
}
</style>


    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <span class="caption-subject bold uppercase"> Evento</span>
                <span class="caption-helper">Formulário de Cadastro</span>
            </div>
        </div>
        <div class="portlet-body">


            <form class="form form-horizontal" role="form" id="form-evento" action="{base_url}evento/CadastroEvento/Processar" enctype="multipart/form-data" method="post">
               
                <div class="form-body">

                    <!-- MODALIDADE -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Modalidade
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="modalidade" id="modalidade">
                                <option value="">- Selecione uma Modalidade -</option>
                                <?php foreach ($DatasetModalidade as $key => $value): ?>
                                <option value="<?php echo $value->evm_id; ?>"><?php echo $value->evm_modalidade; ?></option>
                                <?php endforeach; ?>
                            </select>      
                        </div>
                    </div>


                    <!-- NOME EVENTO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Nome do Evento <span class="required" aria-required="true">
                        * </span>
                        </label>
                        <div class="col-md-6">
                            <textarea name="nome-evento" class="form-control" cols="40" rows="4" style="width:100%;"></textarea>
                        </div>
                    </div>
            

                    <!-- TIPO DE EVENTO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Tipo de Evento
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="tipo-evento[]" id="tipo-evento" multiple style="height: 250px;">
                                <?php foreach ($DatasetTipoEvento as $key => $value): ?>
                                <option value="<?php echo $value->evt_id; ?>"><?php echo $value->evt_sigla . ' - ' . $value->evt_tipo; ?></option>
                                <?php endforeach; ?>
                            </select>      
                        </div>
                    </div>
                    

                    <!-- PERIODO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Periodo
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-6">
                            <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                                <input type="text" class="form-control" name="data-inicio">
                                <span class="input-group-addon"> até </span>
                                <input type="text" class="form-control" name="data-fim">
                            </div>
                        </div>
                    </div>
                    


                    <!-- DATA LIMITE SEM ACRESCIMO  -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Data Limite s/ Acrescimo
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-9">

                            <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                                <input type="text" class="form-control" name="data-limite-sem-acrescimo">
                            </div>

                        </div>

                    </div>
            

                    <!-- VENDA DE INSCRICOES POR -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Venda de Inscrições por
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="tipo-venda" id="tipo-venda">
                                <option value="">- Selecione um Tipo -</option>
                                <?php foreach ($DatasetTipoInscricao as $key => $value): ?>
                                <option value="<?php echo $value->evv_id; ?>"><?php echo $value->evv_venda; ?></option>
                                <?php endforeach; ?>
                            </select>      
                        </div>
                    </div>
                    

                    <!-- DESENHADOR DE PERCURSO -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Desenhador de Percurso
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="desenhador-percurso[]" id="desenhador-percurso" multiple>
                                <?php foreach ($DatasetDesenhador as $key => $value): ?>
                                <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                <?php endforeach; ?>
                            </select>      
                        </div>
                    </div>



                    <!-- BAIAS -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Baias
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="form-control" style="border:none;">
                                
                                <input type="radio" name="baia" value="1" id="baia-sim">
                                <label for="baia-sim" style="margin-right: 15px;">SIM</label>

                                <input type="radio" name="baia" value="2" id="baia-nao">
                                <label for="baia-nao">NÃO</label>

                            </div>
                            <!-- /input-group -->

                        </div>
                    </div>
                    <!-- fim baia -->



                    <!-- QUARTO DE SELA -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Quarto de Sela
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-9">
                            <div class="form-control" style="border:none;">
                                <input type="radio" name="quarto-de-sela" value="1" id="quarto-de-sela-sim">
                                <label for="quarto-de-sela-sim" style="margin-right: 15px;">SIM</label>
                                <input type="radio" name="quarto-de-sela" value="2" id="quarto-de-sela-nao">
                                <label for="quarto-de-sela-nao">NÃO</label>
                            </div>
                        </div>
                    </div>



                    <!-- LOCAL -->
                    <div class="form-group">
                        <label class="control-label col-md-3">
                            Local
                            <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-5">
                            <select class="form-control" name="local" id="local">
                                <option value="">- Selecione um Local -</option>
                                <?php foreach ($DatasetLocal as $key => $value): ?>
                                <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                <?php endforeach; ?>=
                            </select>      
                        </div>
                    </div>



                    <!-- SITE -->
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-yellow-crusta">
                                <i class="icon-share font-yellow-crusta"></i>
                                <span class="caption-subject bold uppercase"> SITE</span>
                            </div>
                        </div>
                        <div class="portlet-body">


                            <!-- Ativar no Site -->
                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    Ativar no Site
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <div class="form-control" style="border:none;">
                                        <input type="radio" name="ativar-site" value="1" id="ativar-site-sim">
                                        <label for="ativar-site-sim" style="margin-right: 15px;">SIM</label>

                                        <input type="radio" name="ativar-site" value="2" id="ativar-site-nao">
                                        <label for="ativar-site-nao">NÃO</label>
                                    </div>
                                </div>
                            </div>

                            
                            <!-- Abertura das Inscricoes -->
                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    Abertura das Inscrições
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-5">

                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control" name="abertura-inscricoes">
                                    </div>

                                </div>
                            </div>


                            <!-- Encerramento das Inscricoes -->
                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    Encerramento das Inscrições
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-5">

                                    <div class="input-group date-picker input-daterange" data-date="10/11/2012" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control" name="encerramento-inscricoes">
                                    </div>

                                </div>
                            </div>


                            <?php
                            /*

                            <!-- PROGRAMA -->
                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    Programa
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-6">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="input-group input-large">
                                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                <span class="fileinput-filename"> </span>
                                            </div>
                                            <span class="input-group-addon btn default btn-file">
                                                <span class="fileinput-new"> Selecione um PDF </span>
                                                <span class="fileinput-exists"> Trocar </span>
                                                <input type="file" name="programa"> </span>
                                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                        </div>
                                        <span id="programa-error-camada" class="help-block help-block-error"></span>
                                    </div>
                                </div>
                            </div>


                                                <input type="file" name="teste"> </span>


                            <!-- LOGOTIPO -->
                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    Logotipo
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-6">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="input-group input-large">
                                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                <span class="fileinput-filename"> </span>
                                            </div>
                                            <span class="input-group-addon btn default btn-file">
                                                <span class="fileinput-new"> Selecionar Logotipo </span>
                                                <span class="fileinput-exists"> Trocar </span>
                                                <input type="file" name="logotipo">
                                            </span>
                                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remover </a>
                                        </div>
                                    </div>
                                    <span class="help-block"> Envie a imagem na resolução 400x400 (pixels) <br> Formato ideal: PNG </span>
                                    <span id="logotipo-error-camada" class="help-block help-block-error"></span>
                                </div>
                            </div>
                            */
                            ?>


                            <!-- DESCRICAO -->
                            <div class="form-group">
                                <label class="control-label col-md-3">
                                    Descrição
                                </label>
                                <div class="col-md-9">
                                    <textarea class="wysihtml5 form-control" rows="6" name="descricao"></textarea>
                                </div>
                            </div>


                        </div>
                        <!-- /porlet-body (site ) -->

                    </div>
                    <!-- portlet(site) -->


                </div>
                <!-- /form-body -->
                  
                <div class="form-actions">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn green">Cadastrar Evento</button>
                    </div>
                </div>
                <!-- /form-actions -->

            </form>

        </div>
        <!-- /portlet-body -->

    </div>
    <!-- /portlet -->












