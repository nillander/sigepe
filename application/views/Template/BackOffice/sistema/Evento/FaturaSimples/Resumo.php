<style type="text/css">
	.titulo-secao-resumo{
		text-align: center;
	}
	#preco-total{
	    letter-spacing: -3px;
	    font-weight: 400;
	    font-size: 50px;
	    color: #000;
	}
	#btn-boleto{
		background-color: #3de43d;
		border-color: #1fc11f;
		border-radius: 7px !important;
		margin-top: 30px;
		margin: 30px auto;
		font-weight: bold;
		color: #0d3c0d;
		padding: 23px;
		transition: .5s;
	}
	#btn-boleto:hover{
	    background-color: #187e18;
	    border-color: #0d5f0d;
	    color: white;
	}
</style>


<?php $this->load->module('evento/FrontOffice/FaturaSimples'); ?>


    <!--
    	AGRUPAR AVISOS EM DOIS GRANDES BLOCOS. POLITICA DE PRECO 1 E 2 .
    	SEPARAR ARQUIVOS DE RESUMO EM CAMADAS. ARQUIVO: AVISOS | ETC ...
     -->



    <!--
    	Aviso fim do preco promocional.
    	So e exibido esse bloco se a politica do preco for 1 e NAO for o ultimo dia para encerrar a data limite.
    -->
    <?php if( isset($PoliticaPreco) && $PoliticaPreco == '1' && !isset($AvisoEncerramentoDataLimiteSemAcrescimo)): ?>
	<div class="alert alert-warning" style="text-align: center; ">
		<strong>Atenção</strong> <br>
		Pague a inscrição até <?php echo strftime('%A, %d de %B', strtotime($DataLimiteSemAcrescimo)); ?> e garanta o desconto.<br>
		Após esse prazo os valores serão atualizados.<br>
		<small>
			<b>
				<?php
					$DiasParaFimPrecoPromocional 	=	DateDifferences($DataLimiteSemAcrescimo, date("Y-m-d"), 'd');
					echo ($DiasParaFimPrecoPromocional == 1) ? 'Encerra amanhã o preço promocional. Aproveite!' : 'Faltam ' . $DiasParaFimPrecoPromocional . ' dias para encerrar o preço promocional';
				?>
			</b>
		</small>
	</div>
    <?php endif; ?>



    <!--
    	// Encerra Hoje preço promocional
    	Aviso fim do preco promocional. ( hoje )
    	So e exibido esse bloco se a politica do preco for 1 e se for o ultimo dia para encerrar a data limite.
    -->
    <?php if( isset($PoliticaPreco) && $PoliticaPreco == '1' && isset($AvisoEncerramentoDataLimiteSemAcrescimo)): ?>
	<div class="alert alert-danger" style="text-align: center; ">
		<strong>Atenção</strong> <br>
		Pague a inscrição <b>hoje <?php echo strftime('%A, %d de %B', strtotime($DataLimiteSemAcrescimo)); ?></b> e garanta o desconto.<br>
		Amanhã os valores já serão atualizados<br>
	</div>
    <?php endif; ?>



    <!--
    	AVISO FIM DAS INSCRICOES
    	- Aviso amarelo sinalizando o dia do fim das inscricoes. Alertar que apos esse prazo nao sera permitido inscricoes no sistema somente via federacao com preço diferente.
    	- Aviso vermelho sinalizando que hoje e o ultimo dia e amanha já nao será possivel fazer inscrição no sistema
    -->










    <hr style="margin-top: 10px;margin-bottom:0px;border: 1px dotted #ddd;float:left;width: 100%">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:0px;border: 1px dotted #ddd;">
    <hr style="margin-top: 0px;margin-bottom:40px;border: 1px dotted #ddd;float:left;width: 100%;">
