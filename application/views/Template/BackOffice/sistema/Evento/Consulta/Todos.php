<style type="text/css">
.h4, .h5, .h6, h4, h5, h6 {
    margin-top: 0px;
    margin-bottom: 5px;
}
hr, p {
    margin: 10px 0;
}
.portlet.light.bordered {
    border-bottom: 3px solid rgba(204, 204, 204, 0.45) !important;
}
</style>


    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light bordered portlet-modalidade-tabela" style="">
        <div class="portlet-title">
            <div class="caption font-green-sharp">
                <i class="icon-speech font-green-sharp"></i>
                <span class="caption-subject bold uppercase"> Tabela</span>
                <span class="caption-helper">Relação de todos os eventos </span>
            </div>
            <div class="actions">
                <!-- <a style="margin-bottom: 50px;" class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a> -->
            </div>
        </div>

        <div class="portlet-body">
            <table id="tabela-consulta-evento" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome do Evento</th>
                        <th>Modalidade</th>
                        <th>Período</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Nome do Evento</th>
                        <th>Modalidade</th>
                        <th>Período</th>
                        <th>Status</th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach ($DatasetEvento as $key => $ValueEvento): ?>
                    <tr>
                        <td> <a href="{base_url}BackOffice/Evento/Dashboard/Detalhar/<?php echo $ValueEvento->eve_id; ?>"> #<?php echo $ValueEvento->eve_id;// id ?> </a> </td>
                        <td> <a href="{base_url}BackOffice/Evento/Dashboard/Detalhar/<?php echo $ValueEvento->eve_id; ?>"> <?php echo nl2br($ValueEvento->eve_nome); // nome ?> </a> </td>
                        <td> <?php echo $ValueEvento->evm_modalidade; // modalidade ?> </td>
                        <td style="text-align: center;"> <?php
                                $dataInicio    =   new DateTime($ValueEvento->eve_data_inicio);
                                $anoDataInicio   =   $dataInicio->format("d/m");
                                
                                $dataFim    =   new DateTime($ValueEvento->eve_data_fim);
                                $anoDataFim   =   $dataFim->format("d/m");


                                echo $anoDataInicio ." - ". $anoDataFim."<br>";
                                echo "<small>". $dataFim->format("Y") ."</small>"
                                
                             ?>
                        </td>
                        <td>
                            <?php 
                            $corLabel = "";
                            switch ($ValueEvento->fk_sta_id) {
                                case 400:
                                    $corLabel = "primary";
                                    break;
                                    
                                case 401:
                                    $corLabel = "primary";
                                    break;

                                case 402:
                                    $corLabel = "info";
                                    break;

                                case 403:
                                    $corLabel = "success";
                                    break;

                                case 404:
                                    $corLabel = "danger";
                                    break;

                                case 405:
                                    $corLabel = "warning";
                                    break;
                                
                                default:
                                    $corLabel = "default";
                                    break;
                            }
                            echo 
                            "<span class='label label-".$corLabel."'>". str_replace("[ Evento ] ", "", $ValueEvento->sta_status) ."</span>";
                            ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->