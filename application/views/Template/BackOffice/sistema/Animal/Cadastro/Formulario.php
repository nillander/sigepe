<style type="text/css">

.easy-autocomplete-container{
    top: 25px;
}
.easy-autocomplete{
    width: 100% !important;
}
#nome-proprietario{
    text-transform: uppercase;
}

</style>



        <div class="portlet light " id="form_wizard_1">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-red"></i>
                    <span class="caption-subject font-red bold uppercase"> Formulário -
                        <span class="step-title"> Etapa 1 de 3 </span>
                    </span>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal" action="#" id="form-animal" method="POST">
                    <div class="form-wizard">
                        <div class="form-body">
                            <ul class="nav nav-pills nav-justified steps">
                                <li>
                                    <a href="#tab1" data-toggle="tab" class="step">
                                        <span class="number"> 1 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Vínculos </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab" class="step">
                                        <span class="number"> 2 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Informações do Animal </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab3" data-toggle="tab" class="step">
                                        <span class="number"> 3 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Conclusão </span>
                                    </a>
                                </li>
                            </ul>


                            <div id="bar" class="progress progress-striped" role="progressbar">
                                <div class="progress-bar progress-bar-success"> </div>
                            </div>



                            <!-- 
                            TAB-CONTENT
                            ================================================ -->
                            <div class="tab-content">
                            
                                <!-- ALERTS -->
                                <div class="alert alert-danger display-none">
                                    <button class="close" data-dismiss="alert"></button> Existe alguns campos no formulário que são obrigatórios ou que não foram preenchidos corretamente. Confira antes de avançar. </div>

                                <div class="alert alert-success display-none">
                                    <button class="close" data-dismiss="alert"></button> Formulário validado com sucesso! </div>





                                <!-- TAB1 -->
                                <div class="tab-pane" id="tab1">


                                    <div class="portlet-title col-sm-12 margin-top-30">
                                        <div class="col-sm-3 text-right">
                                            <div class="caption text-right">
                                                <i class=" icon-layers font-red"></i>
                                                <span class="caption-subject font-red bold uppercase"> PROPRIETÁRIO </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="float: left; width: 100%;">


                                    <!--
                                        Autor
                                    ==============================================================-->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Autor
                                            <span class="required" aria-required="true">* </span>
                                        </label>
                                        <div class="col-md-4">
                                            <span class="form-control-static">

                                                <?php
                                                    echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $this->session->userdata('PessoaId'), 1, 'pes_nome_razao_social');
                                                ?>
                                                <br>
                                                <small style="font-size:12px;">
                                                    <?php
                                                        echo "
                                                            CPF: ".$this->my_pessoa->InserirPontuacaoCpfCnpj($this->session->userdata('PessoaCpf'))." |
                                                            ID: #".$this->session->userdata('PessoaId')."
                                                        ";
                                                    ?>
                                                </small>

                                            </span>
                                        </div>
                                    </div>


                                    <!-- PROPRIETARIO FLAG -->
                                    <div class="form-group" id="camada-flag-proprietario-animal">
                                        <label class="control-label col-md-3">Você é o proprietário do animal? <span class="required" aria-required="true">
                                        * </span>
                                        </label>
                                        <div class="col-md-4">

                                            <div id="layer-radio-proprietario-flag">
                                                <label id="label-proprietario-flag-sim">
                                                    <input type="radio" name="proprietario-flag" value="1" data-title="SIM">
                                                    SIM
                                                </label>
                                                <label>
                                                    <input type="radio" name="proprietario-flag" value="2" data-title="NÃO">
                                                    NÃO
                                                </label>
                                                <div id="form-proprietario-animal-error"> </div>
                                            </div>

                                        </div>
                                    </div>


                                    <?php
                                    /*
                                    
                                    // BLOCO EM FUNCIONAMENTO - BUSCA PELO CPF DO PROPRIETARIO

                                    <!-- PROPRIETARIO -->
                                    <div class="form-group" id="camada-cpf-cnpj-proprietario" style="display: none;">
                                        <label class="control-label col-md-3">CPF do Proprietário <span class="required" aria-required="true">
                                        * </span>
                                        </label>
                                        <div class="col-md-4">
                                            

                                            <!-- PROPRIETARIO LOCALIZAR -->
                                            <div id="camada-cpf-cnpj-proprietario-localizar">
                                                <div class="input-group">
                                                    <input type="text" class="form-control mask-cpf" name="cpf-cnpj-proprietario" id="cpf-cnpj-proprietario" placeholder="Digite o CPF" maxlength="3">
                                                    <span class="input-group-btn">
                                                        <button class="btn red" type="button" id="btn-localizar-proprietario">
                                                            Localizar Proprietário
                                                            <i class="m-icon-swapright m-icon-white"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <span class="help-block">
                                                    <i class="fa fa-info-circle"></i>
                                                    Digite o CPF do proprietário e clique em Localizar Proprietário
                                                </span>
                                            </div>


                                            <!-- PROPRIETARIO LOCALIZADO -->
                                            <div id="camada-cpf-cnpj-proprietario-localizado" style="display: none;">
                                                <div class="input-group">

                                                    <span class="form-control-static">
                                                       <span class="nome">{PessoaNomeCompleto}</span><br>
                                                        <small style="font-size:12px;">
                                                            CPF: <span class="cpf-cnpj">000.000.000-00</span> |
                                                            ID: #<span class="id">-</span>
                                                        </small>
                                                    </span>

                                                    <div style="margin-top: 10px;">
                                                        <a href="#" title="" class="label label-danger" id="btn-trocar-proprietario">
                                                            Trocar Proprietário
                                                        </a>                                                    
                                                    </div>

                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    */
                                    ?>

                                    <!-- PROPRIETARIO -->
                                    <div class="form-group" id="camada-cpf-cnpj-proprietario" style="display: none;">
                                        <label class="control-label col-md-3">Nome do Proprietário <span class="required" aria-required="true">
                                        * </span>
                                        </label>
                                        <div class="col-md-5">
                                            

                                            <!-- PROPRIETARIO LOCALIZAR -->
                                            <div class="input-group" id="camada-proprietario-localizar" style="display: block;">
                                                <input type="text" class="form-control" name="nome-proprietario" id="nome-proprietario" placeholder="Digite o nome do proprietário" style="width: 100%;" >
                                            </div>


                                            <!-- PROPRIETARIO LOCALIZADO -->
                                            <div id="camada-proprietario-localizado" style="display: none;">
                                                <div class="input-group">

                                                    <span class="form-control-static">
                                                       <span class="nome">{PessoaNomeCompleto}</span><br>
                                                        <small style="font-size:12px;">
                                                            ID: #<span class="id">-</span>
                                                        </small>
                                                    </span>

                                                    <div style="margin-top: 10px;">
                                                        <a href="#" title="" class="label label-danger" id="btn-trocar-proprietario">
                                                            Trocar Proprietário
                                                        </a>                                                    
                                                    </div>

                                                </div>
                                            </div>


                                        </div>
                                    </div>




                                    <!-- Responsavel Financeiro -->
                                    <?php
                                    /*
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Responsável Financeiro
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="registro-genealogico" id="registro-genealogico">
                                        </div>
                                        <div class="col-md-3">
                                            <a href="javascript:;" class="btn green m-icon" style="line-height: inherit !important;"> Localizar
                                                <i class="m-icon-swapright m-icon-white"></i>
                                            </a>   
                                        </div>
                                    </div>
                                    */
                                    ?>


                                    <div class="portlet-title col-sm-12 margin-top-30">
                                        <div class="col-sm-3 text-right">
                                            <div class="caption text-right">
                                                <i class=" icon-layers font-red"></i>
                                                <span class="caption-subject font-red bold uppercase"> VÍNCULOS DO ANIMAL </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="float: left; width: 100%;">


                                    <div class="form-group">
                                        <label class="control-label col-md-3">Confederação
                                        <span class="required" aria-required="true"> * </span>
                                        </label>
                                        <div class="col-md-4">CBH - Confederação Brasileira de Hipismo</div>
                                    </div>


                                    <!-- Federacao -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Federação
                                        <span class="required" aria-required="true"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="federacao" id="federacao">
                                                <option value="">- Seleciona sua Federação -</option>
                                                <?php
                                                    $Contador = 1;
                                                    foreach ($DatasetFederacao as $key => $value):
                                                        if($Contador==2)
                                                            echo "<option disabled>──────────────────────────────────────────────────</option>";
                                                ?>
                                                <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                                <?php
                                                    $Contador++;
                                                    endforeach;
                                                ?>
                                            </select>                                    
                                        </div>
                                    </div>


                                    <!-- Entidade Filiada -->
                                    <div class="form-group display-none" id="form-group-entidade-filiada">
                                        <label class="control-label col-md-3"> Entidade Filiada
                                        </label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="entidade-filiada" id="entidade-filiada">
                                                <option value="">- Seleciona sua Entidade Filiada -</option>
                                                <?php foreach ($DatasetEntidadeFiliada as $key => $value): ?>
                                                <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                                <?php endforeach; ?>
                                            </select>                                    
                                        </div>
                                    </div>


                                    <!-- Escola de Equitacao -->
                                    <div class="form-group display-none" id="form-group-escola-equitacao">
                                        <label class="control-label col-md-3"> Escola de Equitação
                                        </label>
                                        <div class="col-md-8">
                                            <select class="form-control" name="escola-equitacao" id="escola-equitacao">
                                                <option value="">- Seleciona sua Escola de Equitação -</option>
                                                <?php foreach ($DatasetEscolaEquitacao as $key => $value): ?>
                                                <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                                <?php endforeach; ?>
                                            </select>                                    
                                        </div>
                                    </div>

                                    <div class="form-group display-none" id="form-group-link-remover-entidade">
                                        <label class="control-label col-md-3"> 
                                        </label>
                                        <div class="col-md-4">
                                            <a href="#" title="" class="label label-danger" id="link-trocar-entidade">Trocar Entidade/Escola</a>
                                        </div>
                                    </div>


                                    <input type="hidden" id="proprietario-id" name="proprietario-id" value="">
                                    <input type="hidden" id="proprietario-cpf-cnpj" name="proprietario-cpf-cnpj" value="">
                                    <input type="hidden" id="proprietario-nome" name="proprietario-nome" value="">


                                </div>
                                <!-- tab-pane / #tab1 -->



                                <!-- TAB2 -->
                                <div class="tab-pane active" id="tab2">

                                    <div class="portlet-title col-sm-12">
                                        <div class="col-sm-3 text-right">
                                            <div class="caption text-right">
                                                <i class=" icon-layers font-red"></i>
                                                <span class="caption-subject font-red bold uppercase"> INFORMAÇÕES GERAIS </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="float: left; width: 100%;">



                                    <!-- Nº Chip -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nº Chip
                                            <span class="required" aria-required="true"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="numero-chip" id="numero-chip" >
                                            <span id="numero-chip-error" class="help-block help-block-error valid" style="display: inline;"></span>
                                            <span class="help-block">
                                                <i class="fa fa-info-circle"></i>
                                                Nº do Chip do Animal - Consta do Passaporte do Animal
                                            </span>
                                        </div>
                                    </div>



                                    <!-- Nº Passaporte -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nº Passaporte
                                            <span class="required" aria-required="true"> * </span>
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="passaporte" id="passaporte">
                                        </div>
                                    </div>


                                    <!-- Data de Nascimento -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Data de Nascimento 
                                            <span class="required" aria-required="true"> * </span>
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control mask-data text-center" placeholder="dd/mm/aaaa" name="data-nascimento" id="data-nascimento">
                                         </div>
                                    </div>


                                    <!-- NOME COMPLETO -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome Completo <span class="required" aria-required="true">
                                        * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="nome-completo" id="nome-completo">
                                            <span class="help-block">
                                                <i class="fa fa-info-circle"></i>
                                                Digite o nome exatamente como está no Passaporte
                                            </span>
                                        </div>
                                    </div>

                                    <!-- APELIDO -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome Patrocinado</label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="nome-patrocinado" id="nome-patrocinado">
                                        </div>
                                    </div>

                                    <!-- RACA -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Raça <span class="required" aria-required="true">
                                        * </span>
                                        </label>
                                        <div class="col-md-4">

                                            <select class="form-control" name="raca" id="raca">
                                                <option value="">- Selecione a raça do animal -</option>
                                                <?php
                                                    foreach ($DatasetRaca as $key => $value):
                                                ?>
                                                <option value="<?php echo $value->anr_id; ?>"><?php echo $value->anr_raca; ?></option>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </select>      

                                        </div>
                                    </div>


                                    <!-- PELAGEM -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Pelagem <span class="required" aria-required="true">
                                        * </span>
                                        </label>
                                        <div class="col-md-4">

                                            <select class="form-control" name="pelagem" id="pelagem">
                                                <option value="">- Selecione a pelagem do animal -</option>
                                                <?php
                                                    foreach ($DatasetPelagem as $key => $value):
                                                ?>
                                                <option value="<?php echo $value->anp_id; ?>"><?php echo $value->anp_pelagem; ?></option>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </select>      

                                        </div>
                                    </div>
                                    

                                    <!-- GÊNERO -->
                                    <div class="form-group" id="camada-genero">
                                        <label class="control-label col-md-3">Gênero <span class="required" aria-required="true">
                                        * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <div class="radio-list">
                                                <label>
                                                    <input type="radio" name="genero" value="1" data-title="Macho">
                                                    Macho
                                                </label>
                                                <label>
                                                    <input type="radio" name="genero" value="2" data-title="Fêmea ( Égua )">
                                                    Fêmea ( Égua )
                                                </label>
                                            </div>
                                            <div id="form_gender_error">
                                            </div>
                                        </div>
                                    </div>


                                    <!-- TIPO -->
                                    <div class="form-group display-none" id="camada-genero-tipo-macho">
                                        <label class="control-label col-md-3"> Tipo do Macho <span class="required" aria-required="true">
                                        * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <div class="radio-list">
                                                <label>
                                                <input type="radio" name="genero-tipo" value="1" data-title="Inteiro (Garanhão)">
                                                Inteiro (Garanhão) </label>
                                                <label>
                                                <input type="radio" name="genero-tipo" value="2" data-title="Castrado">
                                                Castrado </label>
                                            </div>
                                            <div id="form_gender_error">
                                            </div>
                                        </div>
                                    </div>


                                    <!-- PESO -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Peso
                                        </label>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <input type="text" class="form-control weight" placeholder="" aria-describedby="sizing-addon1" name="peso" id="peso">
                                                <span class="input-group-addon" id="sizing-addon1" style="background:#eee;">kg</span>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- ALTURA DA CRUZ -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Altura da Cruz
                                        </label>
                                        <div class="col-md-2">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="" aria-describedby="sizing-addon1" id="altura-cruz" name="altura-cruz">
                                                <span class="input-group-addon" id="sizing-addon1" style="background:#eee;">cm</span>
                                            </div>
                                        </div>
                                    </div>





                                    <div class="portlet-title col-sm-12 margin-top-30">
                                        <div class="col-sm-3 text-right">
                                            <div class="caption text-right">
                                                <i class=" icon-layers font-red"></i>
                                                <span class="caption-subject font-red bold uppercase"> IDENTIFICAÇÃO </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="float: left; width: 100%;">


                                    <!-- N FEI -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nº FEI
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="registro-fei" id="registro-fei">
                                        </div>
                                    </div>

                                    <!-- N CAPA FEI -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nº CAPA - FEI
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="registro-capa-fei" id="registro-capa-fei">
                                        </div>
                                    </div>

                                    <!-- N REGISTRO CBH -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            Nº Registro - CBH
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="registro-cbh" id="registro-cbh">
                                        </div>
                                    </div>






                                    <div class="portlet-title col-sm-12 margin-top-30">
                                        <div class="col-sm-3 text-right">
                                            <div class="caption text-right">
                                                <i class=" icon-layers font-red"></i>
                                                <span class="caption-subject font-red bold uppercase"> GENEALOGIA </span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="float: left; width: 100%;">



                                    <!-- N REGISTRO GENEALOGIA -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nº Registro Genealógico 
                                        </label>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="registro-genealogico" id="registro-genealogico">
                                        </div>
                                    </div>


                                    <!-- NOME ASSOCIACAO DE REGISTRO -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome Associação de Registro 
                                        </label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="associacao" id="associacao">
                                                <option value="">- Seleciona uma Associação de Registro -</option>
                                                <?php
                                                    foreach ($DatasetAssociacao as $key => $value):
                                                ?>
                                                <option value="<?php echo $value->pes_id; ?>"><?php echo $value->pes_nome_razao_social; ?></option>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </select>   
                                        </div>
                                    </div>


                                    <!-- PAIS DE ORIGEM -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">País de Origem </label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="pais-origem" id="pais-origem">
                                                <option value="">- Seleciona o País de Origem -</option>
                                                <?php
                                                    foreach ($DatasetPais as $key => $value):
                                                ?>
                                                <option value="<?php echo $value->pai_id; ?>"><?php echo $value->pai_pais; ?></option>
                                                <?php
                                                    endforeach;
                                                ?>
                                            </select>      
                                        </div>
                                    </div>


                                    <!-- Nome do Pai -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome do Pai </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="nome-pai" id="nome-pai">
                                        </div>
                                    </div>

                                    <!-- Nome da Mae -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome da Mãe </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="nome-mae" id="nome-mae">
                                        </div>
                                    </div>

                                    <!-- Nome do Avo Materno -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Nome do Avô Materno </label>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="nome-avo-materno" id="nome-avo-materno">
                                        </div>
                                    </div>


                                </div>
                                <!-- tab-pane / #tab2 -->





                                <div class="tab-pane" id="tab3">

                                    <div class="alert alert-warning">
                                        <strong>Atenção! </strong>Seu cadastro ainda não foi concluído. Confirme as informações abaixo e clique no botão <b>CADASTRAR</b>.
                                    </div>

                                    <h3 class="block">Confirme seus dados</h3>


                                    <!-- ROW ( Vinculos ) -->
                                    <div class="row">


                                        <div class="col-sm-6">

                                            <h4 class="form-section">Propriedade</h4>
                                            <div class="form-group">
                                                    <label class="control-label col-md-5">Autor:</label>
                                                    <div class="col-md-7">
                                                        <p class="form-control-static">
                                                            <?php
                                                                echo $this->model_crud->get_rowSpecific('tb_pessoa', 'pes_id', $this->session->userdata('PessoaId'), 1, 'pes_nome_razao_social');
                                                            ?>
                                                            <br>
                                                            <small style="font-size:12px;">
                                                                <?php
                                                                    echo "
                                                                        CPF: ".$this->my_pessoa->InserirPontuacaoCpfCnpj($this->session->userdata('PessoaCpf'))." |
                                                                        ID: #".$this->session->userdata('PessoaId')."
                                                                    ";
                                                                ?>
                                                            </small>
                                                        </p>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Você é o proprietário do animal:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="proprietario-flag"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group" id="form-group-resume-cpf-cnpj-proprietario" style="display: none;">
                                                <label class="control-label col-md-5">CPF do Proprietário:</label>
                                                <div class="col-md-7">
                                                    <p>
                                                        
                                                        <span class="nome">
                                                        </span>
                                                        <br>
                                                        <small style="font-size:12px;">
                                                            CPF: <span class="cpf-cnpj"></span> |
                                                            ID: <span class="id"></span>
                                                        </small>

                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">

                                            <h4 class="form-section">Vínculos</h4>

                                            <div class="form-group">
                                                <label class="control-label col-md-5">Confederação:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static">
                                                     CBH - Confederação Brasileira de Hipismo 
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Federação:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="federacao"> </p>
                                                </div>
                                            </div>

                                            <div class="form-group" id="form-group-resume-entidade-filiada">
                                                <label class="control-label col-md-5">Entidade Filiada:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="entidade-filiada"> </p>
                                                </div>
                                            </div>

                                            <div class="form-group" id="form-group-resume-escola-equitacao">
                                                <label class="control-label col-md-5">Escola de Equitação:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="escola-equitacao"> </p>
                                                </div>
                                            </div>


                                        </div>


                                    </div>
                                    <!-- /row -->

                                    <!-- ROW ( Informacoes Gerais ) -->
                                    <div class="row">


                                        <div class="col-sm-6">


                                            <h4 class="form-section">Informações Gerais</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nº Chip:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="chip"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nº Passaporte:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="passaporte"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Data de Nascimento:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="data-nascimento"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nome Completo:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="nome-completo"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nome Patrocinado:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="nome-patrocinado"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Raça:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="raca"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Pelagem:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="pelagem"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Gênero:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="genero"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group" id="form-group-tipo-macho">
                                                <label class="control-label col-md-5">Tipo do Macho:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="genero-tipo"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Peso (kg):</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="peso"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Altura da Cruz (cm):</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="altura-cruz"> </p>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-sm-6">


                                            <h4 class="form-section">Identificação</h4>

                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nº FEI:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="registro-fei"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nº CAPA FEI:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="registro-capa-fei"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nº Registro CBH:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="registro-cbh"> </p>
                                                </div>
                                            </div>
                                            
                                            <h4 class="form-section">Genealogia</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nº Registro Genealógico:</label>
                                                <div class="col-md-7">
                                                    <p class="form-control-static" data-display="registro-genealogico"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nome Associação de Registro:</label>
                                                <div class="col-md-7">  
                                                    <p class="form-control-static" data-display="associacao"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">País de Origem:</label>
                                                <div class="col-md-7">  
                                                    <p class="form-control-static" data-display="pais-origem"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nome do Pai:</label>
                                                <div class="col-md-7">  
                                                    <p class="form-control-static" data-display="nome-pai"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nome da Mãe:</label>
                                                <div class="col-md-7">  
                                                    <p class="form-control-static" data-display="nome-mae"> </p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5">Nome do Avô Materno:</label>
                                                <div class="col-md-7">  
                                                    <p class="form-control-static" data-display="nome-avo-materno"> </p>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <!-- /row -->


                                </div>
                                <!-- /tab-pane -->

                            </div>
                            <!-- /tab-content -->

                        </div>
                        <!-- /form-body -->


                        <!-- 
                        FORM-ACTIONS
                        ================================================ -->
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <a href="javascript:;" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Voltar </a>
                                    <a href="javascript:;" class="btn btn-outline green button-next"> Continuar
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                    <a href="javascript:;" class="btn green button-submit"> Cadastrar
                                        <i class="fa fa-check"></i>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>

