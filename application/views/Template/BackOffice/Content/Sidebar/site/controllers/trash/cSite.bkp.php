<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class cSite extends CI_Controller
{

	/**
	 * Variavel que contem as regras de validacao do formulario 
	 * @var array
	 */

	function __construct()
	{
		parent::__construct();
	    date_default_timezone_set('America/Sao_Paulo');
	
		$this->data['current_page']				=	'';
		$this->load->library('my_date');

		$this->data['current_nav']				=	'';
		$this->data['title_page']				=	'';
		$this->data['use_wizard']				=	'';
		$this->data['use_mask']					=	'';
		$this->data['use_owlslider']			=	'';
		$this->data['use_maps']					=	'';

		/*
			Modalidades são usadas em todo o site:
			- Menu
			- Listagem de modalidades na index
			- Caledario
			- Noticias
			- Galerias
		*/
		$this->data['slugTipoPagina']		=	NULL;
		$this->data['modalidades']				=	$this->m_crud->get_allOrderby('tb_site_modalidade', 'mod_modalidade', 'asc');
		$this->data['categoriasDeNoticias']		=	$this->db->query("SELECT * FROM tb_site_blog_categoria WHERE fk_tip_id=1 AND cat_slug<>'geral'")->result();
		$this->data['categoriasDeGalerias']		=	$this->db->query("SELECT * FROM tb_site_blog_categoria WHERE fk_tip_id=2 AND cat_slug<>'geral'")->result();



	}
	





	/*-----------------------------------------------	
	@ INDEX
	-----------------------------------------------*/
	public function index()
	{
		$this->inicio();
	}


	/*-----------------------------------------------	
	@ INÍCIO
	-----------------------------------------------*/
	public function inicio()
	{
		$this->data['title_page']				=	'AERS - Associação Econômica e Recreativa dos Servidores do SENAC-GO';
		$this->data['current_page']	=	'inicio';
		$this->data['current_nav']	=	'inicio';


/*		$this->data['noticias_slide']	=	$this->m_crud->get_allLimitOrderby('tb_noticia', 5, 'not_id', 'desc')->result();
		$this->data['noticias_lateral']	=	$this->db->query("SELECT * FROM tb_noticia ORDER BY not_id DESC LIMIT 5,5")->result();

		$this->data['depoimentos']	=	$this->m_crud->get_allLimitOrderby('tb_depoimento', 3, 'dep_id', 'random')->result();

		$this->data['agendas']	=	$this->m_crud->get_allLimitOrderby('tb_agenda', 4, 'age_id', 'desc')->result();

		$this->data['banners']	=	$this->m_crud->get_allOrderby('tb_slideshow', 'sli_posicao', 'asc');

		$this->data['baneficios']	=	$this->m_crud->get_allLimitOrderby('tb_beneficio', 4, 'ben_id', 'random')->result();

		$this->data['use_owlslider']			=	TRUE;
*/


		$this->loadtemplate( 'inicio' );

	}


	/*-----------------------------------------------	
	@ A FEDERAÇÃO - HISTORIA
	-----------------------------------------------*/
	public function afederacao($slugPagina)
	{

		$this->data['title_page']						=	'';
		$this->data['current_nav']						=	'afederacao';

		if($slugPagina==null)
		{
			redirect(base_url() . 'afederacao/' . 'historia');
		}

		if(
			$slugPagina != 'historia' &&
			$slugPagina != 'tribunal-de-justica-desportiva' &&
			$slugPagina != 'prestacao-de-contas' &&
			$slugPagina != 'contratacoes-e-licitacoes'
		)
		{
			redirect(base_url() . 'a-federacao/' . 'historia');
		}
		else
		{

/*
			// ENTIDADES FILIADAS
			if($slugPagina=='noticias')
			{
				redirect(base_url());
			}
*/

			$query 								=	$this->m_crud->get_allWhere('tb_site_pagina_categoria', 'cat_slug', $slugPagina);
			$idCategoria						=   $query[0]->cat_id;

			$this->data['tituloPagina']			=	$this->m_crud->get_rowSpecific('tb_site_pagina_categoria', 'cat_slug', $slugPagina, 1, 'cat_titulo');

			$this->data['paginas']				=	$this->db->query('select * from tb_site_pagina_categoria where cat_id in (1,2,3,4)')->result();
			$this->data['slugPagina'] 			= 	$slugPagina;
			$this->data['current_page']			=	'modalidade_' . $slugPagina;

			$this->data['conteudo']				=	$this->m_crud->get_rowSpecific('tb_site_pagina', 'fk_cat_id', $idCategoria, 1, 'pag_conteudo');

			$this->loadtemplate( 'federacao', 'true', 'federacao' );

		}
	}



	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (LISTAGEM) ENTIDADES FILIADAS
	-----------------------------------------------*/
	public function entidades()
	{

		$this->data['title_page']	=	'';
		$this->data['tipoPagina']	=	'Entidades Filiadas';
		$this->data['current_nav']	=	'afederacao';
		$this->data['current_page']	=	'federacao_entidades';

		$this->loadtemplate( 'federacao_listagemEntidadesEscolas', 'true', 'federacao' );
	}


	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (ITEM) ENTIDADE
	-----------------------------------------------*/
	public function entidade()
	{

		$this->data['title_page']			=	'';
		$this->data['tipoPagina']			=	'Entidades Filiadas';
		$this->data['current_nav']			=	'afederacao';
		$this->data['current_page']			=	'federacao_entidades';
		$this->data['nomeEntidadeEscola']	=	'Sociedade Hípica de Brasília';

		$this->loadtemplate( 'federacao_itemEntidadeEscola', 'true', 'federacao' );
	}



	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (ITEM) ENTIDADE
	-----------------------------------------------*/
	public function modalidades()
	{
		$this->data['title_page']			=	'';
		$this->data['current_nav']			=	'modalidades';
		$this->data['current_page']			=	'-';


		$this->loadtemplate( 'modalidades');
	}


	/*-----------------------------------------------	
	@ A FEDERAÇÃO - (ITEM) ENTIDADE
	-----------------------------------------------*/
	public function modalidade($slugModalidade, $slugPagina=null)
	{


		$this->data['title_page']						=	'';
		$this->data['current_nav']						=	'modalidades';

		if($slugPagina==null)
		{
			redirect(base_url() . 'modalidade/' . $slugModalidade . '/historico');
		}

		if(
			$slugModalidade != 'atrelagem' &&
			$slugModalidade != 'adestramento' &&
			$slugModalidade != 'cce' &&
			$slugModalidade != 'enduro' &&
			$slugModalidade != 'paraequestre' &&
			$slugModalidade != 'redeas' &&
			$slugModalidade != 'salto' &&
			$slugModalidade != 'volteio'
		)
		{
			redirect(base_url().'modalidade');
		}
		else
		{

			/*
			- Verificando qual a página da categoria
			*/
			if(
				$slugPagina != 'historico' &&
				$slugPagina != 'noticias' &&
				$slugPagina != 'galeria' &&
				$slugPagina != 'ranking' &&
				$slugPagina != 'regulamento' &&
				$slugPagina != 'taxas' &&
				$slugPagina != 'quadro-juizes' &&
				$slugPagina != 'reprises'
			)
			{
				// Se houver um valor diferente da condição, redireciona para histórico
				redirect(base_url() . 'modalidade/' . $slugModalidade . '/historico');
			}
			else
			{

				// NOTICIAS
				if($slugPagina=='noticias')
				{
					redirect(base_url());
				}

				// GALERIA
				if($slugPagina=='galeria')
				{
					redirect(base_url() );
				}

			}
			
			

			/* Array com todas as paginas que são de alguma modalidade */	
			$this->data['paginas']	=	$this->db->query('select * from tb_site_pagina_categoria where cat_id in (5,6,7,8,9,10)')->result();


			$idModalidade 					= $this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugModalidade, 1, 'mod_id');	
			$tituloModalidade 				= $this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugModalidade, 1, 'mod_modalidade');
			$this->data['slugModalidade'] 	= $slugModalidade;
		
			$idCategoria 					= $this->m_crud->get_rowSpecific('tb_site_pagina_categoria', 'cat_slug', $slugPagina, 1, 'cat_id');	
			$tituloPagina 					= $this->m_crud->get_rowSpecific('tb_site_pagina_categoria', 'cat_slug', $slugPagina, 1, 'cat_titulo');
			$this->data['slugPagina'] 		= $slugPagina;



			$this->data['tituloModalidade']					=	$tituloModalidade;
			$this->data['tituloPagina']						=	$tituloPagina;
		
			$this->data['modalidade_pictograma']			=	$slugModalidade;
			$this->data['current_page']						=	'modalidade_' . $slugPagina;

			$conteudo 	                                    = $this->m_crud->get_rowSpecificTwoParameters('tb_site_pagina', 'fk_mod_id', $idModalidade, 'fk_cat_id', $idCategoria, 1, 'pag_conteudo');
			$this->data['conteudo']							=	$conteudo;

			$this->loadtemplate( 'modalidade', true, 'modalidade');

		}

	}




	/*-----------------------------------------------	
	@ MODALIDADE - HISTÓRICO
	-----------------------------------------------*/
	public function modalidade_historico()
	{

		$this->data['current_page']						=	'modalidade_historico';
		$this->data['current_page_titulo']				=	'Histórico';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}


	/*-----------------------------------------------	
	@ MODALIDADE - RANKING
	-----------------------------------------------*/
	public function modalidade_ranking()
	{

		$this->data['current_page']						=	'modalidade_ranking';
		$this->data['current_page_titulo']				=	'Ranking';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}


	/*-----------------------------------------------	
	@ MODALIDADE - REGULAMENTO
	-----------------------------------------------*/
	public function modalidade_regulamento()
	{
		$this->data['current_page']						=	'modalidade_regulamento';
		$this->data['current_page_titulo']				=	'Regulamento';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}


	/*-----------------------------------------------	
	@ MODALIDADE - TAXAS
	-----------------------------------------------*/
	public function modalidade_taxas()
	{
		$this->data['current_page']						=	'modalidade_taxas';
		$this->data['current_page_titulo']				=	'Taxas';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}


	/*-----------------------------------------------	
	@ MODALIDADE - QUADRO JUÍZES
	-----------------------------------------------*/
	public function modalidade_quadrojuizes()
	{
		$this->data['current_page']						=	'modalidade_quadrojuizes';
		$this->data['current_page_titulo']				=	'Quadro Juízes';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}


	/*-----------------------------------------------	
	@ MODALIDADE - REPRISES
	-----------------------------------------------*/
	public function modalidade_reprises()
	{
		$this->data['current_page']						=	'modalidade_reprises';
		$this->data['current_page_titulo']				=	'Reprises';

		$this->loadtemplate( 'modalidade', true, 'modalidade');
	}






	/*-----------------------------------------------	
	@ CALENDÁRIO
	-----------------------------------------------*/
	public function calendario()
	{

		$this->data['arrEvento']	=	array(

			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),

			array(
				'fevereiro', '20/02 a 22/02', 'Salto', 'CSN', 'SHBr',
			),
		);

		$this->loadtemplate( 'calendario', false);
	}




	/*-----------------------------------------------	
	@ NOTICIAS
	-----------------------------------------------*/
	public function noticias()
	{
		$this->data['title_page']		=	'';

		$this->data['arrNoticias']	=	array(

			/*array(
				'thumb'=> '2.jpg',
				'titulo'=> 'Giovanna Lara de Freitas estreia em nova categoria na temporada 2015',
				'data'=> '25/07/2014',
				'descricao'=> 'A amazona cubatense Giovanna de Lara Freitas estreia na temporada 2015 em uma nova categoria. Depois de conquistar títulos em 2014, pela categoria mirim (12/14 anos), em saltos com obstáculos de 1,25 metros, aos 14 anos, ela passará a competir pela categoria pré-júnior (14/16 anos), para saltos de 1,30 metros. ',
			),
			array(
				'thumb'=> '3.jpg',
				'titulo'=> 'Barbacena será sede da abertura do Campeonato Mineiro de Hipismo',
				'data'=> '25/07/2014',
				'descricao'=> 'A cidade de Barbacena irá sediar neste ano a abertura do Campeonato Estadual de Hipismo. A disputa está agendada para os dias 13, 14 e 15 de março, no Parque de Exposições da cidade. De acordo com o coordenador técnico da Federação Hípica, Carlos Alberto Sá Grise, nos outros dois anos anteriores, a cidade sediou uma das etapas. Porém, pela primeira vez será palco da abertura do evento.',
			),*/
		);
		$this->data['current_page']	=	'noticias';
		$this->loadtemplate( 'noticias', true, 'noticias');
	}


    /************************************************** 
      NOTÍCIA
    ************************************************* */
    public function noticia($idNoticia) {

        $this->data['current_class'] 	= 'noticias';
		$this->data['current_page']	=	'noticias';
		
		$this->data['titulo']	=	'Nos EUA, Rodrigo Pessoa quer evitar hepta feminino na "Guera dos Sexos"';
		$this->data['idNoticia']	=	$idNoticia;
		$this->data['criado']	=	'0000-00-00 00:00:00';
		$this->data['conteudo']	=	'<p>O hipismo &eacute; a &uacute;nica modalidade ol&iacute;mpica em que homens e mulheres disputam a mesma prova, em igualdade de condi&ccedil;&otilde;es. Mas, na primeira semana do Festival de Inverno, que ser&aacute; realizada a partir da noite desta quarta-feira em Wellington, nos Estados Unidos, o duelo ser&aacute; entre os sexos. Um duelo entre homens e mulheres promete incendiar a competi&ccedil;&atilde;o, que contar&aacute; com a participa&ccedil;&atilde;o do brasileiro Rodrigo Pessoa, campe&atilde;o ol&iacute;mpico em 2004.&nbsp;Nos &uacute;ltimos seis anos, as mulheres levaram a melhor contra os homens no evento, que conta com uma premia&ccedil;&atilde;o de US$ 75 mil, cerca de R$ 180 mil.&nbsp;</p> <div class="foto componente-conteudo" id="3ecff07c-e57c-052a-22f6-5ebf639e3c5f" style="font-family: arial, helvetica, freesans, sans-serif; font-size: 12px; margin: 0px 25px 30px 0px; outline: 0px; padding: 0px; -webkit-font-smoothing: antialiased; border-radius: 3px; overflow: hidden; position: relative; zoom: 1; float: left; color: rgb(0, 0, 0); line-height: 12px; width: 690px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">></div> <table border="0" cellpadding="1" cellspacing="1"> <tbody> <tr> <td> <table border="0" cellpadding="1" cellspacing="1" style="line-height:20.7999992370605px"> <tbody> <tr> <td><img alt="Guerra dos Sexos - Hipismo saltos (Foto: Divulgação)" src="http://s2.glbimg.com/53TFQ5qKi-e17zxTqjMgIzWRHSo=/29x0:719x475/690x476/s.glbimg.com/es/ge/f/original/2015/01/07/hipismo_1.jpg" style="-webkit-font-smoothing:antialiased; background:transparent; border:0px; color:rgb(0, 0, 0); display:block; font-family:inherit; font-size:12px; height:476px; line-height:12px; margin:0px; outline:0px; padding:0px; width:690px" title="Guerra dos Sexos - Hipismo saltos (Foto: Divulgação)" /></td> </tr> </tbody> </table> </td> </tr> </tbody> </table> <p>O Festival Equestre de Inverno tem a dura&ccedil;&atilde;o de 12 semanas, com a participa&ccedil;&atilde;o de quase tr&ecirc;s mil cavaleiros de 31 pa&iacute;ses. O principal evento individual da primeira etapa do evento ser&aacute; o Grande Pr&ecirc;mio, no domingo, com a premia&ccedil;&atilde;o de US$ 30 mil, cerca de R$ 80 mil.&nbsp;</p> <div class="foto componente-conteudo" id="d76892da-b775-ff88-1711-5a39c074fd31" style="font-family: arial, helvetica, freesans, sans-serif; font-size: 12px; margin: 0px 25px 30px 0px; outline: 0px; padding: 0px; -webkit-font-smoothing: antialiased; border-radius: 3px; overflow: hidden; position: relative; zoom: 1; float: left; color: rgb(0, 0, 0); line-height: 12px; width: 300px; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"><img alt="Rodrigo Pessoa, Copa das Nações de Hipismo (Foto: Agência EFE)" src="http://s2.glbimg.com/tfJ3_PvPglywfl05YhqZHExi5w4=/0x21:950x759/300x233/s.glbimg.com/es/ge/f/original/2014/10/09/rodrigopessoa-hipismo-efe.jpg" style="-webkit-font-smoothing:antialiased; background:transparent; border:0px; display:block; font-family:inherit; height:233px; margin:0px; outline:0px; padding:0px; width:300px" title="Rodrigo Pessoa, Copa das Nações de Hipismo (Foto: Agência EFE)" /><span style="font-family:inherit"><span style="font-size:10px"><strong>Rodrigo Pessoa &eacute; um dos favoritos ao t&iacute;tulo</strong><br /> (Foto: Ag&ecirc;ncia EFE)</span></span> <hr /></div> <p>Rodrigo Pessoa &eacute; um dos favoritos ao t&iacute;tulo da competi&ccedil;&atilde;o. No ano passado, o campe&atilde;o ol&iacute;mpico venceu cinco provas ao longo da competi&ccedil;&atilde;o na Fl&oacute;rida, incluindo o Grande Pr&ecirc;mio disputado na nona semana do Festival.</p> <p>&Eacute; o primeiro passo de Rodrigo no ano de 2015, importante pois o brasileiro participar&aacute; dos Jogos Pan-Americanos e de algumas das principais competi&ccedil;&otilde;es internacionais. No ano passado, ele ajudou a sele&ccedil;&atilde;o nacional a terminar em quinto lugar os Jogos Equestres.</p>';
		$this->data['comentarios']	=	true;

		$this->data['noticias']	=	array(
			array(
				'not_thumb'=> '3.jpg',
				'not_thumb'=> '3.jpg',
				'not_titulo'=> 'Barbacena será sede da abertura do Campeonato Mineiro de Hipismo',
				'criado'=> '25/07/2014',
				'descricao'=> 'A cidade de Barbacena irá sediar neste ano a abertura do Campeonato Estadual de Hipismo. A disputa está agendada para os dias 13, 14 e 15 de março, no Parque de Exposições da cidade. De acordo com o coordenador técnico da Federação Hípica, Carlos Alberto Sá Grise, nos outros dois anos anteriores, a cidade sediou uma das etapas. Porém, pela primeira vez será palco da abertura do evento.',
			),
		);


		$this->loadtemplate( 'noticia', true, 'noticias');
    }


	/*-----------------------------------------------	
	@ BLOG
	// $slugTipoPagina	= pode ser 'noticias' ou 'galerias'
	// $slugCategoria	= categoria selecionada de alguma pagina
	-----------------------------------------------*/
	public function blog($slugTipoPagina=null, $slugCategoria=null)
	{

		// $idTipoPagina = retorna 1(noticias) ou 2(galerias).
		$idTipoPagina	=	$this->m_crud->get_rowSpecific('tb_site_blog_tipo', 'tip_slug', $slugTipoPagina, 1, 'tip_id');

		$idTipoCategoria	=	$this->m_crud->get_rowSpecificTwoParameters('tb_site_blog_categoria', 'fk_tip_id', $idTipoPagina, 'cat_slug', $slugCategoria, 1, 'cat_categoria');
		if(is_null($idTipoCategoria))
		{
			$idTipoCategoria 	= 	1;	//modalidade
			$idModalidade 		=	$this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugCategoria, 1, 'mod_id');
			var_dump($idModalidade);
			$query	=	array();
		}
		else
		{
			$idTipoCategoria 	= 	2;	//categoria
			#$idModalidade 		=	$this->m_crud->get_rowSpecificTwoParameters('tb_site_blog_categoria', 'cat_slug', $slugTipoPagina, 'fk_tip_id',  $idTipoPagina, 1, 'cat_id');
		}

		/*
			$idTipoCategoria é uma variavel que servirá para indicar em qual tabela a
			query deverá ser feita, se será na categoria ou na modalidade
		*/






	/*	if(is_null($tituloCategoria))
		{
			$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugCategoria, 1, 'mod_modalidade');
		}
		$this->data['tituloCategoria']		=	$tituloCategoria*/;







		// identificar se a categoria é da 



		// Tratamento de caso quando a categoria for nula
		if(empty($slugCategoria)) 
		{
			redirect( base_url() . $slugTipoPagina . '/geral');
		}

		/* ARRAY DAS CATEGORIAS */
		$categoriasSidebar						=	$this->db->query("SELECT * FROM tb_site_blog_categoria WHERE fk_tip_id=$idTipoPagina AND cat_slug<>'geral'")->result();
		$this->data['categoriasSidebar']		=	$categoriasSidebar;



		// Tratamento caso quando slugcategoria não existir
		//
		//
		//

		// NOTICIAS
		if($slugTipoPagina=='noticias')
		{	

			$tipoPagina			=	'Notícias';
			/*$arrData			=	$this->db->query("
									SELECT * FROM tb_site_blog 
									WHERE fk_tip_id=$idTipoPagina AND
										  fk_cat_id==$idCategoria
									")->result();*/

			$arrData 		=	array();
		}


		// GALERIAS
		if($slugTipoPagina=='galerias')
		{	

			$tipoPagina			=	'Galerias';
			$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_site_blog_categoria', 'cat_slug', $slugCategoria, 1, 'cat_categoria');
			$arrData			=	array();
		}



		// PROCURA EM CATEGORIAS, SE NÃO ACHAR PROCURA EM MODALIDADES
		$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_site_blog_categoria', 'cat_slug', $slugCategoria, 1, 'cat_categoria');
		if(is_null($tituloCategoria))
		{
			$tituloCategoria	=	$this->m_crud->get_rowSpecific('tb_site_modalidade', 'mod_slug', $slugCategoria, 1, 'mod_modalidade');
		}
		$this->data['tituloCategoria']		=	$tituloCategoria;


		// SLUGS
		$this->data['slugTipoPagina']		=	$slugTipoPagina;
		$this->data['slugCategoria']		=	$slugCategoria;


		// DADOS
		$this->data['arrData']				=	$arrData;
		$this->data['tipoPagina']			=	$tipoPagina;
	

		// CARREGA VIEW		
		$this->loadtemplate( 'blog', true, 'blog');
	}



	/*-----------------------------------------------	
	@ CONTATO
	-----------------------------------------------*/
	public function contato()
	{
		$this->data['title_page']		=	'Contato | AERS - Associação Econômica e Recreativa dos Servidores do SENAC-GO';
		$this->data['current_page']	=	'contato';
		$this->loadtemplate( 'contato' );

	}






	/**
	 * Mostra a estrutura do template 
	 * @param string $pathView
	 */
	protected function loadTemplate( $pathView, $sidebar=false, $sidebar_type=false )
	{
		$this->load->view('header', $this->data);
		$this->load->view( $pathView );

		if($sidebar==true)
		{
			$sidebar_type = 'sidebar_' . $sidebar_type;
			$this->load->view($sidebar_type);
		}
		
		$this->load->view('footer');
	}

}
/* END CLASS */

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */



