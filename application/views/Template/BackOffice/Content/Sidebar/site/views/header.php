<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
 <html lang="pt-br">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
	<meta charset="utf-8">
	<title>FHBr - Federação Hípica de Brasília</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta content="" name="description">
	<meta content="" name="keywords">
	<meta content="Federação Hípica de Brasília" name="author">

	<meta property="og:site_name" content="Federação Hípica de Brasília">
	<meta property="og:title" content="Federação Hípica de Brasília">
	<meta property="og:description" content="-">
	<meta property="og:type" content="website">
	<meta property="og:image" content="-"><!-- link to image for socio -->
	<meta property="og:url" content="<?php echo base_url(); ?>">

	<link rel="shortcut icon" href="<?php echo base_url()?>assets/pages/img/logo-fav.png">

	<!-- Fonts START -->
	<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>

	<!-- Global styles START -->          
	<link href="<?php echo base_url(); ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Page level plugin styles START -->
	<link href="<?php echo base_url(); ?>assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet">

	<!-- Plugin Owl START -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/layout/plugins/owlslider/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/layout/plugins/owlslider/owl-carousel/owl.theme.css">


  <!-- Style - Datatables
  ============================================= -->
  <link rel="stylesheet" type="text/css" href="{base_url}assets/sigepe/plugins/DataTables-1.10.8/media/css/jquery.dataTables.css">
    

	<!-- Plugin Owl START -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/override/css/vanguarda.css" >

	<!-- Theme styles START -->
	<link href="<?php echo base_url(); ?>assets/global/css/components.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/frontend/layout/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/frontend/layout/css/style-responsive.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/frontend/layout/css/themes/blue.css" rel="stylesheet" id="style-color">
	<link href="<?php echo base_url(); ?>assets/frontend/layout/css/custom.css" rel="stylesheet">

  <!-- Gallery -->
  <link href="<?php echo base_url(); ?>assets/frontend/pages/css/gallery.css" rel="stylesheet">


</head>
<!-- Head END -->


<!-- Body BEGIN -->
<body class="corporate">
    
    <!-- GOOGLE PLUS -->  
    <script src="https://apis.google.com/js/platform.js" async defer>
      {lang: 'pt-BR'}
    </script>

    <!-- FACEBOOK -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- TWITTER -->
    <script>
    window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
    </script>
    

    <div id="sb-site">


    <!-- BEGIN HEADER -->
    <div class="header_top logo_advertise">
      <div class="container" style="padding:0;">

        <div class="logo col-sm-4">
          <a class="site-logo" href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url(); ?>assets/frontend/layout/img/logos/logo-header.png" alt="" class="img-responsive" />
          </a>
        </div>

        <div class="col-sm-8 header_advertising">
          <?php foreach ($banners as $banner): ?>
          
            <?php if(!is_null($banner->ban_link)): ?>    
            <a href="<?php echo $banner->ban_link; ?>" title="<?php echo $banner->ban_titulo; ?>" target="_blank">
            <?php endif; ?>

              <img src="<?php echo base_url() . 'manager/uploads/advertising/' . $banner->ban_thumb; ?>" alt="<?php echo $banner->ban_titulo; ?>" class="img-responsive" />
            
            <?php if(!is_null($banner->ban_link)): ?>    
            </a>
            <?php endif; ?>
        
          <?php endforeach; ?>
        </div>

      </div>
    </div>
    <!-- Header END -->


    <div class="header" style="background:#F0F0E1;">


      <div class="hidden-md hidden-lg">
        <div class="container">
          <a href="javascript:void(0);" class="mobi-toggler">
          <div class="sb-togsgle-left" style="padding: 0 30px; cursor:pointer;font-size: 22px; font-weight: 400;">
            <span>MENU</span>
          </div>
          </a>
        </div>
      </div>


      <div class="container no-padding menu_principal">
        <!-- BEGIN NAVIGATION -->
        <div class="header-navigation pull-right font-transform-inherit">
          <ul>

            <!-- INÍCIO -->
            <li class="<?php echo ($current_nav=='inicio') ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>" title="">INÍCIO</a></li>

            <!-- PORTAL FHBr
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                PORTAL FHBr   <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu">
                <li class="dropdown-submenu">
                  <a href="index.html">Cadastro <i class="fa fa-angle-right"></i></a>
                  <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-submenu">
                      <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                        Pessoa Física 
                        <i class="fa fa-angle-right"></i>
                      </a>
                      <ul class="dropdown-menu">
                        <li><a href="index.html">Atleta</a></li>
                        <li><a href="index.html">Proprietário de Animal</a></li>
                        <li><a href="index.html">Veterinário</a></li>
                        <li><a href="index.html">Oficial FHBr</a></li>
                        <li><a href="index.html">Instrutor</a></li>
                      </ul>
                    </li>
                    <li class="dropdown-submenu">
                      <a href="index.html">Pessoa Juridica</a>
                    </li>
                  </ul>
                </li>
                <li class="dropdown-submenu">
                  <a href="index.html">Login </a>
                </li>                
              </ul>
            </li>
             -->

            <!-- A FEDERAÇÃO -->
            <li class="dropdown dropdown-megamenu <?php echo ($current_nav=='afederacao') ? 'active' : ''; ?>">
              <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                A FEDERAÇÃO  <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <div class="header-navigation-content">
                    <div class="row">
                      <div class="col-md-6 header-navigation-col">
                        <h4>A Federação</h4>
                        <ul>
                          <li <?php echo ($current_page=='federacao_historia') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/historia'; ?>">História</a></li>
                          <li <?php echo ($current_page=='federacao_entidades') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/entidades-filiadas'; ?>">Entidades Filiadas</a></li>
                          <li <?php echo ($current_page=='federacao_escolas') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/escolas-de-equitacao'; ?>">Escolas de Equitação</a></li>
                          <li <?php echo ($current_page=='federacao_tribunal') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/tribunal-de-justica-desportiva'; ?>">Tribunal de Justiça Desportiva</a></li>
                        </ul>
                      </div>
                      <div class="col-md-6 header-navigation-col">
                        <h4>Portal da Transparência</h4>
                        <ul>
                          <li><a href="<?php echo base_url() . 'a-federacao/estatuto' ?>">Estatuto</a></li>
                          <li <?php echo ($current_page=='federacao_prestacao') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/prestacao-de-contas'; ?>">Prestação de Contas</a></li>
                          <li <?php echo ($current_page=='federacao_contratacoes') ? 'class="active"' : ''; ?>><a href="<?php echo base_url() . 'a-federacao/contratacoes-e-licitacoes'; ?>">Contratações e Licitações</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </li>

            <!-- MODALIDADES -->
            <li class="dropdown <?php echo ($current_nav=='modalidades') ? 'active' : ''; ?>">
              <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                MODALIDADES   <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu">
                <?php
                  foreach ($modalidades as $modalidade):
                    $url      = base_url() . 'modalidade/' . $modalidade->mod_slug . '/'; 
                    $noticias = base_url() . 'noticias/' . $modalidade->mod_slug . '/'; 
                    $galerias = base_url() . 'galerias/' . $modalidade->mod_slug . '/'; 
                ?>


                  <li class="dropdown-submenu">
                    <a href="#" title="title="<?php echo $modalidade->mod_modalidade; ?>""><?php echo $modalidade->mod_modalidade; ?> <i class="fa fa-angle-right"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo $url; ?>historico">Histórico</a></li>
                        <li><a href="<?php echo $noticias; ?>">Notícias</a></li>
                        <li><a href="<?php echo $galerias; ?>">Galerias</a></li>
                        <li><a href="<?php echo $url . 'ranking'; ?>">Ranking</a></li>
                        <li><a href="<?php echo $url . 'regulamento'; ?>">Regulamento</a></li>
                        <li><a href="<?php echo $url . 'taxas'; ?>">Taxas</a></li>
                        <li><a href="<?php echo $url . 'quadro-juizes'; ?>">Quadro Juízes</a></li>
                        <li><a href="<?php echo $url . 'cursos'; ?>">Cursos</a></li>
                      </ul>
                  </li>


                <?php endforeach; ?>
              </ul>
            </li>

            <!-- CALENDÁRIO -->
            <li class="dropdown <?php echo ($current_nav=='calendario') ? 'active' : ''; ?>">
              <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                CALENDÁRIO  <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url() . 'calendario'; ?>">Calendário FHBr</a></li>
                <li><a href="http://www.cbh.org.br/calendarios.html" title="Calendário CBH" target="_blank">Calendário CBH</a></li>
                <li><a href="https://data.fei.org/Calendar/Search.aspx" title="Calendário FEI" target="_blank">Calendário FEI</a></li>
              </ul>
            </li>

            <!-- NOTICIAS -->
            <li class="dropdown <?php echo ($slugTipoPagina=='noticias') ? 'active' : ''; ?>">
              <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                NOTÍCIAS  <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu">
                
                <li class="<?php echo ( $slugCategoria == 'geral' && $slugTipoPagina=='noticias') ? 'active' : ''; ?>">
                  <a href="<?php echo base_url() . 'noticias'; ?>" title="Geral">
                    Geral
                  </a>
                </li>
                
                <?php foreach ($modalidades as $modalidade): ?>
                  <li class="<?php echo ( $slugTipoPagina=='noticias' && $slugCategoria == $modalidade->mod_slug) ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'noticias/' . $modalidade->mod_slug; ?>" title="<?php echo $modalidade->mod_modalidade; ?>">
                      <?php echo $modalidade->mod_modalidade; ?>
                    </a>
                  </li>
                <?php endforeach; ?>

                <?php foreach ($categoriasDeNoticias as $categoriaDeNoticias): ?>
                  <li class="<?php echo ( $slugTipoPagina=='noticias' && $slugCategoria == $categoriaDeNoticias->cat_slug) ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'noticias/' . $categoriaDeNoticias->cat_slug; ?>" title="<?php echo $categoriaDeNoticias->cat_categoria; ?>">
                      <?php echo $categoriaDeNoticias->cat_categoria; ?>
                    </a>
                  </li>
                <?php endforeach; ?>

              </ul>
            </li>


            <!-- GALERIAS -->
            <li class="dropdown <?php echo ($slugTipoPagina=='galerias') ? 'active' : ''; ?>">
              <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">
                GALERIAS  <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu">
                
                <li class="<?php echo ( $slugCategoria == 'geral' && $slugTipoPagina=='galerias') ? 'active' : ''; ?>">
                  <a href="<?php echo base_url() . 'galerias'; ?>" title="Geral">
                    Geral
                  </a>
                </li>

                <?php foreach ($modalidades as $modalidade): ?>
                  <li class="<?php echo ( $slugTipoPagina=='galerias' && $slugCategoria == $modalidade->mod_slug) ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'galerias/' . $modalidade->mod_slug; ?>" title="<?php echo $modalidade->mod_modalidade; ?>">
                      <?php echo $modalidade->mod_modalidade; ?>
                    </a>
                  </li>
                <?php endforeach; ?>

                <?php foreach ($categoriasDeGalerias as $categoriaDeGalerias): ?>
                  <li class="<?php echo ( $slugTipoPagina=='galerias' && $slugCategoria == $categoriaDeGalerias->cat_slug) ? 'active' : ''; ?>">
                    <a href="<?php echo base_url() . 'galerias/' . $categoriaDeGalerias->cat_slug; ?>" title="<?php echo $categoriaDeGalerias->cat_categoria; ?>">
                      <?php echo $categoriaDeGalerias->cat_categoria; ?>
                    </a>
                  </li>
                <?php endforeach; ?>

              </ul>
            </li>


            <!-- SIGEPE -->
            <li class="dropdown <?php echo ($current_nav=='calendario') ? 'active' : ''; ?>">
              <a href="http://fhbr.com.br/sigepe" target="_blank">
                PORTAL FHBr 
              </a>
<!--               <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>">Cadastro de Responsável</a></li>
                <li><a href="<?php echo base_url(); ?>">Cadastro de Proprietário</a></li>
                <li><a href="<?php echo base_url() . 'sigepe/register/RegisterAthlete'; ?>" target="_blank">Registro de Atleta</a></li>
                <li><a href="<?php echo base_url() . 'sigepe/register/RegisterAnimal'; ?>" target="_blank">Registro de Animal</a></li>
                <li><a href="<?php echo base_url() . 'sigepe/login'; ?>" target="_blank">Login</a></li>
              </ul>
 -->            </li>

            <!-- CONTATO -->
            <li><a href="<?php echo base_url() . 'contato'; ?>">CONTATO</a></li>

          </ul>
        </div>
        <!-- END NAVIGATION -->
      </div>
    </div>


    <!-- Begin Main -->
    <div class="main">

      <!-- Begin Container -->
      <div class="container no-padding">
