<div class="col-sm-9 padding-left-0 <?php echo $slugModalidade; ?>" id="pagina-modalidade">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
     	<li><a href="<?php echo base_url() . 'modalidades'; ?>">Modalidades</a></li>
     	<li><a href="<?php echo base_url() . 'modalidade/' . $slugModalidade; ?>"><?php echo $tituloModalidade; ?></a></li>
	    <li class="active"><?php echo $tituloPagina; ?></li>
	</ul>	

	
	<div class="conteudo">

	    <div class="titulo">
	      <div class="pictograma"></div>
	      <div class="pagina"><?php echo $tituloPagina; ?></div>
	      <div class="modalidade"><?php echo $tituloModalidade; ?></div>
	    </div>    

	    <div class="content" style="font-size: 15px; color: #666; line-height: 24px;">
	    	<?php echo $conteudo; ?>
	    </div>

	</div>

</div>