
<!-- BEGIN Sidebar -->
<div class="col-sm-3 federacao_sidebar visible-md visible-lg">

	<h2 class="no-top-space">Categorias</h2>
	<ul class="nav sidebar-categories margin-bottom-40">

		<!-- Exibir modalidades e as categorias -->
        <li class="<?php echo ( $slugCategoria == NULL) ? 'active' : ''; ?>">
			<a href="<?php echo base_url() . $slugTipoPagina; ?>" title="Geral">
				Geral
			</a>
		</li>

		<?php foreach ($modalidades as $modalidade): ?>
          <li class="<?php echo ( $slugCategoria == $modalidade->mod_slug) ? 'active' : ''; ?>">
				<a href="<?php echo base_url() . $slugTipoPagina . '/' . $modalidade->mod_slug; ?>" title="<?php echo $modalidade->mod_modalidade; ?>">
					<?php echo $modalidade->mod_modalidade; ?>
				</a>
			</li>
		<?php endforeach; ?>

        <?php foreach ($categoriasSidebar as $categoriaSidebar): ?>
          <li class="<?php echo ( $slugCategoria == $categoriaSidebar->cat_slug) ? 'active' : ''; ?>">
            <a href="<?php echo base_url() . $slugTipoPagina . '/' . $categoriaSidebar->cat_slug; ?>" title="<?php echo $categoriaSidebar->cat_categoria; ?>">
              <?php echo $categoriaSidebar->cat_categoria; ?>
            </a>
          </li>
        <?php endforeach; ?>

	</ul>
	<!-- CATEGORIES END -->

</div>
<!-- END Sidebar -->

