    <section id="content" class="container-fluid no-padding pagina-aassociacao">
        <div class="container">
              
            <!-- BREADCRUMBS -->
            <ul class="breadcrumb">
                <li><a href="<?php echo base_url() . 'inicio'; ?>">Site</a></li>
                <li class="active">A Associação</li>
            </ul>

            <div class="row">

                <div class="col-sm-6 instititucional">
                    <?php echo $institucional; ?>
                </div>
                <div class="col-sm-6">
                    <p class="box-aers"><?php echo $origem; ?></p>
                </div>

            </div> <!-- /.row -->
            <div class="row">
                <h4 class="red-title">PRESIDENTES ANTERIORES</h4>
                <h5 class="sub-title"><?php echo $lema; ?> <br /> <?php echo $gestao; ?></h5>

                <div class="col-sm-6 no-padding">
                    <table class="tabela" cellspacing='0'>
                        <tr><th>Cargo</th><th>Nome</th></tr><!-- Table Header -->

                        <?php $contador_esquerdo = 1; ?>
                        <?php foreach ($coluna_esquerda as $esquerda): ?>

                        <tr class="<?php echo ($contador_esquerdo % 2 == 0) ? 'even' : ''; ?>"><td><?php echo $esquerda->car_cargo; ?></td><td><?php echo $esquerda->car_nome; ?></td></tr>

                        <?php $contador_esquerdo++; ?>
                        <?php endforeach; ?>

                    </table>
                </div>
                
                <div class="col-sm-6 no-padding">
                    <table class="tabela" cellspacing='0'>
                        <tr><th>Cargo</th><th>Nome</th></tr><!-- Table Header -->
                        

                        <?php $contador_direita = 1; ?>
                        <?php foreach ($coluna_direita as $direita): ?>

                        <tr class="<?php echo ($contador_direita % 2 == 0) ? 'even' : ''; ?>"><td><?php echo $direita->car_cargo; ?></td><td><?php echo $direita->car_nome; ?></td></tr>

                        <?php $contador_direita++; ?>
                        <?php endforeach; ?>


                    </table>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.conteiner -->
    </section> <!-- /#content -->

</section> <!-- /#aassociacao -->
