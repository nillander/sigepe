
  <!-- BEGIN LEFT SIDEBAR -->            
  <div class="col-md-9 col-sm-9 blog-item" id="page_noticia">

    <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
        <li><a href="<?php echo base_url(); ?>">Noticias</i></a></li>
        <li><a href="<?php echo base_url(); ?>">Adestramento</i></a></li>
        <li class="active">Nos EUA, Rodrigo Pessoa quer evitar hepta feminino na "Guera dos Sexos"</li>
    </ul> 


    <!-- BEGIN Cabecalho -->
    <div class="cabecalho">

      <h1 class="titulo"><?php echo $titulo; ?></h1>
      
      <div class="cabecalho_info"><b>Publicado em: </b> 07/01/2015 12h43 - <b>Atualizado em:</b> 08/01/2015 19h04</div>

      <div class="row">
        <div class="col-sm-12 redessociais">
          <div id="ferramentas">
            <div id="social">
              <div class="item">
                <div class="fb-like" data-href="<?php echo base_url() . 'noticia/' . $idNoticia; ?>" data-width="150" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"> </div>
              </div>
              <div class="item">
                <a href="<?php echo base_url() . 'noticia/' . $idNoticia; ?>" class="twitter-share-button" data-lang="en">Tweet</a>              
              </div>
              <div class="item">
                <div class="g-plusone" data-size="medium"></div>
              </div>
              <div class="item">
                <div class="addthis_toolbox addthis_default_style "> <a href="http://www.addthis.com/bookmark.php" class="addthis_button" style="text-decoration:none;"> + compartilhar</a> </div>              <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-4efa1b0c111a32e5"></script>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- END Cabecalho -->  

    <!-- BEGIN Conteúdo -->
    <div class="both">
      <div class="conteudo"><?php echo $conteudo; ?></div>
    </div>
    <!-- END Conteúdo -->

    <!-- BEGIN Info -->
    <ul class="blog-info">
      <li><i class="fa fa-user"></i> By admin</li>
      <li><i class="fa fa-calendar"></i> 25/07/2013</li>
      <li><i class="fa fa-calendar"></i> 25/07/2013</li>
    </ul>
    <!-- END Info -->


    <!-- SEPARADOR -->
    <div class="gridline"></div>

    <?php if($comentarios==1): ?>

    <!-- COMENTÁRIOS -->
    <div class="comentarios">

      <div class="title_template margin-bottom-20">COMENTÁRIOS DA <b>NOTÍCIA</b></div>

      <div class="box">
          <div class="fb-comments" data-href="http://www.aers.org.br/?pub=70" data-width="100%" data-numposts="5" data-colorscheme="light"></div>  
      </div>

    </div>
    <?php endif; ?>

    
    <!-- BEGIN Relacionadas -->
    <div class="relacionadas">

      <div class="row">
        <div class="col-sm-12">
          <div class="title_template margin-bottom-20">NOTÍCIAS <b>RELACIONADAS</b></div>
        </div>
      </div>

      <div class="box">
        <?php foreach ($noticias as $noticia): ?>
        <div class="col-sm-4">
        </div>
        <?php endforeach; ?>
      </div>

    </div>
    <!-- END Relacionadas -->

  </div>
  <!-- END LEFT SIDEBAR -->            
