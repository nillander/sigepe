<div class="container-fluid" id="pagina-agendainterna">      

  <div class="container">
      
    <!-- BREADCRUMBS -->
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Site</a></li>
        <li><a href="<?php echo base_url() . 'agenda'; ?>">Agenda</a></li>
        <li class="active"><?php echo $titulo; ?></li>
    </ul>

    <!-- HEADER DA AGENDA -->
    <div id="pagina-agendainterna-header">

      <div class="col-sm-7">
        <h1><?php echo $titulo; ?></h1>
      </div>

      <!-- INÍCIO: REDESSOCIAIS -->
      <div class="col-sm-5 redessociais">

        <div id="ferramentas">
          
          <div id="social">
              
              <!-- FACEBOOK -->
              <div 
                  class="fb-like" 
                  data-href="http://www.aers.org.br/?pub=70"
                  data-width="150" 
                  data-layout="button_count" 
                  data-action="like" 
                  data-show-faces="true"
                  data-share="false">
              </div>
              
              <!-- TWITTER -->
              <a 
              href="http://www.go.senac.br/portal/noticia/8404-servidores-do-senac-aparecida-de-goiania-arrecadam-e-doam-produtos" 
              class="twitter-share-button" 
              data-lang="en">Tweet</a>
              
              <!-- GOOGLE PLUS -->
              <div class="g-plusone" data-size="medium"></div>
              
              <!-- ADDTHIS -->
              <div class="addthis_toolbox addthis_default_style ">
                 <a href="http://www.addthis.com/bookmark.php" class="addthis_button"  style="text-decoration:none;"> + compartilhar</a> 
              </div>

              <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-4efa1b0c111a32e5"></script>
              
          </div>
          
        </div>

      </div>
      <!-- FIM: REDESSOCIAIS -->

      <div class="col-sm-12 info">
          <span> <b><?php echo $dia; ?> <?php echo $mes; ?>/<?php echo $ano ?></b> - 
            <?php
            $datahoje = date('Y-m-d');
            if($data==$datahoje )
            {
              echo "Esse evento acontece hoje!";
              echo date("F m, Y: h:iA e");
            }
            if($data < $datahoje )
            {
              echo "Evento já aconteceu!";
            }
            if($data > $datahoje )
            {

              $datetime1 = strtotime(date('Y-m-d'));
              $datetime2 = strtotime($data);

              $secs = $datetime2 - $datetime1;// == <seconds between the two times>
              $days = round($secs / 86400);

              if($days==1)
              {
                echo "Esse evento acontece amanhã!";
              }
              else
              {
                echo "Faltam " . $days . " dias para este evento acontecer!";                
              }

            }

            ?>
          </span> <br />
          <b>Local:</b> <?php echo $local; ?> - <a href="#ancora-localizacao" class="locationpage" title="">confira localização no mapa</a>
      </div>

    </div>
    <!-- FIM: HEADER -->  


    <!-- CONTEÚDO -->
    <div class="conteudo">

      <img src="<?php echo base_url(); ?>assets/images/agenda/4.jpg" class="foto-principal" />

      <?php echo $conteudo; ?>

    </div>
    <!-- FIM CONTEÚDO -->

    <div class="espacamento">
      <hr class="separador" />
    </div>


    <!-- COMENTÁRIOS -->
    <?php if($comentarios==1): ?>
    <div class="comentarios">

      <div class="col-sm-1"><img src="<?php echo base_url(); ?>assets/images/agenda/chat.png" /></div>

      <div class="col-sm-11 red-title">Comentários</div>
      
      <div class="box">
        <div class="col-sm-1"></div>     
        <div class="col-sm-11">
          <div class="fb-comments" data-href="http://www.aers.org.br/?pub=70" data-width="1035" data-numposts="5" data-colorscheme="light"></div>  
        </div>
      </div>

    </div>
    <?php endif; ?>

    <!-- MAPA -->
      <div class="localizacao col-sm-12" id="ancora-localizacao" style="padding-top:25px;">
        <div class="col-sm-1"><img src="<?php echo base_url(); ?>assets/images/agenda/localizacao.png" /></div>
        <div class="col-sm-11 red-title">Localização <span><?php echo $local; ?> - <?php echo $endereco; ?></span></div>
      </div>

  </div>
  <!-- FIM CONTAINER -->

</div>
<!-- FIM CONTAINER-FLUID -->



<div class="template-mapa">

    <div id="map-canvas"></div>

</div>