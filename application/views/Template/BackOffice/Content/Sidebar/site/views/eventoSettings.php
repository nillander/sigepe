<?php
	
	// Triggers - Botões Cabecalho
	$realizarInscricao 		= FALSE;
	$programa 				= FALSE;
	$adendo 				= FALSE;
	$ordensDeEntrada 		= FALSE;
	$quadroHorario  		= TRUE;
	$resultados 			= TRUE;
	$provas 				= FALSE;
	$comprovanteInscricao 	= FALSE;
	$segundaViaBoleto 		= FALSE;




	$status_titulo 		   = $this->m_crud->get_rowSpecific('tb_site_evento_status', 'sta_id', $evento[0]->fk_sta_id, 1, 'sta_titulo');
	$status_botao 		   = $this->m_crud->get_rowSpecific('tb_site_evento_status', 'sta_id', $evento[0]->fk_sta_id, 1, 'sta_botao');

	/*---
	Tratamento de Casos para Status do Evento
	---*/
	$status = $evento[0]->fk_sta_id;
	switch ($status) {


		//Inscricoes em Breve
		case 1:
			$status_bootstrap  = 'btn-primary';
			break;

		//Inscricoes Abertas
		case 2:
			$status_bootstrap  	= 'btn-success';
			$provas 			= TRUE;
			break;

		//Inscricoes Encerradas
		case 3:
			$status_bootstrap  	= 'btn-danger';
			$ordensdeentrada   	= TRUE;
			$provas 			= TRUE;
			break;

		//Evento Acontecendo
		case 4:
			$status_bootstrap  	= 'btn-primary';
			$provas 			= TRUE;
			break;

		//Evento já Aconteceu
		case 5:
			$status_bootstrap  	= 'btn-warning';
			$provas 			= TRUE;
			break;

		//Evento Cancelado
		case 6:
			$status_bootstrap  = 'btn-default';
			break;

		//Default
		default:
			$status_bootstrap  = 'btn-default';
			break;

	}

	/* ORDEM DE ENTRADA */	
	/*if($evento[0]->fk_sta_id==3 || $evento[0]->fk_sta_id==4 || $evento[0]->fk_sta_id==5){
		$ordensdeentrada = TRUE;
		$arr_ordensdeentrada 	=	$this->m_crud->get_allWhere('tb_site_evento_ordensdeentrada', 'fk_eve_id', $evento[0]->eve_id);
	}*/


	/* RESULTADOS */
	$eve_resultado = $this->m_crud->get_rowSpecific('tb_site_evento', 'eve_id', $evento[0]->eve_id, 1, 'eve_resultado');
	if(is_null($eve_resultado)){
		$eve_resultado = 0;
	}else{
		$eve_resultado = 1;
	}

	/* QUADRO HORARIOS */
	$eve_quadrohorarios = $evento[0]->eve_quadrohorarios;
	if(is_null($eve_quadrohorarios)){
		$eve_quadrohorarios = 0;
	}else{
		$eve_quadrohorarios = 1;
	}



	/* INSCRICAO */
	if(!empty($evento[0]->eve_inscricao) && $evento[0]->fk_sta_id==2){
		$realizarInscricao  = TRUE;
	}

	/* PROGRAMA */
	if(!empty($evento[0]->eve_programa)){
		$programa 			= TRUE;
	}

	/* ADENDO */
	if(!empty($evento[0]->eve_adendo)){
		$adendo 			= TRUE;
	}

?>

<!-- BEGIN Evento -->
<div class="col-sm-12 padding-left-0 page_evento">
	

	<!-- Breadcrumb
	============================================= -->
  	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'calendario'; ?>">Calendário</a></li>
	    <li class="active"><?php echo strip_tags($evento[0]->eve_evento); ?></li>
	</ul>	

	<h1><?php echo $evento[0]->eve_evento; ?></h1>

	<?php if($global_map==true): ?>
	<div class="row infoheader">
		<div class="col-sm-12 local"><b>Local:</b> <?php echo strip_tags($global_map_content); ?></div>
		<div class="col-sm-12 vernomapa"><a href="#ancora-localizacao" class="locationpage" title=""><i class="fa fa-map-marker"></i> Confira localização no Mapa</a></div>
	</div>	
	<?php endif; ?>

	<div class="gridline"></div>

	<div class="row float-left">

		<div class="col-sm-12">

			<!-- REALIZAR INSCRIÇÃO -->
			<?php if($realizarInscricao): ?>
			<a href="<?php echo $evento[0]->eve_inscricao ?>" class="btn btn-success" target="_blank">
				Realizar Inscrição
			</a>
			<?php endif; ?>

			<!-- PROGRAMA -->
			<?php if($programa): ?>
			<a href="<?php echo base_url() . 'uploads/evento/programa/' . $evento[0]->eve_programa; ?>" class="btn btn-default" target="_blank">
				Programa
			</a>
			<?php endif; ?>

			<!-- ADENDO -->
			<?php if($adendo): ?>
			<a href="<?php echo base_url() . 'uploads/evento/adendo/' . $evento[0]->eve_adendo; ?>" target="_blank" class="btn btn-primary" target="_blank">
				Adendo
			</a>
			<?php endif; ?>

			<!-- ORDENS DE ENTRADA -->
			<?php if($ordensDeEntrada): ?>
			<a href="#ordensresultados" class="btn btn-info locationpage">
				Ordens de Entrada
			</a>
			<?php endif; ?>

			<!-- QUADRO DE HORARIOS -->
			<?php if($eve_quadrohorarios): ?>
			<a href="<?php echo base_url() . 'uploads/evento/quadrodehorario/' . $evento[0]->eve_quadrohorarios; ?>" class="btn btn-primary" target="_blank">
				Quadro de Horários
			</a>
			<?php endif; ?>

			<!-- RESULTADOS -->
			<?php if($resultados): ?>

			<?php if($eve_resultado==0): ?>
			<a href="#provas" class="btn btn-warning locationpage">
				Resultados
			</a>
			<?php endif; ?>
			
			<?php if($eve_resultado==1): ?>
			<a href="<?php echo base_url() . 'uploads/evento/resultado/' . $evento[0]->eve_resultado; ?>" class="btn btn-warning" target="_blank">
				Resultados
			</a>
			<?php endif; ?>
			
			<?php endif; ?>

			<!-- PROVAS -->
			<?php if($provas): ?>
			<a href="#provas" class="btn btn-info locationpage">
				Provas
			</a>
			<?php endif; ?>

			<!-- COMPROVANTE DE INSCRIÇÃO -->
			<?php if($comprovanteInscricao): ?>
			<a href="#" class="btn btn-primary">
				Comprovante de Inscrição
			</a>
			<?php endif; ?>

			<!-- 2º VIA DE BOLETO -->
			<?php if($comprovanteInscricao): ?>
			<a href="#" class="btn btn-primary">
				2º Via de Boleto
			</a>
			<?php endif; ?>

		</div>

	</div>

	<div class="gridline"></div>


	<div class="row informacoesgerais margin-bottom-30 float-left">

		<div class="col-sm-12">
		    <div class="title_template">INFORMAÇÕES <b>GERAIS</b></div>
		    <div class="logo col-sm-3"><div class="box"><img src="<?php echo base_url() . 'uploads/evento/' . $evento[0]->eve_logo; ?>" class="img-responsive" /></div></div>
		    <div class="descricao col-sm-9">
		    	<?php 
		    		if(is_null($evento[0]->eve_descricao)){
						echo "Nenhuma descrição para este evento";		    			
		    		}else{
		    			echo $evento[0]->eve_descricao;
		    		}
		    	?>
		    </div>
		</div>

		<div class="col-sm-12">
			
			<div class="col-sm-<?php echo ($counter==true) ? '5' : '12'; ?> no-padding detalhes">
				<div class="row">

					<!-- PERÍODO -->
					<?php if($periodo): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Período:</div>
							<div class="col-sm-6 value">
				        		<?php echo $this->my_date->datetime($evento[0]->eve_datainicio, 'justDate'); ?>
		        				<?php if(!empty($evento[0]->eve_datafim)): ?>
				        		a <?php echo $this->my_date->datetime($evento[0]->eve_datafim, 'justDate'); ?>
					        	<?php endif; ?>
							</div>
						</div>
					</div>
					<?php endif; ?>

					<!-- DATA LIMITE DE PAGAMENTO COM DESCONTO ESPECIAL -->
					<?php if($eve_data_limite_pagamento_desconto_especial): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Data Limite de Pagamento com Desconto Especial:</div>
							<div class="col-sm-6 value"><?php echo $this->my_date->datetime($evento[0]->eve_data_limite_pagamento_desconto_especial, 'justDate'); ?></div>
						</div>
					</div>
					<?php endif; ?>
					
					<!-- DATA LIMITE DE PAGAMENTO COM DESCONTO -->
					<?php if($eve_data_limite_pagamento_com_desconto): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Data Limite de Pagamento com Desconto:</div>
							<div class="col-sm-6 value"><?php echo $this->my_date->datetime($evento[0]->eve_data_limite_pagamento_com_desconto, 'justDate'); ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- DATA ENCERRAMENTO INSCRIÇÃO ( DATA FIM ) -->
					<?php if($eve_data_encerra_inscricao): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Data Encerramento de Inscrição:</div>
							<div class="col-sm-6 value"><?php echo $this->my_date->datetime($evento[0]->eve_data_encerra_inscricao, 'justDate'); ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- PRESIDENTE DO JURI -->
					<?php if($eve_juri_presidente): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Presidente do Juri:</div>
							<div class="col-sm-6 value"><?php echo $evento[0]->eve_juri_presidente; ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- MEMBROS DO JURI DE CAMPO -->
					<?php if($eve_juri_membros): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Membro Juri de Campo:</div>
							<div class="col-sm-6 value"><?php echo $evento[0]->eve_juri_membros; ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- DELEGADO TÉCNICO -->
					<?php if($eve_delegado_tecnico): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Delegado Técnico:</div>
							<div class="col-sm-6 value"><?php echo $evento[0]->eve_delegado_tecnico; ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- DESENHADOR DE PERCURSO -->
					<?php if(!is_null($eve_armador)): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Desenhador de Percurso:</div>
							<div class="col-sm-6 value"><?php echo $evento[0]->eve_armador; ?></div>
						</div>
					</div>
					<?php endif; ?>

					<!-- STATUS DO EVENTO -->
					<?php if(!is_null($fk_sta_id)): ?>
					<div class="col-sm-12">
						<div class="item">
							<div class="col-sm-6 title">Status do Evento:</div>
							<div class="col-sm-6 value"><span class="btn btn-xs <?php echo $status_bootstrap; ?>"><?php echo $status_titulo; ?></span></div>
						</div>
					</div>
					<?php endif; ?>

				</div>
			</div>


			<?php if($counter==TRUE): ?>

			<!-- 
			//Inscricoes em Breve
			case 1:
			-->
			<?php if($evento[0]->fk_sta_id==1): ?>
			<div class="col-sm-7 contador status1">
				
				<h3>INSCRIÇÕES <b>ABREM</b> EM</h3>
				<div id="counter"></div>
				<div class="desc">
					<div>Dia(s)</div>
					<div>Hora(s)</div>
					<div>Minuto(s)</div>
					<div>Segundo(s)</div>
				</div>
			</div>
			<?php endif; ?>

			<!-- 
			//Inscricoes Abertas
			case 2:
			-->
			<?php if($evento[0]->fk_sta_id==2): ?>
			<div class="col-sm-7 contador status2">
				<h3>INSCRIÇÕES <b>ENCERRAM</b> EM</h3>
				<div id="counter"></div>
				<div class="desc">
					<div>Dia(s)</div>
					<div>Hora(s)</div>
					<div>Minuto(s)</div>
					<div>Segundo(s)</div>
				</div>
				<div class="btninscricao col-sm-12">
					<a href="<?php echo $evento[0]->eve_inscricao ?>" class="btn btn-success btn-block" target="_blank">
						Inscrever agora
					</a>
				</div>
			</div>
			<?php endif; ?>


			<!-- 
			//Inscricoes Encerradas
			case 3:
			prototipo 21
			-->
			<?php if($evento[0]->fk_sta_id==3): ?>
			<div class="col-sm-7 contador status3">

				<div class="box">
					<span class="label label-default">Inscriçõe Encerradas</span>
				</div>

				<h3><b>CONTAGEM REGRESSIVA</b> PARA INICIAR O EVENTO:</h3>
				<div id="counter"></div>
				<div class="desc">
					<div>Dia(s)</div>
					<div>Hora(s)</div>
					<div>Minuto(s)</div>
					<div>Segundo(s)</div>
				</div>
<!-- 				<div class="btninscricao col-sm-12">
					<a href="<?php echo $evento[0]->eve_inscricao ?>" class="btn btn-success btn-block" target="_blank">
						Inscrever agora
					</a>
				</div> -->
			</div>
			<?php endif; ?>


			<!-- 
			//Evento Acontecendo
			case 4:
			prototipo 22
			-->
			<?php if($evento[0]->fk_sta_id==4): ?>
			<div class="col-sm-7 contador status4">

				<div class="box">
					<span class="label label-default">Evento acontecendo</span>
				</div>

				<div class="content">
					<h3>ACOMPANHE A <b>PROVA ONLINE</b></h3>
				</div>
			
			</div>
			<?php endif; ?>


			<!-- 
			//Evento Encerrado
			case 5
			prototipo 23
			-->
			<?php if($evento[0]->fk_sta_id==5): ?>
			<div class="col-sm-7 contador status5">

				<div class="box">
					<span class="label label-default">Evento Encerrado</span>
				</div>

				<div class="content">
					<h3>CONFIRA O <b>RESULTADO DO EVENTO:</b> <a href="#" class="btn btn-default">Clique Aqui</a></h3>
				</div>
			
			</div>
			<?php endif; ?>

			<?php endif; ?>

		</div>


	</div>


	<!-- BEGIN PROVAS -->
	<?php if($provas): ?>
	<div class="row col-sm-12 margin-bottom-30 margin-top-30" id="provas">

		<div class="col-sm-12 margin-bottom-20">
		    <div class="title_template">PROVAS</div>
		</div>

		<div class="col-sm-12">

			<?php foreach ($dias as $dia) : ?>
			<div class="col-sm-12 no-padding">
				<a href="#<?php echo "provas_" . $dia->dia_id; ?>" title="" class="dia_title text-center col-sm-12" id="<?php echo "linkprova_" . $dia->dia_id; ?>">
					<?php $diasemana = $this->my_date->datetime($dia->dia_dia, 'justDate'); ?>
					<?php echo $dia->dia_semana . ' - ' . $diasemana; ?>
					<i class="fa fa-plus-circle"></i>
				</a>
			</div>

			<div class="col-sm-12 no-padding margin-bottom-30 box-table-responsive" id="<?php echo "provas_" . $dia->dia_id; ?>">

				<div class="table-responsive">

				    <table class="table tabela no-margin display stackTableTrigger" cellspacing='0' id="">

				     	<!-- Table Header -->
					    <thead>
					        <tr>
					        	<th>Nome</th>
					        	<th>Pista</th>
					        	<th>Altura</th>
					        	<th>Categorias</th>
					        	<th>Tipo</th>
					        	<th>Hora</th>
					        	<th>Croqui</th>
					        	<th>Status</th>
					        </tr>
					    </thead>

					    <!-- tbody -->
					    <tbody>
						     	<?php $contador=0; ?>
						     	<?php $provas = $this->m_crud->get_allWhere('tb_site_evento_prova', 'fk_dia_id', $dia->dia_id); ?>
						     	<?php if(is_array($provas)): ?>
							        <?php foreach ($provas as $prova): ?>
							        <tr class="<?php echo ($contador % 2==0) ? 'even' : ''; ?>">
							        	<td><?php echo $prova->pro_nome; ?></td>
							        	<td><?php echo $prova->pro_pista; ?></td>
							        	<td><?php echo $prova->pro_altura; ?></td>
							        	<td><?php echo $prova->pro_categorias; ?></td>
							        	<td><?php echo $prova->pro_tipo; ?></td>
							        	<td><?php echo $prova->pro_hora; ?></td>
							        	<td>
							        		<?php if(!empty($prova->pro_croqui)): ?>
							        		<a href="<?php echo base_url() . 'uploads/evento/croqui/' . $prova->pro_croqui; ?>" class="label label-warning btn-block btn-croqui" target="_blank">Ver Croqui</a>
								        	<?php endif; ?>
							        		<?php if(empty($prova->pro_croqui)): ?>
							        		-
								        	<?php endif; ?>
							        	</td>

							        	<!-- STATUS -->
							        	<?php
							        		$statusProva = $this->m_crud->get_rowSpecific('tb_site_evento_prova_status', 'sta_id', $prova->fk_sta_id, 1, 'sta_titulo');
											switch ($prova->fk_sta_id) {

												//A Realizar
												case 1:
													$statusProva_bootstrap  	= 'btn-primary';
													break;

												//Em Andamento
												case 2:
													$statusProva_bootstrap  	= 'btn-success';
													break;

												//Encerrada
												case 3:
													$statusProva_bootstrap  	= 'btn-danger';
													break;

												//Cancelada
												case 4:
													$statusProva_bootstrap  	= 'btn-primary';
													break;

												//Default
												default:
													$status_bootstrap 			= 'btn-default';
													break;

											}
							        	?>
							        	<td>
											<div class="btns">
												
												<span class="item label btn-block <?php echo $statusProva_bootstrap; ?>">Prova <?php echo $statusProva; ?></span>
						
												<!-- ORDEM DE ENTRADA -->
												<?php if($prova->pro_ordem_status==1): ?>
												<div>
													<a href="<?php echo base_url() . 'uploads/evento/ordem/' . $prova->pro_ordem; ?>" title="Fazer download da ordem de entrada da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Ordem de Entrada</a>
												</div>
												<?php endif; ?>

												<!-- RESULTADO ONLINE -->
												<?php if($prova->pro_resultado_link_status==1): ?>
												<div>
													<a href="<?php echo $prova->pro_resultado_link ?>" title="Acessar resultado online da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Resultado Online</a>
												</div>
												<?php endif; ?>

												<!-- RESULTADO EQUIPE -->
												<?php if($prova->pro_resultado_equipe_status==1): ?>
												<div>
													<a href="<?php echo base_url() . 'uploads/evento/equipe/' . $prova->pro_resultado_equipe_pdf; ?>" title="Confira resultado final da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Resultado Equipe</a>
												</div>
												<?php endif; ?>

												<!-- RESULTADO CAMPEONATO -->
												<?php if($prova->pro_resultado_campeonato_status==1): ?>
												<div>
													<a href="<?php echo base_url() . 'uploads/evento/campeonato/' . $prova->pro_resultado_equipe_pdf; ?>" title="Confira resultado final da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Resultado Campeonato</a>
												</div>
												<?php endif; ?>

												<!-- RESULTADO FINAL -->
												<?php if($prova->pro_resultado_pdf_status==1): ?>
												<div>
													<a href="<?php echo base_url() . 'uploads/evento/resultado/final/' . $prova->pro_resultado_pdf; ?>" title="Confira resultado final da prova: <?php echo $prova->pro_nome; ?>" class="item label label-warning btn-block" target="_blank">Resultado Final</a>
												</div>
												<?php endif; ?>

											</div>
							        	</td>
							        </tr>	
							        <?php $contador++;  ?>
									
									<?php
										if(empty($provas))
										echo '<div class="btn btn-warning margin-top-20" style="display:table;">Nenhuma prova para <span style="text-decoration:underline;"><b><i>este dia</i></b></span>.</div>';
									?>



								    <?php endforeach; ?>
								<?php endif; ?>

					    </tbody>
					</table>
				</div>

		</div>
		<!-- END col-sm-12 ( table ) -->

		<?php endforeach; ?>

			<?php
				if(empty($dias))
					echo '<div class="btn btn-warning margin-top-20" style="display:table;">Nenhuma prova para <span style="text-decoration:underline;"><b><i>este evento</i></b></span>.</div>';
			?>

		</div>

	</div>
	<!-- END PROVAS -->
	<?php endif; ?>



	<?php
	/*
	<!-- PATROCINADORES GOLD -->
	<div class="row patrocinadores">
		<div class="col-sm-12">
		    <div class="title_template">PATROCINADORES <b>GOLD</b></div>
		</div>
	</div>


	<!-- PATROCINADORES GOLD -->
	<div class="row patrocinadores">
		<div class="col-sm-12">
		    <div class="title_template">APOIADORES DO <b>EVENTO</b></div>
		</div>
	</div>
	*/
	?>


</div>
<!-- END Evento -->


<?php if($global_map==TRUE): ?>

</div>
<!-- END Container ( opened header.php ) -->


		<!-- MAPA -->
		<div class="localizacao col-sm-12" id="ancora-localizacao" style="padding-top:25px;position:relative;z-index:200;">
			<div class="container">
				<div class="col-sm-1"><i class="fa fa-map-marker"></i></div>
				<div class="col-sm-11 red-title">
					<span class="titulo">Venha nos visitar</span>
					
					<span class="local">
						<?php echo $global_map_content; ?>
					</span>
				</div>
			</div>
		</div>

		<div class="col-sm-12 no-padding" style="position:relative;z-index:100;">
          <div id="map" class="gmaps" style="height:400px;"></div>
        </div>



<div class="container no-padding">

<?php endif; ?>