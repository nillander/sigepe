<div class="container-fluid" id="pagina-agenda">      

  <div class="container">
      
    <!-- BREADCRUMBS -->
    <ul class="breadcrumb">
        <li><a href="index.html">Site</a></li>
        <li class="active">Agenda</li>
    </ul>

      <?php foreach ($agendas as $agenda): ?>
      <div class="col-sm-12">
          <div class="col-sm-2">
              <div class="data">
                  <span class="dia"><?php echo $this->my_date->datetime($agenda->age_data, 'justDay'); ?></span>
                  <span class="mes">
                    <?php $agenda_mes = strtoupper($this->my_date->mes_extenso($agenda->age_data)); ?>
                    <?php echo substr($agenda_mes,0,3); ?>
                  </span>
                  <span class="ano"><?php echo $this->my_date->datetime($agenda->age_data, 'justYear'); ?></span>
              </div>
          </div>
          <div class="col-sm-2">
            <div class="box">
              <a href="<?php echo base_url() . 'agenda/' . $agenda->age_id; ?>" title="">
                <img src="<?php echo base_url() . 'uploads/agenda/' . $agenda->age_capa; ?>" alt="<?php echo $agenda->age_titulo; ?>" style="max-width:100%;" />
              </a>
            </div>
          </div>
          <div class="col-sm-8">
              <h3 class="titulo"><a href="<?php echo base_url() . 'agenda/' . $agenda->age_id; ?>" title="<?php echo base_url() . 'agenda/' . $agenda->age_titulo; ?>"><?php echo $agenda->age_titulo; ?></a></h3>
              <div class="local"><strong>Local:</strong> <?php echo $agenda->age_local; ?></div>
              <div class="conteudo">
                <?php 
                  echo $agenda->age_descricaoCurta;
                ?>
              </div>
              <span class="botao"><a href="<?php echo base_url() . 'agenda/' . $agenda->age_id; ?>" class="btn btn-default btn-primary">DETALHES</a></span>
          </div>
      </div>

      <div class="espacamento">
        <hr class="separador" />
      </div>
      <?php endforeach; ?>


      <div class="centralizador">
      <ul class="pagination">
        <li><a href="#">&laquo;</a></li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#">&raquo;</a></li>
      </ul>
    </div>


  </div>

</div>