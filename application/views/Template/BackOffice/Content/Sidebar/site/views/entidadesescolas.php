<!-- BEGIN EntidadesEscolas -->
<div class="col-sm-9 padding-left-0" id="federacao_listagemEntidadesEscolas">
	
	<ul class="breadcrumb">
	    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
	    <li><a href="<?php echo base_url() . 'afederacao/historia'; ?>">A Federação</a></li>
	    <li class="active"><?php echo $tituloTipoPagina; ?></li>
	</ul>	

	<h1><?php echo $tituloTipoPagina; ?></h1>
	
	<div class="conteudo margin-bottom-30">
		
		<?php foreach ($entidadesescolas as $entidadeescola): ?>

		<?php 
		$titulo 	=	$entidadeescola->ent_titulo;
		$sigla 		=	$entidadeescola->ent_sigla;
		$img 		=	base_url() . 'uploads/entidadesescolas/' . $entidadeescola->ent_logo;
		$link 		=	base_url() . 'a-federacao/' . $slugTipoPagina . '/' . $entidadeescola->ent_slug;
		?>

		<div class="col-sm-6 item padding-left-0">
			<div class="box">
				<div class="col-sm-5 thumb">
					<a href="<?php echo $link; ?>" title="<?php echo $titulo; ?>">
						<img src="<?php echo $img; ?>" alt="" class="img-responsive" />
					</a>
				</div>
				<div class="col-sm-7">
					<a href="<?php echo $link; ?>" title="<?php echo $titulo; ?>" class="titulo"><?php echo $titulo; ?></a>
					<a href="<?php echo $link; ?>" class="btn btn-sm btn-primary">Mais Detalhes</a>
				</div>
			</div>
		</div>
		<?php endforeach; ?>

	</div>


</div>
<!-- END EntidadesEscolas -->
