    
    <!-- BEGIN PAGE BREADCRUMBS -->
    <div class="row">
        <div class="col-sm-12">
            <ul class="page-breadcrumb breadcrumb">
                <?php
                if ($this->uri->segment(1) != "GuestOffice" && $this->uri->segment(1) != "FrontOffice" && $this->uri->segment(1) != "BackOffice") {
                    echo "<li style='color: red;'><span>URI Não amigável, informar ao suporte.</li></span>";
                } else {
                    if($this->uri->segment(1)){
                        echo "<li style='color: black;'><span>Ambiente: ".$this->uri->segment(1)." | </li></span>";
                    }
                    if($this->uri->segment(2)){
                        echo "<li style='color: black;'><span>Módulo: ".$this->uri->segment(2)." | </li></span>";
                    }
                    if($this->uri->segment(3)){
                        echo "<li style='color: black;'><span>Pacote: ".$this->uri->segment(3)." | </li></span>";
                    }
                    if($this->uri->segment(4)){
                        echo "<li style='color: black;'><span>Função: ".$this->uri->segment(4)." | </li></span>";
                    }
                    if($this->uri->segment(5)){
                        echo "<li style='color: black;'><span>Atributo: ".$this->uri->segment(5)." | </li></span>";
                    }
                }
                ?>
            </ul>
        </div>
    </div>
    <!-- END PAGE BREADCRUMBS -->

