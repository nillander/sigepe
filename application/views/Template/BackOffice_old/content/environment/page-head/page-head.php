
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container-fluid">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title" style="padding-left: 15px;padding-right: 15px;">
                <h1>
                    {PageHeadTitle}
                    <small>{PageHeadSubtitle}</small>
                </h1>

            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>
    <!-- END PAGE HEAD-->	