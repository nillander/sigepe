
<?php

    $FotoThumb  =   base_url() . 'assets/sigepe/Global/Images/Autenticacao/Profile.png';
    
    if(is_null($this->session->userdata('PessoaFoto')))
        $FotoThumb  =   base_url() . 'assets/sigepe/Global/Images/Autenticacao/Profile.png';
    
?>



        <!-- BEGIN USER LOGIN DROPDOWN -->
        <li class="dropdown dropdown-user dropdown-dark">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img alt="" class="img-circle" src="<?php echo $FotoThumb; ?>">
                <span class="username username-hide-mobile"><?php echo ucfirst(strtolower($this->session->userdata('PessoaPrimeiroNome'))); ?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    <a href="{base_url}FrontOffice/MeuPerfil">
                        <i class="icon-user"></i> Meu Perfil </a>
                </li>
                <li>
                    <a href="{base_url}FrontOffice/MeuPerfil">
                        <i class="icon-calendar"></i> Calendário  de Eventos </a>
                </li>

<!-- 
                <li>
                    <a href="app_inbox.html">
                        <i class="icon-envelope-open"></i> My Inbox
                        <span class="badge badge-danger"> 3 </span>
                    </a>
                </li>
                <li>
                    <a href="app_todo_2.html">
                        <i class="icon-rocket"></i> My Tasks
                        <span class="badge badge-success"> 7 </span>
                    </a>
                </li>
-->
                <li class="divider"> </li>
                
<!-- 
                <li>
                    <a href="page_user_lock_1.html">
                        <i class="icon-lock"></i> Lock Screen </a>
                </li>
-->
                <li>
                    <a href="{base_url}logout">
                        <i class="icon-login"></i> Sair do SIGEPE </a>
                </li>
            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->

