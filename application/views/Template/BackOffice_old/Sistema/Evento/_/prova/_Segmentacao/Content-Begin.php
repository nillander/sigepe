
                    <div class="col-md-10 col-sm-10 col-xs-10">

                        <div class="tab-content">

                            <?php
                                $Contador = 1;
                                foreach ($DatasetProva as $key => $value){
                            ?>
                            <div class="tab-pane <?php echo ($Contador == '1') ? 'active' : ''; ?>" id="tab_<?php echo $value->srp_id; ?>">
                                    
                                    <?php //echo $value->srp_nome; ?>


                                <div class="portlet-body">
                                    <h3 style="margin-top: 0px;" class="bold"><?php echo nl2br($value->srp_nome); ?></h3>
                                    <?php if(!is_null($value->srp_nome_trofeu)): ?><h4><?php echo nl2br($value->srp_nome_trofeu); ?></h4><?php endif; ?>

                                    <hr style="margin-bottom: 10px;">

                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs ">
                                            <li class="active">
                                                <a href="#tab_<?php echo $value->srp_id; ?>_informacoes" data-toggle="tab">
                                                    Informações
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_<?php echo $value->srp_id; ?>_categorias" data-toggle="tab">
                                                    Categorias
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_<?php echo $value->srp_id; ?>_desenhador" data-toggle="tab"> 
                                                    Desenhador de Percurso 
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_<?php echo $value->srp_id; ?>_provasdependentes" data-toggle="tab"> 
                                                    Provas Dependentes
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#tab_<?php echo $value->srp_id; ?>_inscricoes" data-toggle="tab"> 
                                                    Inscrições
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="tab-content" style="padding-top: 10px;">

