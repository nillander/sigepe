    <style type="text/css">

        .sigepe .tiles .tile{
            overflow: initial !important;
        }

        .tiles .tile{
            border: none !important;
        }

        .tiles .tile .tile-body{
            margin-bottom: 0 !important;
        }




/*
       .sigepe .tiles .tile:after{
            content: "1";
            background: red;
            width: 100%;
            height: 30px;   
        }
*/

        .tiles-empresas .tile{
            margin-bottom: 60px !important;
        }
        .sigepe .tiles .tile{
            width: 100% !important;
            margin-bottom: 0px;
        }
        .sigepe .tiles .tile .tile-object>.name{
            position: relative !important;
            margin-left: 0px !important;
            margin-right: 0px !important;
            text-align: center;
        }
        .sigepe .tiles{
            margin-right: 0px !important;
        }


        .btn.blue:not(.btn-outline) {
            color: #FFF;
            background-color: #3598dc;
            border-color: #3598dc;
            width: 100%;
            white-space: normal;
        }


    </style>


    <div class="col-sm-9 evento-conteudo" id="evento-serie">

        




        <div class="portlet light bordered" style="float: left;width: 100%;">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-speech font-green-sharp"></i>
                    <span class="caption-subject bold uppercase"> SÉRIE</span>
                    <span class="caption-helper">Relação das <b>séries</b> cadastradas para esse evento.</span>
                </div>
                <div class="actions">


                    <a href="{base_url}BackOffice/Evento/Serie/Cadastrar/{EventoId}" class="btn btn-circle display-block yellow-lemon " style="margin-right: 10px">
                        <i class="fa fa-plus"></i> CADASTRAR SÉRIE </a>

                    <a href="{base_url}BackOffice/Evento/Serie/Cadastrar/{EventoId}" class="btn btn-circle display-block yellow-lemon ">
                        <i class="fa fa-plus"></i> CADASTRAR PROVA </a>

                </div>
            </div>
            <div class="portlet-body">


                <div class="tabbable-line" style="display: block">

                    <ul class="nav nav-tabs ">


                        <?php
                            $Contador   =   1;
                            foreach ($DatasetSerie as $key => $value):
                        ?>
                        <li class="<?php echo ($Contador===1) ? 'active' : ''; ?>">
                            <a href="#tab-<?php echo $value->ers_id; ?>" data-toggle="tab">
                                <i class="fa fa-caret-right" aria-hidden="true"></i>
                                <?php echo $value->evs_altura . 'M'; ?>
                            </a>
                        </li>
                        <?php
                            $Contador++;
                            endforeach;
                        ?>

                    </ul>



                    <div class="tab-content" style="padding-top: 10px;">



                        <!-- 
                        ABA DA SERIE
                        =========================================== -->
                        <?php
                            $Contador   =   1;
                            foreach ($DatasetSerie as $key => $value):
                        ?>
                    
                        <div class="tab-pane <?php echo ($Contador===1) ? 'active' : ''; ?>" id="tab-<?php echo $value->ers_id; ?>" data-serie-id="<?php echo $value->evs_id; ?>">

                            <div class="alert alert-info " style="background-color: #f0f0f0 !important; border: none !important; color: #999; padding-top:5px; padding-bottom: 0px;margin-bottom: 15px;float: left;width: 100%;padding: 15px 0;">


                                <div class="col-sm-6">
                                    <small>
                                        <div style="margin-right: 10px;margin-bottom: 5px;">Categorias dessa série: </div> 
                                        <?php
                                        $this->load->module('evento/serie');
                                        $CategoriasDaSerie  =   $this->serie->GetCategoriasDaSerie($value->ers_id);

                                        if(!is_null($CategoriasDaSerie)):
                                            foreach ($CategoriasDaSerie as $key => $categoria) {
                                                echo '<a href="javascript:;" class="btn btn-xs btn-default tooltips" data-original-title="'.$categoria->evc_categoria.'" style="margin-right: 5px;"> '.$categoria->evc_sigla.' </a> ';
                                            }
                                        endif;

                                        if(is_null($CategoriasDaSerie) || empty($CategoriasDaSerie))
                                            echo "<span class='font-red'>ERRO! Nenhuma categoria associada a essa Série. Solicite suporte.</span>";

                                        ?>
                                    </small>
                                </div>

                                <div style="text-align: right;display: block;margin-top: 7px;" class="col-sm-6">

                                    <span
                                        class="btn btn-circle btn-primary popovers btn-sm"
                                        data-container="body"
                                        onclick=" "
                                        data-html="true" 
                                        data-trigger="hover"
                                        data-placement="left"
                                        data-content="

                                            <small> 
    
                                                <b>Nome da Série:</b><br>
                                                <?php echo $value->ers_nome; ?>

                                                <hr style='margin:8px 0;'>
  
                                                <b>Valor até Inscrição Definitiva:</b><br>
                                                R$ <?php echo $this->my_moeda->InserirPontuacao($value->ers_valor_ate_inscricao_definitiva); ?>

                                                <hr style='margin:8px 0;'>
  
                                                <b>Valor após Inscrição Definitiva:</b><br>
                                                R$ <?php echo $this->my_moeda->InserirPontuacao($value->ers_valor_apos_inscricao_definitiva); ?>

                                                <hr style='margin:8px 0;'>
  
                                                <b>ID Série:</b><br>
                                                <?php echo $value->ers_id; ?>

                                            </small>

                                        "
                                        data-original-title="Detalhes">
                                        <i class="fa fa fa-info-circle" aria-hidden="true"></i>
                                        Detalhes da Série
                                    </span>

                                    <a href="#" class="btn btn-circle btn-info btn-sm" style="margin:0 10px;font-weight: 400 !important;"><i class="fa fa-pencil" aria-hidden="true"></i> Editar Série</a>

                                    <a href="#" class="btn btn-circle btn-danger btn-sm" style="border-radius: 28px !important;font-weight: 400 !important;"><i class="fa fa-trash" aria-hidden="true"></i> Deletar Série</a>

                                </div>

                            </div>



                            <?php
                                $this->load->module('evento/Serie');
                                $SerieId            =   $value->ers_id;
                                $DatasetProva       =   $this->serie->GetProvasDaSerie($SerieId);
                                if(empty($DatasetProva)):
                            ?>
                            <hr style="float: left;width: 100%;margin-top: 0;">
                            <div class="alert alert-warning col-sm-12">
                                <strong>Atenção!</strong> <br> Não existe nenhuma prova vinculada a essa série. <br> Para cadastrar uma nova prova a essa série selecione o menu provas na lateral.
                            </div>
                            <?php endif; ?>


                            <!-- Provas Vinculadas a serie -->
                            <?php if(!empty($DatasetProva)): ?>


                            <h3 class="margin-top-20 margin-bottom-20 row col-sm-12"> Provas da Série <?php echo $value->evs_altura . 'M'; ?></h3>   

                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th> Dia </th>
                                            <th> Nº Prova </th>
                                            <th> Característica </th>
                                            <th> Categorias da Prova </th>
                                            <th> Pista </th>
                                            <th> Status </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($DatasetProva as $key => $value): ?>
                                        <tr>
                                            <td>
                                                <?php echo $this->my_data->ConverterData($value->srp_dia, 'ISO', 'PT-BR'); ?>
                                                <br>
                                                <small>
                                                    <?php echo $this->my_data->diasemana($value->srp_dia); ?>
                                                    <br>
                                                    às <?php echo $value->srp_hora; ?>
                                                </small>
                                            </td>
                                            <td> Pr. <?php echo $value->srp_numero_prova; ?> </td>
                                            <td>
                                                <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_caracteristica'); ?>
                                                <br>
                                                <small class="font-grey-cascade"><?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_caracteristica', 'spc_id', $value->fk_spc_id, 1, 'spc_regulamento'); ?></small>
                                            </td>
                                            <td>
                                                <small>

                                                    <?php
                                                        $this->load->module('evento/BackOffice/prova');
                                                        $CategoriasDaProva  =   $this->prova->GetCategoriasDaProva($value->srp_id);
                                                        foreach ($CategoriasDaProva as $key => $value):
                                                    ?>
                                                    <a href="javascript:;" class="btn btn-xs btn-default tooltips" data-original-title="<?php echo $value->evc_categoria; ?>" style="margin-right: 5px;"> <?php echo $value->evc_sigla; ?> </a>
                                                    <?php endforeach; ?>

                                                </small>
                                            </td>
                                            <td>
                                                <?php echo $this->model_crud->get_rowSpecific('tb_evento_serie_prova_pista', 'spp_id', $value->fk_spp_id, 1, 'spp_pista'); ?>
                                            </td>
                                            <td>
                                                <?php
                                                        echo '<span class="label label-sm label-info"> '. $this->model_crud->get_rowSpecific('tb_status', 'sta_id', $value->fk_sta_id, 1, 'sta_status') .' </span>';
                                                ?>
                                                
                                            </td>
                                        </tr>
                                        <?php endforeach;  ?>
                                      
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>




                        </div>
                        <?php
                            $Contador++;
                            endforeach;
                        ?>
    







                    </div>
                </div>            










            </div>
        </div>




</div>
<!-- /evento-serie -->





 