
	<!-- BEGIN PAGE-CONTENT-WRAPPER ( this div will be close footer.php file ) -->
	<div class="page-content-wrapper">

		<!-- BEGIN PAGE-CONTENT ( this div will be close content-end.php file ) -->
		<div class="page-content">
			
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>{module} <small><!-- {breadcrumbs_page} --></small></h1>
				</div>
				<!-- END PAGE TITLE -->
			</div>
			<!-- END PAGE HEAD -->


			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb">
				
				<li>
					<a href="{base_url}">MANAGER</a>
					<i class="fa fa-circle"></i>
				</li>
		
				<li>
				</li>
				<?php //$this->templatemounting->mounting( $crudOperation, 'breadcrumb', $moduleSlug ); ?>

			</ul>
			<!-- END PAGE BREADCRUMB -->


			<!-- Begin Row-->
			<div class="row">

				<!-- Begin col-sm-12 -->
				<div class="col-md-12">
