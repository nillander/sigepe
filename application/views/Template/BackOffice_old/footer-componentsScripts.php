
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{base_url}assets/global/plugins/respond.min.js"></script>
<script src="{base_url}assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="{base_url}assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="{base_url}assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->


<!-- Datatable
============================================= -->
<?php if($use_datatable): ?>
<script type="text/javascript" src="{base_url}assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<?php endif; ?>


<!-- Fancybox
============================================= -->
<script type="text/javascript" src="{base_url}assets/global/plugins/fancybox/source/jquery.fancybox.pack.js"></script>


<!-- Modal
============================================= -->
<?php if($use_modal): ?>
<script src="{base_url}assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<?php endif; ?>


<!-- Toastr
============================================= -->
<?php if($use_toastr): ?>
<script src="{base_url}assets/global/plugins/bootstrap-toastr/toastr.min.js"></script>
<?php endif; ?>


<!-- Datepicker
============================================= -->
<?php if($use_datepicker): ?>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/clockface/js/clockface.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<?php endif; ?>


<!-- Dropdown
============================================= -->
<?php if($use_dropdown): ?>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>
<?php endif; ?>


<!-- Slug
============================================= -->
<?php if($use_slug): ?>
<script type="text/javascript" src="{base_url}assets/override/plugins/jQuery-Slugify-Plugin-master/jquery.slugify.js"></script>
<?php endif; ?>


<!-- Validation Scripts
============================================= -->
<?php if($use_cft): ?>
<script type="text/javascript" src="{base_url}assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<?php endif; ?>


<!-- CFT - Components Form Tools
============================================= -->
<?php if($use_cft): ?>
<script type="text/javascript" src="{base_url}assets/global/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
<script src="{base_url}assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<!-- Forms Samples --><script src="{base_url}assets/admin/pages/scripts/form-samples.js"></script>
<?php endif; ?>


<!-- Markdown & WYSIWYG Editors
============================================= -->
<?php if($use_cft): ?>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="{base_url}assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script src="{base_url}assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="{base_url}assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<?php endif; ?>



<!-- Gallery Plugin
============================================= -->
<?php if($use_jfum): ?>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/vendor/load-image.min.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
<script src="{base_url}assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
    <script src="{base_url}assets/global/plugins/jquery-file-upload/js/cors/jquery.xdr-transport.js"></script>
    <![endif]-->
<?php endif; ?>



  <script src="{base_url}assets/global/plugins/ckeditor/ckeditor.js"></script>


<!-- jQuery Crop Plugin
============================================= -->
<?php if($use_jcrop): ?>
  <script src="{base_url}assets/override/plugins/cropper-master/dist/cropper.min.js"></script>
  <script src="{base_url}assets/override/plugins/cropper-master/examples/crop-avatar/js/main.js"></script>
<?php endif; ?>


<!-- Portfolio
============================================= -->
<?php if($use_portfolio): ?>
<script type="text/javascript" src="{base_url}assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<?php endif; ?>


<!-- jMask
============================================= -->
<?php if($use_jmask): ?>
<script type="text/javascript" src="{base_url}assets/override/plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js"></script>
<?php endif; ?>


<!-- Tab Maps
============================================= -->
<?php if(isset($tabMaps) && $tabMaps): ?>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript" src="{base_url}assets/override/plugins/google-dynamic-map/google_dynamic_map.js"></script>
<?php endif; ?>


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- Toastr --><?php if($use_toastr): ?><script src="{base_url}assets/admin/pages/scripts/ui-toastr.js"></script><?php endif; ?>
<!-- Metronic.js --><script src="{base_url}assets/global/scripts/metronic.js" type="text/javascript"></script>
<!-- Layout.js --><script src="{base_url}assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
<!-- Demo.js --><script src="{base_url}assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>
<!-- Components Forms Tools - CFT --><?php if($use_cft): ?><script src="{base_url}assets/admin/pages/scripts/components-form-tools.js"></script><?php endif; ?>
<!-- Makdown & WYSIWYG Editors --><?php if($use_cft): ?><script src="{base_url}assets/admin/pages/scripts/components-editors.js"></script><?php endif; ?>
<!-- Form Validation --><?php if($use_cft): ?><script src="{base_url}assets/admin/pages/scripts/form-validation.js"></script><?php endif; ?>
<!-- File Upload --><?php if($use_jfum): ?><script src="{base_url}assets/admin/pages/scripts/form-fileupload.js" type="text/javascript"></script><?php endif; ?>
<!-- Portfolio --><?php if($use_portfolio): ?><script src="{base_url}assets/admin/pages/scripts/portfolio.js"></script><?php endif; ?>
<!-- Form - Change Password --><script src="{base_url}assets/admin/pages/scripts/form-change-password.js"></script>


<!-- Complements
============================================= -->
<?php if($use_datatable): ?><script src="{base_url}assets/admin/pages/scripts/table-advanced.js"></script><?php endif; ?>
<?php if($use_modal): ?><script src="{base_url}assets/admin/pages/scripts/ui-extended-modals.js"></script><?php endif; ?>
<?php if($use_dropdown): ?><script src="{base_url}assets/admin/pages/scripts/components-dropdowns.js"></script><?php endif; ?>
<?php if($use_datepicker): ?><script src="{base_url}assets/admin/pages/scripts/components-pickers.js"></script><?php endif; ?>


<!-- Override Javascripts
============================================= -->
<!-- CRUD  --><script src="{base_url}assets/global/scripts/crud.js"></script>
<!-- C --><?php if(isset($create) && $create==TRUE): ?><script src="{base_url}assets/global/scripts/crud-create.js"></script><?php endif; ?>
<!-- R --><?php if(isset($retrieve) && $retrieve==TRUE): ?><script src="{base_url}assets/global/scripts/crud-retrieve.js"></script><?php endif; ?>
<!-- U -->
<?php if(isset($update) && $update==TRUE): ?>
	<!-- <script src="{base_url}assets/global/scripts/crud-retrieve.js"></script>-->
	<script src="{base_url}assets/global/scripts/crud-update.js"></script>
	<!--<script src="{base_url}assets/global/scripts/crud-create.js">-->
<?php endif; ?>

<?php if(isset($delete) && $read==TRUE): ?><script src="{base_url}assets/global/scripts/crud-read.js"></script><?php endif; ?>



<script>
/*CKEDITOR.replace( 'blo_conteudo' ,{
	filebrowserBrowseUrl : baseUrl + 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
	filebrowserUploadUrl : baseUrl +'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
	filebrowserImageBrowseUrl : baseUrl + 'filemanager/dialog.php?type=1&editor=ckeditor&fldr='
});*/
</script>