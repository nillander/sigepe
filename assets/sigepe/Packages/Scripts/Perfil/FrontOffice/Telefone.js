/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){


    /*
        TRIGGER 
        BTN-DELETAR-TELEFONE
    ===========================================================*/
    $('body').on('click', '.btn-deletar-telefone', function (){

    	TableRowId 		=	$(this).parent().parent().data('id');

        swal({
          title: "Você tem certeza?",
          text: "Esse procedimento irá deletar o telefone associado a sua conta. Esse procedimento é irreversível.",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Sim, deletar telefone!",
          closeOnConfirm: false
        },
        function(){
            $.ajax({
                url : BaseUrl + 'perfil/FrontOffice/Telefone/Deletar',
                type : 'POST',
                data: { TelefoneId: TableRowId },
                async: false,
                dataType:'json',
                success : function(data) {            
                    if(data != false){
                        $("#table-row-" + TableRowId).fadeOut();
                        $("body, html").animate({ 
                          scrollTop: $( "#perfil-telefone" ).offset().top 
                        }, 600);
                        swal("Telefone deletado!", "Ação realizada com sucesso.", "success");
                    }else{
                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação de deletar telefone. Tente novamente. ", "error");
                        return;                                    
                    }
                },
                error : function(request,error) { alert("Request: "+JSON.stringify(request)); }
            });
        });

	});





    /*
        TRIGGER 
        BTN-TELEFONE-PRINCIPAL
    ===========================================================*/
    $('body').on('click', '.btn-telefone-principal', function (){

        TableRowId      =   $(this).parent().parent().data('id');

        swal({
          title: "Você tem certeza?",
          text: "Esse procedimento irá transformar esse telefone como principal.",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-warning",
          confirmButtonText: "Sim!",
          closeOnConfirm: false
        },
        function(){
            $.ajax({
                url : BaseUrl + 'perfil/FrontOffice/Telefone/TelefonePrincipal',
                type : 'POST',
                data: { TelefoneId: TableRowId },
                async: false,
                dataType:'json',
                success : function(data) {            

                    if(data != false){
                        
                        // Remover todos os Flag P que houver.
                        $("#table-telefone").find('.flag-telefone-principal').remove();


                        // HTML dos btns
                        HtmlBtnDeletarTelefone      =   "<a href='javascript:;' class='btn btn-circle btn-sm btn-danger btn-deletar-telefone'> <i class='fa fa-close' aria-hidden='true'></i> Deletar Telefone </a>";
                        HtmlBtnTelefonePrincipal    =   "<a href='javascript:;' class='btn btn-circle btn-sm btn-warning btn-telefone-principal tooltips' data-original-title='Transformar esse telefone como principal.'> <i class='fa fa-pencil' aria-hidden='true'></i> Telefone Principal </a>";


                        // Inserindo Botão de Telefone Principal e Deletar Telefone para o antigo telefone principal
                        $("#table-telefone tbody tr").each(function(){

                            if( ! $(this).find('.btn-telefone-principal').length )
                                $(this).find('td:eq(1)').append(HtmlBtnTelefonePrincipal);

                            if( ! $(this).find('.btn-deletar-telefone').length )
                                $(this).find('td:eq(1)').append(HtmlBtnDeletarTelefone);

                        });                        


                        // HTML flag Telefone Principal
                        HtmlTagTelefonePrincipal    =   "<a href='javascript:;' class='tooltips badge badge-warning bold flag-telefone-principal' title='' data-original-title='Telefone Principal. Preferencialmente vamos utilizar esse número para entrar em contato com você.'> P </a>";


                        // Atualizando na linha
                        $("#table-row-" + TableRowId).find('.btn-telefone-principal').remove();
                        $("#table-row-" + TableRowId).find('.btn-deletar-telefone').remove();
                        $("#table-row-" + TableRowId).children("td:first").prepend(HtmlTagTelefonePrincipal);


                        // Resetando componentes
                         $(".popovers").popover();  // Hack para reinicializar popover. Popover nao funciona para elementos carregados dinamicamente via ajax.
                         $(".tooltips").tooltip();  // Hack para reinicializar 

                        // Alerta
                        swal("Telefone alterado como principal!", "Ação realizada com sucesso.", "success");

                    }else{
                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação de deletar telefone. Tente novamente. ", "error");
                        return;                                    
                    }

                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                }
            });

        });

    });





    /*
        TRIGGER 
        BTN-CADASTRAR-TELEFONE
    ===========================================================*/
    jQuery("#perfil-telefone .portlet-title .btn-cadastrar-telefone, #perfil-telefone #aviso-nenhum-telefone").click(function(e){
        e.preventDefault();
        $("#form-telefone").slideDown();
        $("body, html").animate({ 
          scrollTop: $( "#form-telefone" ).offset().top 
        }, 600);
    });

    $("#perfil-telefone .form-actions .btn-submit").click(function(){
        $("#form-telefone").submit();
    });



    /*
        FORM VALIDATION
    ===========================================================*/
    var form        =   $('#form-telefone');
    var error       =   $('.alert-danger', form);
    var success     =   $('.alert-success', form);

    form.validate( {

        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            telefone: {
                required: true,
            },
            tipo: {
                required: true,
            }
        },
        messages: {
            telefone: {
                required: "Informe um telefone.",
            },
            tipo: {
                required: "Informe qual o tipo do telefone.",
            },
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form_gender_error");
            } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form_payment_error");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();

            $.ajax({

                url : BaseUrl + 'perfil/FrontOffice/Telefone/AjaxProcessar',
                type : 'POST',
                data: { DataSerialized: $("#form-telefone").serialize() },
                async: false,
                dataType:'json',
                success : function(data) {            

                    if(data != false){

                        $("#aviso-nenhum-telefone").remove();
                        $("#table-scrollable-telefone").slideDown();

                        LineTelefone 	=	$("#form-telefone input[name='telefone']").val();
                        LineTipo 		=	$("#form-telefone input[name='tipo']").val();
                        LineId 			=	data;

                        $("#form-telefone .alert").hide();                        
                        $("#form-telefone input").val('');                        
                        $('#form-telefone select').prop('selectedIndex',0);
						$("#form-telefone").slideUp();

						/*
                            Inserindo Dinamicamente o telefone cadastrado.
                            E realizado uma verificacao pra checar se sera o primeiro telefone ou nao. 
                        */ 
                        if($("#table-telefone tbody tr").length > 0){
                            $("#table-telefone tbody tr:first").after(data.TableRow);
                        }else{
                            $("#table-telefone tbody").prepend(data.TableRow);
                        }


                        // Hack para reinicializar popover. Popover nao funciona para elementos carregados dinamicamente via ajax.
                        $(".popovers").popover();  

					    $("body, html").animate({ 
					      scrollTop: $( "#perfil-telefone" ).offset().top 
					    }, 600);

                        swal("Telefone cadastrado!", "Ação realizada com sucesso.", "success");

                    }else{
                        sweetAlert("Oops... Aconteceu algo de errado!", " Houve algum erro ao processar sua solicitação de cadastrar telefone. Tente novamente. ", "error");
                        $("#form-telefone input").val('');                        
                        $('#form-telefone select').prop('selectedIndex',0);
                        return;                                    
                    }

                },
                error : function(request,error)
                {
                    alert("Request: "+JSON.stringify(request));
                }
            });

        }
        // end submitHandler

    }); // end form.validate

    
});