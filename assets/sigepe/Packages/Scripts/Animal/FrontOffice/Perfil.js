$(document).ready(function(){

	$(".btn-editar").click(function(e){
		e.preventDefault();
		handleForm();
	});

	$(".btn-salvar").click(function(e){

		var form1 = $('#form-animal');
	    var error1 = $('.alert-danger', form1);
	    var success1 = $('.alert-success', form1);

	    form1.validate({
	        errorElement: 'span', //default input error message container
	        errorClass: 'help-block help-block-error', // default input error message class
	        focusInvalid: false, // do not focus the last invalid input
	        ignore: "",  // validate all fields including form hidden input
	        rules: {
	            'peso': {required: true },
	            'nome-patrocinado': {required: true }
	        },
	        messages: {
	            'peso': {required: "Campo obrigatório"},
	            'nome-patrocinado': {required: "Campo obrigatório"}
	        },

	        invalidHandler: function (event, validator) { //display error alert on form submit              
	            success1.hide();
	            error1.show();
	            App.scrollTo(error1, -200);
	        },

	        errorPlacement: function (error, element) { // render error placement for each input type
	            var cont = $(element).parent('.input-group');
	            if (cont.size() > 0) {
	                cont.after(error);
	            } else {
	                element.after(error);
	            }

	        },

	        highlight: function (element) { // hightlight error inputs
	            $(element)
	                .closest('.form-group').addClass('has-error'); // set error class to the control group
	        },

	        unhighlight: function (element) { // revert the change done by hightlight
	            $(element)
	                .closest('.form-group').removeClass('has-error'); // set error class to the control group
	        },

	        success: function (label) {
	            label
	                .closest('.form-group').removeClass('has-error'); // set success class to the control group
	        },

	        submitHandler: function (form) {
	            success1.show();
	            error1.hide();
	            
	            formSubmitAjax();
	        }
	    });

	    function formSubmitAjax() {

	        $.ajax({
	            type: "POST",
	            dataType:'json',
	            url : BaseUrl + 'animal/FrontOffice/Animal/processar/',
	            data: { dataJson: $('#form-animal').serializeFormJSON() },
	            error: function (jqXHR, textStatus, errorThrown) {
	                console.log(jqXHR);
	                console.log(textStatus);
	                console.log(errorThrown);
	                alert("Server down. Try Later")
	            },
	            success: function (data) {

	                console.log("data down");
	                console.log(data);
	                if(data == true){
	                    if($('input[name=id]').val() == undefined){
	                    }else{
	                        sweetAlert("Sucesso!", "Animal atualizado.", "success");
						    $("input.form-control").each(function(){
								valueInput 	=	 $(this).val();
								nameInput 	=	 $(this).attr('name');
								fcs 		=    'fcs-' + nameInput;
								$("#"+fcs).html(valueInput);
						    });	  
	                        handleForm('resetar');

	                    }
	                }else{
	                    sweetAlert("Erro!", "Solicitação não processada. Solicite suporte técnico.", "error");
	                }
	            }
	        });

	    }

       
    });


	function handleForm(action){

		$("body, html").animate({ 
		  scrollTop: $( "#form-animal" ).offset().top 
		}, 600);		

		if(action=='resetar'){
	        $(".btn-salvar").hide();
	        $(".btn-editar").show();
	        $("input[name=nome-patrocinado],input[name=peso]").hide();
	        $("#fcs-nome-patrocinado, #fcs-peso").show();
		}else{
	        $(".btn-salvar").show();
	        $(".btn-editar").hide();
	        $("input[name=nome-patrocinado],input[name=peso]").show();
	        $("#fcs-nome-patrocinado, #fcs-peso").hide();
		}

	}




	// Form Serialize 
	// Request Ajax
	// Tratamento de exceção ( aviso de sucesso/erro ) 		

});