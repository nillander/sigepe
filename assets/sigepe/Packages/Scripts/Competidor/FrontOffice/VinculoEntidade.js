/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){


    $('#btn-trocar-entidade').on('click', function(event) {
        event.preventDefault();
        $("body, html").animate({ 
          scrollTop: $( "#portlet-vinculos" ).offset().top 
        }, 600);
        $("#tablerow-trocar-entidade").slideDown();
    });


    $('#form-trocar-entidade .btn-submit').on('click', function(event) {
        event.preventDefault();
        $("#form-trocar-entidade").submit();
    });


    $('#form-trocar-entidade .btn-cancelar').on('click', function(event) {
        event.preventDefault();
        $("#tablerow-trocar-entidade").hide();
        $("body, html").animate({ 
          scrollTop: $( "#portlet-vinculos" ).offset().top 
        }, 600);
        $('#form-trocar-entidade .form-group').removeClass('has-error');
        $('#form-trocar-entidade')[0].reset();
    });


    var FormTrocarEntidade        =   $('#form-trocar-entidade');
    var error       =   $('.alert-danger', FormTrocarEntidade);
    var success     =   $('.alert-success', FormTrocarEntidade);

    FormTrocarEntidade.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            'entidade-equestre': {
                required: true
            }
        },
        messages: {
            'entidade-equestre': {
                required: "Todos os atletas da FHBr devem estar vinculados a alguma entidade equestre.",
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "entidade-equestre") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form-entidade-equestre-erro");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -85);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (FormTrocarEntidade) {
            success.show();
            error.hide();
            ProcessarTrocaEntidade();
            return false;
            //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
        },


    });    




    function ProcessarTrocaEntidade() {

        EntidadeSelecionada    =   $("#entidade-equestre option:selected").text();

        $("#tablerow-trocar-entidade").hide();
        $("body, html").animate({ 
          scrollTop: $( "#portlet-vinculos" ).offset().top 
        }, 600);
        $('#btn-trocar-entidade').remove();

        $.ajax({

            url : BaseUrl + 'competidor/FrontOffice/CompetidorVinculo/AjaxTrocaEntidade/',
            type : 'POST',
            data: { DataSerialized: $("#form-trocar-entidade").serialize() },
            async: true,
            dataType:'json',
            success : function(data) {            

                console.log(data);

                
                if(data == true){

                    sweetAlert(
                            "Entidade Equestre atualizada!",
                            "Sua solicitaçao foi realizada com sucesso. Aguarde a aprovaçao do vínculo pela entidade selecionada.",
                            "success");
                    
                    setTimeout(function () { 
                      location.reload();
                    }, 1 * 1000);

                }else{

                    sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");

                }


            },
            error : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
                sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
            }
        });
        
    }
    

});
// end jquery 

