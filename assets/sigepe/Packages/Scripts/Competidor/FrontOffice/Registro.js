/**
 * SIGEPE
 * 
 * @package 	Vanguarda
 * @author      Gustavo Botega
 * @copyright   Copyright (c) 2017, Gustavo Botega
 * @link        http://gustavobotega.com
 * @link        http://agenciavanguarda.com.br
 * 
 */

$(document).ready(function(){


    /*
        Click - Novo Registro
    */
    jQuery(".btn-novo-registro").click(function(){
        $("#portlet-formulario-registro").slideDown();
        $("body, html").animate({scrollTop: $( '#portlet-formulario-registro' ).offset().top - 25 }, 600);
    });


    /*
        Change - Modalidade 
    */
    $('input[type=radio][name=modalidade]').change(function() {

        $(".tipo-registro-item").hide();
        $(".loading-tipo-registro").slideDown();
        $("input[name='tipo-registro']:radio").removeAttr("checked");

        $(".aviso-dependencia").slideDown();
        $("#portlet-valores").slideUp();
        $("#form-groupo-tipo-pagamento").slideUp();


        ModalidadeId = this.value;
        $.ajax({

            url : BaseUrl + 'registro/FrontOffice/Taxa/GetTipoRegistro/',
            type : 'POST',
            data: { ModalidadeId: ModalidadeId },
            async: false,
            dataType:'json',
            success : function(data) {            


                if(data.length === 0){
                    sweetAlert("Modalidade", "A modalidade selecionada não possui nenhum tipo de registro associado. Para maiores esclarecimentos entre em contato com a secretaria da federação.", "error");
                    $('input[name="modalidade"]').prop('checked', false);

                   throw new Error('This is not an error. This is just to abort javascript');
                }

                if(data.length > 0){
                    $.each(data, function(index, value) {
                        $("#tipo-registro-item-" + value).show();
                    });
                }

                setTimeout(function(){ 
                    $("body, html").animate({scrollTop: $( '#formulario-registro-tipo-registro' ).offset().top }, 600);
                }, 600);

                $(".loading-tipo-registro").slideUp();
                $("#formulario-registro-tipo-registro .alert-warning").slideUp();
                $("#formulario-registro-tipo-registro .form-group").slideDown();

            },
            error : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
                sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
            }
        });

    });



    /*
        Change - Tipo de Registro 
    */
    $('input[type=radio][name="tipo-registro"]').change(function() {

        $(".aviso-dependencia").slideUp();
        $("#portlet-valores").slideDown();
        $("#form-groupo-tipo-pagamento").slideDown();
        $('input[name="tipo-pagamento"]').prop('checked', false);
        $("#tipo-pagamento-boleto-content").slideUp();
        TipoCompetidor    =   $("input[name='tipo-competidor']").val()

        TipoRegistroId      = this.value;
        ModalidadeId        = $('input[name="modalidade"]:checked').val();  
        TipoRegistro        = $('input[name="tipo-registro"]:checked').data('title');
        
        $.ajax({

            url : BaseUrl + 'registro/FrontOffice/Taxa/GetValores/',
            type : 'POST',
            data: { ModalidadeId: ModalidadeId, TipoRegistroId: TipoRegistroId, TipoCompetidorId: TipoCompetidor },
            async: false,
            dataType:'json',
            success : function(data) {            


                /*
                Resgatando valores
                ---------------------------------*/
                /* Taxas */
                TaxaId                      =   data.Info.Id;
                TaxaTitulo                  =   data.Taxa.Titulo;
                TaxaDescricao               =   data.Taxa.Descricao;
                TaxaValor                   =   data.Taxa.Valor;
                TaxaValorMascara            =   'R$ ' + TaxaValor.replace(".", ",");

                // Taxas Extras
                TaxaExtraFlag               =   data.TaxaExtra.Flag;
                TaxaExtraTitulo             =   data.TaxaExtra.Titulo;
                TaxaExtraDescricao          =   data.TaxaExtra.Descricao;
                TaxaExtraValor              =   data.TaxaExtra.Valor;
                TaxaExtraObrigatorio        =   data.TaxaExtra.Obrigatorio;

                /* Desconto */
                DescontoFlag                =   data.Desconto.Flag;
                DescontoTitulo              =   data.Desconto.Titulo;
                DescontoDescricao           =   data.Desconto.Descricao;
                if (typeof data.Desconto.Valor != "undefined")
                    DescontoValor                    =   data.Desconto.Valor;

                /* Total */
                TotalBruto                  =   data.Total.Bruto;
                TotalLiquido                =   data.Total.Liquido;


                /*
                Label e valor da Taxa de Registro 
                ---------------------------------*/
                $("#tablerow-taxa-item .first .titulo").html( TaxaTitulo );
                $("#tablerow-taxa-item .first .descricao span").html( TaxaDescricao );
                $("#tablerow-taxa-item .second").html(TaxaValorMascara);



                /*
                Label e valor - Taxa Extra
                ---------------------------------*/
                if(TaxaExtraFlag){
                    $("#tablerow-taxa-extra").show();
                    $("#tablerow-taxa-extra td.first .titulo").html( TaxaExtraTitulo );
                    $("#tablerow-taxa-extra td.first .descricao span").html( TaxaExtraDescricao );
                    $("#tablerow-taxa-extra .second").html('R$ ' + TaxaExtraValor.replace(".", ","));
                }else{
                    $("#tablerow-taxa-extra").hide();
                }             


                /*
                Bloco Desconto
                ---------------------------------*/
                if(!DescontoFlag)
                    $("#tablerow-desconto, #tablerow-desconto-item").hide();

                if(DescontoFlag){
                    $("#tablerow-desconto, #tablerow-desconto-item").show();
                    $("#tablerow-desconto-item .first .titulo").html( DescontoTitulo );
                    $("#tablerow-desconto-item .first .descricao").html( DescontoDescricao );
                    $("#tablerow-desconto-item .second").html('R$ ' + DescontoValor.replace(".", ","));
                }


                /*
                Bloco Total
                ---------------------------------*/
                if(DescontoFlag){
                    $("#preco-total-promocional").show();
                    $("#preco-total-promocional").html('R$ ' + TotalBruto.replace(".", ","));
                }

                if(!DescontoFlag){
                    $("#preco-total-promocional").hide();
                }

                $("#preco-total").html('R$ ' +  TotalLiquido.replace(".", ","));
                $("#pagarme-preco").val( TotalLiquido.replace(".", "") );

                $("#portlet-valores").slideDown();
                $("#form-groupo-tipo-pagamento").slideDown();


                $('input[name="sigepe-taxa"]').val(TaxaId);
                 $(".popovers").popover();  // Hack para reinicializar popover. Popover nao funciona para elementos carregados dinamicamente via ajax.



                setTimeout(function(){ 
                    $("body, html").animate({scrollTop: $( '#formulario-registro-valores' ).offset().top }, 600);
                }, 600);


            },
            error : function(request,error)
            {
                alert("Request: "+JSON.stringify(request));
                sweetAlert("Oops... Algo de errado!", "Aconteceu algum problema ao processar sua solicitação.", "error");
            }
        });
        
    });



    /*
        Change - Tipo de Pagamento
    */
    $('input[type=radio][name="tipo-pagamento"]').change(function() {

        $('.tipo-pagamento-item-active').removeClass('tipo-pagamento-item-active');
        $(this).parent().addClass('tipo-pagamento-item-active');

        $("#tipo-pagamento-boleto-content").slideUp();
        //$("#tipo-pagamento-cartao-credito-content").slideUp();

        TipoPagamentoId     =   this.value;

        // Boleto bancario selecionado
        if(TipoPagamentoId == '1'){
            $("#tipo-pagamento-boleto-content").slideDown();
            $("#form-registro-submit small").text('Gerar Boleto');
        } 

        // Cartao de credito selecionado
        if(TipoPagamentoId == '2'){ 
            $("#form-registro-submit small").text('Cartão de Crédito');
            //$("#tipo-pagamento-cartao-credito-content").slideDown();
        } 


        setTimeout(function(){ 
            $("body, html").animate({scrollTop: $( '#formulario-registro-pagamento' ).offset().top }, 600);
        }, 600);

    });

    




    /*
        Trigger - Submit Registro
    */
    $("#form-registro-submit").click(function(){
        $("#form-registro").submit();
    });


    var form        =   $('#form-registro');
    var error       =   $('.alert-danger', form);
    var success     =   $('.alert-success', form);

    /* Validate */
    form.validate({
        doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            'modalidade': {
                required: true
            },
            'tipo-registro': {
                required: true
            },
            'tipo-pagamento': {
                required: true
            },
        },
        messages: {
            'modalidade': {
                required: "Informe uma modalidade.",
            },
            'tipo-registro': {
                required: "Selecione um tipo de registro.",
            },
            'tipo-pagamento': {
                required: "Selecione um tipo de pagamento.",
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            if (element.attr("name") == "modalidade") { // for uniform radio buttons, insert the after the given container
                error.insertAfter("#form-modalidade-erro");
            } else if (element.attr("name") == "tipo-registro") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form-tipo-registro-erro");
            } else if (element.attr("name") == "tipo-pagamento") { // for uniform checkboxes, insert the after the given container
                error.insertAfter("#form-tipo-pagamento-erro");
            } else {
                error.insertAfter(element); // for other inputs, just perform default behavior
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit   
            success.hide();
            error.show();
            App.scrollTo(error, -85);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                label
                    .closest('.form-group').removeClass('has-error').addClass('has-success');
                label.remove(); // remove error label here
            } else { // display success icon for other inputs
                label
                    .addClass('valid') // mark the current input as valid and display OK icon
                .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
            }
        },

        submitHandler: function (form) {
            success.show();
            error.hide();
            SigepeCriarRegistro();
            return false;
        },

    });    
    


    /*
        SigepeCriarRegistro
        - Cria registro e financeiro
    ========================================================================*/
    function SigepeCriarRegistro () {

        $("#perfil-competidor").slideUp();// GMB
        $("#layer-loading-registro").fadeIn();

        $.ajax({
            type: "POST",
            url: BaseUrl + "registro/FrontOffice/Registro/Processar",
            data: { FormSerialized: $( "#form-registro" ).serialize() },
            dataType:'json',
            success: function( data ){

                $("#layer-loading-registro").fadeOut();
                $("#perfil-competidor").slideUp();

                $("input[name='sigepe-transacao']").val(data.Transacao);
                $("input[name='sigepe-registro']").val(data.RegistroId);


                TipoPagamentoSelecionado    =   $('input[type=radio][name="tipo-pagamento"]:checked').val();

                if(TipoPagamentoSelecionado == '1') 
                    CreateTransactionBoleto();

                if(TipoPagamentoSelecionado == '2')
                    CheckoutCartaoCredito();


            },
            error: function (jqXHR, exception) {
              $("#layer-loading-registro").fadeOut();
                console.log(jqXHR);
                GetErrorMessage(jqXHR, exception);
                sweetAlert(
                        "Oops.. aconteceu algum problema!",
                        "Seu registro NÃO foi processado. Entre em contato com a secretaria da federação para mais detalhes.",
                        "error");                           
            },
            complete: function (data) {
              $("#layer-loading-registro").fadeOut();
            }
        });


        setTimeout(function(){ $("#layer-loading-registro").fadeOut() }, 75 *1000);

    }
    // end SigepeCriarRegistro()




    
    
    

});
// end jquery 






