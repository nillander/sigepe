var FormValidation = function() {

    // basic validation
    var handleValidation1 = function() {


        var form1 = $('#form-prova');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            rules: {
                'serie': {required: true},
                'nome-prova': {required: true},
                'numero-prova': {required: true},
                'tipo-pista': {required: true},
                'tipo-sorteio': {required: true},
                'caracteristica': {required: true},
                'valor-prova': {required: true},
                'limite-inscricao-prova': {required: true},
                'limite-inscricao-atleta': {required: true},
                'prova-dependente': {required: true},
                'dia': {required: true},
            },
            messages: {
                'serie': {required: "Selecione qual série essa prova faz parte"},
                'nome-prova': {required: "Informe o nome da prova"},
                'numero-prova': {required: "Informe o número da prova"},
                'tipo-pista': {required: "Informe o tipo da pista"},
                'tipo-sorteio': {required: "Informe o tipo do sorteio"},
                'caracteristica': {required: "Informe a característica da prova"},
                'valor-prova': {required: "Preencha o valor da prova"},
                'limite-inscricao-prova': {required: "Preencha o campo"},
                'limite-inscricao-atleta': {required: "Preencha o campo"},
                'prova-dependente': {required: "Selecione uma opção"},
                'dia': { required: "Selecione uma opção"},
            },

            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function(error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');


                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }

                if (element.attr("type") == "radio") {
                    error.insertAfter(element);
                } else {
                    error.insertAfter(element);
                }

            },

            highlight: function(element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function(form) {
                success1.show();
                error1.hide();

                formSubmitAjax();

                //  form.submit();
            }
        });


        function formSubmitAjax() {
            $.ajax({
                type: "POST",
                dataType: 'json',
                url: BaseUrl + 'evento/BackOffice/Prova/processar/',
                data: {
                    dataJson: $('#form-prova').serializeFormJSON()
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    alert("Server down. Try Later")
                },
                success: function(data) {

                    console.log("data down");
                    console.log(data);
                    if (data == true) {
                        if ($('input[name=id]').val() == undefined) {
                            sweetAlert("Sucesso!", "Prova cadastrada.", "success");
                            document.getElementById("form-prova").reset(); // limpa formulário
                        } else {
                            sweetAlert("Sucesso!", "Prova atualizada.", "success");
                            setTimeout(function() {
                                window.history.back();
                            }, 500);
                        }
                    } else {
                        sweetAlert("Erro!", "Solicitação não processada. Solicite suporte técnico.", "error");
                    }
                }
            });
        }


    }


    return {
        //main function to initiate the module
        init: function() {
            handleValidation1();
        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});