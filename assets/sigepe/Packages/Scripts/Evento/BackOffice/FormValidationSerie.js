var FormValidation = function () {



    // basic validation
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#form-serie');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {

                    'altura': {required: true },
                    'nome-serie': {required: true },
                    'valor-ate-inscricao-definitiva': {required: true },
                    'valor-apos-inscricao-definitiva': {required: true },
                    'serie-categoria': {required: true },

                },
                messages: {
                    'altura': {required: "Selecione uma altura"},
                    'nome-serie': {required: "Informe o nome da série"},
                    'valor-ate-inscricao-definitiva': {required: "Campo obrigatório"},
                    'valor-apos-inscricao-definitiva': {required: "Campo obrigatório"},
                    'serie-categoria': {required: "Selecione pelo menos uma categoria."},
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');


                    if (cont.size() > 0) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }

                    if (element.attr("type") == "radio") {
                        error.insertAfter(element);
                    } else {
                        error.insertAfter(element);
                    }

                    // CATEGORIA
                    if (element.attr("name") == "serie-categoria[]")
                        error.insertAfter("#serie-categoria-error");

                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success1.show();
                    error1.hide();
                    
                    formSubmitAjax();

                  //  form.submit();
                }
            });


            function formSubmitAjax() {
                

                $.ajax({
                    type: "POST",
                    dataType:'json',
                    url : BaseUrl + 'evento/BackOffice/Serie/processar/',
                    data: { dataJson: $('#form-serie').serializeFormJSON() },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        alert("Server down. Try Later")
                    },
                    success: function (data) {

                        console.log("data down");
                        console.log(data);
                        if(data == true){
                            if($('input[name=id]').val() == undefined){
                                sweetAlert("Sucesso!", "Série cadastrada.", "success");
                                document.getElementById("form-serie").reset();
                                $("#my_multi_select1").multiSelect("refresh");
                            }else{
                                sweetAlert("Sucesso!", "Série atualizada.", "success");
                                setTimeout(function () {
                                    //window.location.href = BaseUrl + "dashboard";
                                    window.history.back();
                                }, 500);
                            }
                        }else{
                            sweetAlert("Erro!", "Solicitação não processada. Solicite suporte técnico.", "error");
                        }
                    }
                });
                /*
                $.ajax({
                    url : BaseUrl + 'evento/BackOffice/Serie/processar/',
                    type : 'POST',
                    data: { dataJson:  },
                    async: false,
                    dataType:'json',
                    success : function(data) {            
                            sweetAlert("Sucesso!", "Você será redirecionado para sua conta.", "success");
                    },
                    error : function(request,error)
                    {
                        alert("Request: "+JSON.stringify(request));
                    }
                });*/

            }


    }


    return {
        //main function to initiate the module
        init: function () {
            handleValidation1();
        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});